select *
from Habits

insert into Habits (Habit,UpdatedByUser,UpdatedDate) values ('Self-belief','sa',getdate())
insert into Habits (Habit,UpdatedByUser,UpdatedDate) values ('Perseverance','sa',getdate())
insert into Habits (Habit,UpdatedByUser,UpdatedDate) values ('Resilience','sa',getdate())
insert into Habits (Habit,UpdatedByUser,UpdatedDate) values ('Empathy','sa',getdate())
insert into Habits (Habit,UpdatedByUser,UpdatedDate) values ('Positivity','sa',getdate())
insert into Habits (Habit,UpdatedByUser,UpdatedDate) values ('Curiosity','sa',getdate())
insert into Habits (Habit,UpdatedByUser,UpdatedDate) values ('Excellence','sa',getdate())

select *
from Grades

insert into Grades (Grade, UpdatedByUser, UpdatedDate) values ('Developing','sa',getdate())
insert into Grades (Grade, UpdatedByUser, UpdatedDate) values ('Consolidating','sa',getdate())
insert into Grades (Grade, UpdatedByUser, UpdatedDate) values ('Established','sa',getdate())

select *
from dbo.SkillArea

insert into SkillArea (SkillArea,UpdatedByUser, UpdatedDate) values ('Communication','sa',getdate())
insert into SkillArea (SkillArea,UpdatedByUser, UpdatedDate) values ('Teamwork','sa',getdate())
insert into SkillArea (SkillArea,UpdatedByUser, UpdatedDate) values ('Problem Solving','sa',getdate())
insert into SkillArea (SkillArea,UpdatedByUser, UpdatedDate) values ('Self-Awareness','sa',getdate())
insert into SkillArea (SkillArea,UpdatedByUser, UpdatedDate) values ('Personal Organisation','sa',getdate())

select *
from dbo.SkillFocusArea

insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (1,'Speaking clearly to a range of groups and demonstrating active listening skills','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (1,'Written communication skills for differing contexts (letter, email, resume, SMS text)','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (2,'Works effectively with a range of groups ','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (2,'Applying teamwork skills to a range of situations','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (3,'Showing independence and initiative in identifying problems and solving them ','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (3,'Solving problems in teams and applying strategies in problem solving','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (4,'Understands the impact of behaviour on others.','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (4,'Demonstrates an ability to give and receive feedback','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (5,'Manages personal and work priorities','sa',getdate())
insert into SkillFocusArea (SkillAreaId,FocusArea,UpdatedByUser, UpdatedDate) values (5,'Demonstrates an ability to plan and organise information','sa',getdate())

select *
from dbo.SkillFocusLevel
where SkillFocusAreaId=9

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   1   -- SkillFocusAreaId - int
  ,'Demonstrates a limited ability to clearly communicate ideas to a group. Needs to develop a broader range of active listening skills.'  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   1   -- SkillFocusAreaId - int
  ,'Demonstrates a reasonable ability to clearly communicate and deliver ideas to a group.Needs to further develop a broader range of active listening skills.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   1   -- SkillFocusAreaId - int
  ,'Demonstrates a sound ability to communicate and deliver ideas to a group. Displays some use of active listening skills in a   range of situations.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   1   -- SkillFocusAreaId - int
  ,'Demonstrates    a good ability to communicate and deliver ideas to a group. Generally displays  good use of active listening skills in a range of situations.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   1   -- SkillFocusAreaId - int
  ,'Demonstrates a   very good ability to communicate and deliver ideas to a range of groups. Displays some insight through the effective use of   active listening skills in a range of situations.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   1   -- SkillFocusAreaId - int
  ,'Demonstrates an excellent ability to communicate and deliver ideas to a range of groups Displays insight  through the highly effective use of active listening skills in a range of situations.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   2   -- SkillFocusAreaId - int
  ,'Demonstrates basic written communication skills through the ability to sometimes use the format and conventions of written communication for work and social purposes. More thorough planning and editing of work would result in the production of texts of an improved standard.'  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   2   -- SkillFocusAreaId - int
  ,'Demonstrates reasonable written communication skills through the ability to generally use the format and conventions of written communication for work and social purposes. More thorough planning and editing of work would result in the production of texts of a better quality.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   2   -- SkillFocusAreaId - int
  ,'Demonstrates able written communication skills through the ability to frequently use the format and conventions of written communication for work and social purposes. More thorough planning and editing of work would result in the production of texts of a higher calibre.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevels (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   2   -- SkillFocusAreaId - int
  ,'Demonstrates good written communication skills in a variety of contexts. The ability to consistently use the format and conventions of written communication for work and social purposes resulted in texts of a good standard. More thorough planning and editing of work would result in texts of a higher calibre.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   2   -- SkillFocusAreaId - int
  ,'Demonstrates very good written communication skills in a variety of contexts. The ability to plan and edit your writing; and your effective use of the format and conventions of written communication for work and social purposes produced texts of a high calibre. '  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   2   -- SkillFocusAreaId - int
  ,'Demonstrates excellent written communication skills in a variety of contexts. The ability to plan and edit your writing; and your highly effective use of the format and conventions of written communication for work and social purposes produced texts of outstanding quality. '  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   3   -- SkillFocusAreaId - int
  ,'Shows a limited ability to work with a range of groups. Needs to develop more effective skills in this area.'  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   3   -- SkillFocusAreaId - int
  ,'Shows a reasonable ability to work with a range of groups. Needs to develop further skills in this area.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   3   -- SkillFocusAreaId - int
  ,'Shows a sound ability to work with a range of groups.  Has some awareness for the needs of others in the group.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   3   -- SkillFocusAreaId - int
  ,'Shows effective skills when working with a range of groups. Is beginning to become sensitive to the needs of particular group members.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   3   -- SkillFocusAreaId - int
  ,'Shows very good skills when working with a range of groups. Is able to recognise the needs of particular group members.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   3   -- SkillFocusAreaId - int
  ,'Shows excellent skills when working with a range of groups. Is able to confidently  recognise and respond to the needs of particular group members.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   4   -- SkillFocusAreaId - int
  ,'Applies a basic range of teamwork skills when working in a variety of situations. Further skill development is needed. '  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   4   -- SkillFocusAreaId - int
  ,'Applies a reasonable range of teamwork skills when working in a variety of situations.  further  support in the development of  skills is needed.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   4   -- SkillFocusAreaId - int
  ,'Applies a sound range of teamwork skills when working in a variety of situations. Is beginning to tailor skills to suit the various situation.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   4   -- SkillFocusAreaId - int
  ,'Applies an effective range of teamwork skills when working in a variety of situations. Is able to tailor skills to suit the situation'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   4   -- SkillFocusAreaId - int
  ,'Applies a very good range of teamwork skills when working in various situations. Is able to effectively tailor a variety of skills to suit the situation'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   4   -- SkillFocusAreaId - int
  ,'Applies an excellent range of teamwork skills when working in various situations. Is able to be perceptive and implement the skills needed to suit the situation.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   5   -- SkillFocusAreaId - int
  ,'Shows a limited level of independence and initiative when identifying problems. Needs to develop greater levels of confidence and skill in solving problems. '  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   5   -- SkillFocusAreaId - int
  ,'Shows a reasonable level of independence and initiative when identifying problems. Needs  support in the development of   problem-solving skills.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   5   -- SkillFocusAreaId - int
  ,'Shows a sound level of independence and initiative when identifying problems.  Is beginning to demonstrate problem-solving skills.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   5   -- SkillFocusAreaId - int
  ,'Shows an effective level of independence and initiative when identifying problems. Is beginning to demonstrate more advanced problem-solving skills.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   5   -- SkillFocusAreaId - int
  ,'Shows a very good level of independence and initiative when identifying problems. Is able to demonstrate an array of problem-solving skills.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   5   -- SkillFocusAreaId - int
  ,'Shows an excellent level of independence and initiative when identifying problems. Is able to demonstrate an array of problem-solving skills and apply them as needed.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   6   -- SkillFocusAreaId - int
  ,'Applies a  basic range of strategies when solving problems   Is only able to provide a limited contribution to solving problems in teams.'  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   6   -- SkillFocusAreaId - int
  ,'Applies a reasonable range of strategies when solving problems.  Needs further support in devising    ways to contribute ideas to solve problems in teams. '  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   6   -- SkillFocusAreaId - int
  ,'Applies a sound range of strategies when solving problems   Is able to contribute ideas to solve problems within a team .'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   6   -- SkillFocusAreaId - int
  ,'Applies an effective range of strategies when solving problems.     Is able contribute a variety of ideas to solve problems in a team.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   6   -- SkillFocusAreaId - int
  ,'Applies a very good range of strategies when solving problems in teams.  Is able to participate effectively and contribute a range of ideas to solve problems in a team.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   6   -- SkillFocusAreaId - int
  ,'Applies an excellent range of strategies when solving problems.   Is able to demonstrate a broader knowledge of more complex strategies and apply these  to solve problems in a team.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   7   -- SkillFocusAreaId - int
  ,'Displays a limited understanding of the impact of behaviour on others. Needs to make an effort to demonstrate empathy towards others.'  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   7   -- SkillFocusAreaId - int
  ,'Displays a reasonable understanding of the impact of behaviour on others. Needs to make further efforts to demonstrate empathy towards others.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   7   -- SkillFocusAreaId - int
  ,'Displays a sound understanding of the impact of behaviour on others.  Is beginning to show a more perceptive understanding of the needs of others.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   7   -- SkillFocusAreaId - int
  ,'Displays an effective understanding of the impact of behaviour on others.  Is  able to show a perceptive understanding of the needs of others and the ability to respond accordingly.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   7   -- SkillFocusAreaId - int
  ,'Displays a very good understanding of the impact of behaviour on others. Is  able to show a  highly perceptive understanding of the needs of others and the ability to respond accordingly.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   7   -- SkillFocusAreaId - int
  ,'Displays an excellent understanding of the impact of behaviour on others. Is able to show an insightful understanding of the needs of others and the ability to respond accordingly.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   8   -- SkillFocusAreaId - int
  ,'Demonstrates a basic ability to give and receive feedback. Needs to be more receptive to constructive feedback.'  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   8   -- SkillFocusAreaId - int
  ,'Demonstrates a reasonable ability to give and receive feedback. Needs to be more receptive to constructive feedback and respond appropriately.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   8   -- SkillFocusAreaId - int
  ,'Demonstrates a sound ability to give and receive feedback.  Is beginning to be more receptive to constructive feedback and is able to offer some appropriate comments in return.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   8   -- SkillFocusAreaId - int
  ,'Demonstrates an effective ability to give and receive feedback. Is   receptive to constructive feedback and is able to offer some appropriate comments in return.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   8   -- SkillFocusAreaId - int
  ,'Demonstrates a very good ability to give and receive feedback.  Encourages   constructive feedback and is able to offer appropriate comments in return to form a collaborative process.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   8   -- SkillFocusAreaId - int
  ,'Demonstrates an excellent ability to give and receive feedback. Is highly receptive to constructive feedback and is able to offer perceptive comments in return to form a collaborative process.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   9   -- SkillFocusAreaId - int
  ,'Is able to manage personal and work priorities at a basic level. Needs to develop a management system in order to meet deadlines.'  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   9   -- SkillFocusAreaId - int
  ,'Is able to manage personal and work priorities at a reasonable level. Needs support in   developing a more effective management system in order to meet deadlines.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   9   -- SkillFocusAreaId - int
  ,'Is able to manage personal and work priorities at an adequate level.  Is beginning to develop a management system in order to meet deadlines and  expectations.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   9   -- SkillFocusAreaId - int
  ,'Is able to manage personal and work priorities efficiently. Demonstrates a   management system which regularly meets deadlines and  expectations.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   9   -- SkillFocusAreaId - int
  ,'Manages personal and work priorities in a highly organised fashion. Demonstrates a more advanced management system which regularly meets deadlines and exceeds expectations.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   9   -- SkillFocusAreaId - int
  ,'Manages personal and work priorities in a highly organised fash;ion  and is able to respond to urgent tasks appropriately.  Meets deadlines with ease and regularly exceeds expectations.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)

insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   10   -- SkillFocusAreaId - int
  ,'Demonstrates a limited ability to plan and organise information. Needs to implement a process to improve this skill. '  -- SkillLevel - varchar(8000)
  ,1 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   10   -- SkillFocusAreaId - int
  ,'Demonstrates a reasonable ability to plan and organise information. Needs further support to implement an appropriate  process for organising information.'  -- SkillLevel - varchar(8000)
  ,2 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   10   -- SkillFocusAreaId - int
  ,'Demonstrates a sound ability to plan and organise information.  Is beginning to implement and follow a process for organising and accessing information for improved outcomes.'  -- SkillLevel - varchar(8000)
  ,3 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   10   -- SkillFocusAreaId - int
  ,'Demonstrates an effective ability to plan and organise information. Has been  able to implement and follow a  process for organising and accessing information for improved outcomes.'  -- SkillLevel - varchar(8000)
  ,4 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   10   -- SkillFocusAreaId - int
  ,'Demonstrates a very good ability to plan and organise information. Has been able to implement a more advanced  process for organising and accessing information for improved outcomes.'  -- SkillLevel - varchar(8000)
  ,5 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)
insert into SkillFocusLevel (SkillFocusAreaId,SkillLevel,SkillLevelValue,UpdatedByUser,UpdatedDate) VALUES (
   10   -- SkillFocusAreaId - int
  ,'Demonstrates an excellent ability to plan and organise information for strategic outcomes. Has been able to devise and implement advanced  processes for organising and accessing information to suit a range of situations.'  -- SkillLevel - varchar(8000)
  ,6 -- SkillLevelValue - int
  ,'sa'
  ,getdate()
)