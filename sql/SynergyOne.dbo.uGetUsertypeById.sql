-- Roles create table ---------------------------------

USE [SEDAMyPlan]
GO

/****** Object:  Table [dbo].[Roles]    Script Date: 01-Jun-17 11:09:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Roles](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NOT NULL,
	[RoleDescription] [varchar](1000) NULL,
	[UpdatedByUser] [varchar](1000) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[DeletedAt] [datetime] NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

-- Users create table -----------------------------------------------------------------

USE [SEDAMyPlan]
GO

/****** Object:  Table [dbo].[Users]    Script Date: 01-Jun-17 11:14:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[SynergyId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[UpdatedByUser] [varchar](1000) NOT NULL,
	[UpdatedDate] [datetime] NOT NULL,
	[DeletedAt] [datetime] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([RoleId])
GO

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles]
GO



--- Alter function uGetUsertypeById  -----------------------------------------------------------

USE [SEDAMyPlan]
GO

/****** Object:  UserDefinedFunction [dbo].[uGetUsertypeById]    Script Date: 03-Apr-18 10:36:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER FUNCTION [dbo].[uGetUsertypeById]
(@userId int)
RETURNS varchar(10)
WITH EXEC AS CALLER
AS
BEGIN
declare @result varchar(10)

if ( SynergyOne.dbo.uIsStudent(@userId)=1 )
	SET @result = 'student'
else if exists( select * from SynergyOne.dbo.Constituencies where ConstitCode='@PC' and ID = @userId )
    SET @result = 'parent'
else if exists(select RoleName from SEDAMyPlan.dbo.Roles r inner join SEDAMyPlan.dbo.Users u on u.RoleId = r.Id where u.SynergyId = @userId)
	(select @result =RoleName 
	from SEDADevPlan.dbo.Roles r
	inner join SEDADevPlan.dbo.Users u 
	on u.RoleId = r.Id
	where u.SynergyId =  @userId)
else
  SET @result = 'teacher'
 
return @result
END
GO




