// Angular 2
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';
import '@angular/forms';
import '@angularclass/hmr';
import 'rxjs';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...

// Bootstrap
import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';

// PrimeNG
import 'primeng/primeng';
import 'primeng/resources/themes/omega/theme.css';
import 'primeng/resources/primeng.min.css';
// import 'primeng/resources/'

// Quill - WYSIWYG editor
import 'quill/dist/quill.js';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';

// FontAwesome
import 'font-awesome/css/font-awesome.min.css';

// Loading Bar
import 'ng-loading-bar/loading-bar.css'

import 'jspdf/dist/jspdf.min.js'
