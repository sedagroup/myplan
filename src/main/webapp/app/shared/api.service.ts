// Angular built-in libarary imports
import { Injectable } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { URLSearchParams } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// Angular related library imports
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

// Third party library imports
import * as _ from 'lodash';
import { Message } from 'primeng/primeng';

// Application specific class imports
import { Profile, Semester, Document, StudentSectionCompletion, MyStudentSectionEditable, StudentSkillFocusArea, StudentConversation } from '../interfaces/model';

@Injectable()
export class ApiService {

  /******* Variables to be changed depending of the environment deployed ***********/
  apiEndPoint: String = '/MyPlan/';
  instanceType: String = 'Group';
  /******* Section ends ************************************************************/

  form: NgForm;
  profile: Profile = new Profile();
  documents: Array<Document> = [];
  errorMessages: Message[];
  successMessages: Message[];
  studentId: string;
  key: string;
  time: string;
  name: string;
  students: Array<any>;
  teachers: Array<any>;
  supervisors: Array<any>;
  isTeacher: Boolean = false;
  isParent: Boolean = false;
  isSupervisor: Boolean = false;
  isAdmin: Boolean = false;
  studentName: String = '';
  private urlParams: URLSearchParams;
  grades: Array<any>;
  habits: Array<any>;
  industries: Array<any>;
  organizationTypes: Array<any>;
  habitGrades: any[][];
  skillAreas: Array<any> = [];
  areas: Array<any> = [];
  semesterId: number;
  previousSemesterId: number;
  currentSemesterId: number;
  chart: string;
  semesters:  Array<Semester>;
  habitGradesMapBySemester = new Map();
  habitCommentsMap = new Map();
  skillGradesMapBySemester = new Map();
  gradesMapBySemester = new Map();
  gradeCommentsMapBySemester = new Map();

  // Charts 
  lineCharts: any[] = [];
  skillAreasGroupBySkillArea: any;

  conversation: StudentConversation;
  studentConversations: Array<StudentConversation> = [];

  myStudentSectionEditable: MyStudentSectionEditable = {
    isDisabledMyprofile: true,
    isDisabledMyGoals: true,
    isDisabledMyPlacement: true,
    isDisabledMyExperiences: true,
    isDisabledMyPortfolio: true,
    isDisabledMySkills: true,
  };

  // BehaviorSubjects
  private _skillAreasSubject: BehaviorSubject<Array<any>>;
  private _habitsGradesSubject: BehaviorSubject<any>;
  private _myPlanWeightagesSubject: BehaviorSubject<Array<any>>;
  private _studentsSubject: BehaviorSubject<Array<any>>;
  private _studentConversationsSubject: BehaviorSubject<Array<any>>;

  private _areasSubject: BehaviorSubject<Array<any>>;

  // Observables
  public skillAreasObservable: Observable<Array<any>>;
  public habitGradesObservable: Observable<any>;
  public myPlanWeightagesObservable: Observable<Array<any>>;
  public studentsObservable: Observable<Array<any>>;
  public studentConversationsObservable: Observable<Array<any>>;

  public areasObservable: Observable<Array<any>>;


  constructor(private httpClient: HttpClient) {
    // create BehaviorSubjects
    this._skillAreasSubject = new BehaviorSubject(new Array());
    this._habitsGradesSubject = new BehaviorSubject(new Object());
    this._myPlanWeightagesSubject = new BehaviorSubject(new Array());
    this._studentsSubject = new BehaviorSubject(new Array());
    this._studentConversationsSubject = new BehaviorSubject(new Array());

    this._areasSubject = new BehaviorSubject(new Array());


    // create Observables from BehaviorSubjects
    this.skillAreasObservable = this._skillAreasSubject.asObservable();
    this.habitGradesObservable = this._habitsGradesSubject.asObservable();
    this.myPlanWeightagesObservable = this._myPlanWeightagesSubject.asObservable();
    this.studentsObservable = this._studentsSubject.asObservable();
    this.studentConversationsObservable = this._studentConversationsSubject.asObservable();

    this.areasObservable = this._areasSubject.asObservable();


  }

  // =================== Utility Methods =====================================================

  findByMatchingProperties(objArray, properties) {
    return objArray.filter(function (entry) {
      return Object.keys(properties).every(function (key) {
        return entry[key] === properties[key];
      });
    });
  }

   /*Return URLSearchParams of the current URL parameters*/
   getUrlParameters(): URLSearchParams {
    if (!this.urlParams) {
      this.urlParams = new URLSearchParams(window.location.search.substring(1));
    }
    return this.urlParams;
  }

  /*Convert the map of the current url parameters to an object*/
  getUrlParametersJSON() {
    let obj = Object.create(null);
    this.getUrlParameters().paramsMap.forEach((value: String[], key: string) => {
      obj[key] = value[0];
    });
    return obj;
  }

  removeFromCollection(collection: Array<any>, index: number, models: Array<NgModel>) {
    this.removeModelsForm(models);
    collection.splice(index, 1);
  }

  removeFromCollectionByElement(collection: Array<any>, element: any, models: Array<NgModel>) {
    this.removeModelsForm(models);
    let index = collection.indexOf(element);
    collection.splice(index, 1);
  }

  private removeModelsForm(models: Array<NgModel>) {
    models.forEach(model => {
      if (model) {
        this.form.removeControl(model);
      }
    });
  }

  goBack() {
    window.history.back();
  }

  // ========= END - utility methods ========================================================

  getSemesterInfoBySemesterId(selectedSemesterId: number) {
    return this.semesters.find(semesterItem =>  semesterItem.semesterId == selectedSemesterId);
  }

  saveMyProfile() {
    this.httpClient.post(this.apiEndPoint + 'api/saveStudentProfile?' + this.getUrlParameters().rawParams, 
      {
        'studentId': this.profile.id,
        'teacherId': null,
        'studentName': this.profile.studentName,
        'mybio': this.profile.mybio,
        'familyMembers': this.profile.familyMembers,
        'studentNetworks': this.profile.studentNetworks
      }
    )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
            this.profile = new Profile(data['data']);
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }
  
  getProfileByStudentId() {
    this.httpClient.get(this.apiEndPoint + 'api/latest/getProfileByStudentId', { params: this.getUrlParametersJSON() })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this.profile = new Profile(data['data']);
            if (data['data']) {
              this.getSkillsAndHabits();
              this.getAreas();
              this.getOrganizationTypes();
              this.getStudentConversations();
            }
            // console.log(this.profile);

          } else {
            this.profile = new Profile();
            this.getSkillsAndHabits();
            this.getAreas();
            this.getOrganizationTypes();
            this.getStudentConversations();
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  saveMyGoals() {
    this.httpClient.post(this.apiEndPoint + 'api/saveMyGoals?' + this.getUrlParameters().rawParams,
      {
        'studentId': this.profile.id,
        'studentCareerGoals': this.profile.studentCareerGoals,
        'studentIndustryGoals': this.profile.studentIndustryGoals
      }
    )
    .subscribe(
      data => {
        if (data['error']) {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
        } else {
          this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
        }
      },
      err => {
        this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
        console.log('Error ' + err);
      }
    );

  }

  saveMyPlacements() {
    this.httpClient.post(this.apiEndPoint + 'api/saveMyPlacements?' + this.getUrlParameters().rawParams,
      {
        'studentId': this.profile.id,
        'studentWorkPlacements': this.profile.studentJobs , 
        'studentJobReflections': this.profile.studentJobReflections
      }
    )
    .subscribe(
      data => {
        if (data['error']) {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
        } else {
          this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
        }
      },
      err => {
        this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
        console.log('Error ' + err);
      }
    );

  }

  saveMyExperiences() {
    this.httpClient.post(this.apiEndPoint + 'api/saveMyExperiences?' + this.getUrlParameters().rawParams,
      {
        'studentId': this.profile.id,
        'jobExperiences': this.profile.studentJobs
      }
    )
    .subscribe(
      data => {
        if (data['error']) {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
        } else {
          this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
        }
      },
      err => {
        this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
        console.log('Error ' + err);
      }
    );

  }
  

  setValuesSkillFocusLevels(skillAreas: Array<any>, screenWidth: number) {
    if(skillAreas) {
       // Clear already selected focusLevels      
      this.skillAreas = skillAreas;
      this.initializeSkillsMatrix('v1');
      this.loadChartData(screenWidth);      
    }
  }
  
  setRubricValues(areas: Array<any>) {
    if(areas) {
      this.areas = areas;
      this.initializeAreasMatrix('v2');
      //this.initializeFocusAreaCommentsBySemester();
    }
  }

  setValuesHabitGrades(habitGrades) {

    if (habitGrades.grades && habitGrades.habits) {
      this.grades = habitGrades.grades;
      this.habits = habitGrades.habits;
      this.initializeHabitsMatrix();

      /* load teacherMarks and studentMarks for habits by semester */
      this.profile.studentHabitGrades.forEach(habitGrade => {
        let semesterId = habitGrade.semesterId;
        if (semesterId) {
          let habitGradesMatrixBySemesterId = this.habitGradesMapBySemester.get(semesterId);
          // array index starting from 0 
          let gradeIndex = habitGrade.grade.id - 1;
          let habitIndex = habitGrade.habit.id - 1;
          let matchingHabitGradeCell = habitGradesMatrixBySemesterId[gradeIndex][habitIndex];
  
          if (habitGrade.id) {
            matchingHabitGradeCell.id = habitGrade.id;                      
          }
          if (habitGrade.isTeacherGrade) {
            matchingHabitGradeCell.isTeacherSelected = true;                      
          }
          if (habitGrade.isStudentGrade) {
            matchingHabitGradeCell.isStudentSelected = true;
          }          
          
        }          
      });

      /* Set habitGradeComment for the habit by semester */
      this.profile.studentHabitGradeComments.forEach(comment => {
        let semesterId = comment.semesterId;
        if (semesterId) {
          let habitCommentsBySemesterId = this.habitCommentsMap.get(semesterId);
          if (comment.gradeComments) {
            let habitIndex = comment.habit.id - 1;
            habitCommentsBySemesterId[habitIndex] = comment.gradeComments;
          }
        }

      });
      
    }
  }

  getDocuments() {
    this.httpClient.get(this.apiEndPoint + 'api/getDocuments', { params: this.getUrlParametersJSON() })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this.documents = data['data'];
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }
 
  deleteDocument(document: any) {
    // Normal params
    let params = this.getUrlParametersJSON();
    params['documentId'] = document.id;

    this.httpClient.delete(this.apiEndPoint + 'api/deleteDocument', { params: params })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            this.profile.studentDocuments.splice(this.profile.studentDocuments.findIndex(
              selectedDocument => { return selectedDocument.id == data['data'] }), 1);
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Document successfully removed' });
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  getUserType(): Observable<any> {
    return this.httpClient.get(this.apiEndPoint + 'api/getUserType', { params: this.getUrlParametersJSON() });
  }

  getSkillsAndHabits() {
    this.getGradesAndHabitsList();
    this.getSkillAreas();
  }

  getTeacherStudentList(selectedTeacherId?: string) {
    this.httpClient.get(this.apiEndPoint + 'api/teacherStudentList?teacherId=' + selectedTeacherId, { params: this.getUrlParametersJSON() })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this._studentsSubject.next(data['data']);
            /* Load habits, grades and skillAreas after the student profiles */
            this.getSkillsAndHabits();
            this.getAreas();
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  getParentStudentList() {
    this.httpClient.get(this.apiEndPoint + 'api/parentStudentList', { params: this.getUrlParametersJSON() })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this.students = data['data'];
            this.getSkillsAndHabits();
            this.getAreas();
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  getOrganizationTypes() {
    this.httpClient.get(this.apiEndPoint + 'api/getOrganizationTypes', { params: this.getUrlParametersJSON() } )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this.organizationTypes = data['data'];
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }
 
  getGradesAndHabitsList() {
    this.httpClient.get(this.apiEndPoint + 'api/getAllGrades', { params: this.getUrlParametersJSON() })
      .subscribe(
        gradesData => {
          if (gradesData['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: gradesData['errorMessage'] });
          } else if (gradesData['data']) {
            this.grades = gradesData['data'];
            this.httpClient.get(this.apiEndPoint + 'api/getAllHabits', { params: this.getUrlParametersJSON() })
              .subscribe(
                habitsData => {
                  if (habitsData['error']) {
                    this.errorMessages.push({ severity: 'error', summary: 'Error', detail: habitsData['errorMessage'] });
                  } else if (habitsData['data']) {
                    this.habits = habitsData['data'];
                    this._habitsGradesSubject.next({ 'habits': this.habits, 'grades': this.grades });
                  }
                },
                err => {
                  this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
                  console.log('Error ' + err);
                }
              );
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  initializeHabitsMatrix() {    
    for(let x = 0; x < this.semesters.length; x++) {
      let habitGrades = []; 
      let habitComments = [];
      for (let i = 0; i < this.grades.length; i++) {
        habitGrades[i] = [];
        for (let j = 0; j < this.habits.length; j++) {
          habitGrades[i][j] = {
            'isTeacherSelected': false,
            'isStudentSelected': false,
            'habit': this.habits[j].habit,
            'grade': this.grades[i].grade,
            'semesterId': this.semesters[x].semesterId
          };

          habitComments[j] = null;
        }
      }
      this.habitGradesMapBySemester.set(this.semesters[x].semesterId, habitGrades);
      this.habitCommentsMap.set(this.semesters[x].semesterId, habitComments);     
    }

    // console.log(this.habitCommentsMap);
    // console.log(this.habitGradesMapBySemester);
  }

  
  initializeSkillsMatrix(selectedAppVersion: string) {    
    
    if(this.semesters) {      
      
        /* Filter semesters with appVersion - v1(pre 2020) */
        let semestersForAppVersion1 = this.semesters.filter(semester => {
          return semester.appVersion == selectedAppVersion;
        });

        if (semestersForAppVersion1 && semestersForAppVersion1.length > 0) {
          for (let x = 0; x < semestersForAppVersion1.length; x++) { 
            let skillGrades = [];
            let semesterId = semestersForAppVersion1[x].semesterId; 
            let studentSkillFocusLevelsBySemester = this.findByMatchingProperties(this.profile.studentSkillFocusLevels, { semesterId: semesterId } );

            //console.log(studentSkillFocusLevelsBySemester);
      
            let skillFocusAreasBySemesterWithComments = this.profile.studentSkillFocusAreas.filter( studentSkillFocusAreaItem => {
                                              if(studentSkillFocusAreaItem.teacherComments != null && studentSkillFocusAreaItem.semesterId == semesterId) {
                                                return studentSkillFocusAreaItem;
                                              }
                                            });
            
            for (let i = 0; i < this.skillAreas.length; i++) {
              let skillArea = this.skillAreas[i];
              let skillAreaClone = _.cloneDeep(skillArea);
      
              for (let j = 0; j < skillAreaClone.skillFocusAreas.length; j++) {
                let skillFocusArea = skillAreaClone.skillFocusAreas[j];
                skillFocusArea.comments = null;       
                skillFocusArea.semesterId = semesterId;
                
                // Assign studentSkillFocusArea teacherComments in semester matrix
                let matchingSkillAreaCell = skillFocusAreasBySemesterWithComments.find(sfaItem => sfaItem.skillFocusAreaId == skillFocusArea.id);
                if(matchingSkillAreaCell){
                  skillFocusArea.comments = matchingSkillAreaCell.teacherComments;
                }
                
                // Assign teacher and student skillFocusLevel marks in semester matrix
                for (let k = 0; k < skillFocusArea.skillFocusLevels.length; k++) {
                  let skillFocusLevel = skillFocusArea.skillFocusLevels[k];
                  skillFocusLevel.semesterId = semesterId;
                  let matchingSkillLevelCell = studentSkillFocusLevelsBySemester.find(skillFocusLevelItem => skillFocusLevelItem.skillFocusLevel.id == skillFocusLevel.id);
                  if(matchingSkillLevelCell) {
                    skillFocusLevel.isTeacherSelected = matchingSkillLevelCell.isTeacherGrade;
                    skillFocusLevel.isStudentSelected = matchingSkillLevelCell.isStudentGrade;
                    skillFocusLevel.studentSkillFocusLevelId = matchingSkillLevelCell.id;
                    //console.log(skillFocusLevel);
                  }
                }
      
              }
      
              skillGrades[i] = skillAreaClone;   
              skillGrades[i].semesterId = semesterId;
            }
      
            this.skillGradesMapBySemester.set(semesterId, skillGrades);  
          }
        }

    } // end if semesters

   //console.log(this.skillGradesMapBySemester);
  }


  // load common student skill/habit matrix for 2020 rubrics
  initializeAreasMatrix(selectedAppVersion: string) {    
    
    if(this.semesters) {      
    
      /* Filter semesters with appVersion - v2 (2020 onwards) */
      let semestersForAppVersion2 = this.semesters.filter(semester => {
        return semester.appVersion == selectedAppVersion;
      });

      if (semestersForAppVersion2 && semestersForAppVersion2.length > 0) {
        for (let x = 0; x < semestersForAppVersion2.length; x++) { 
          let areaGrades = [];
          let semesterId = semestersForAppVersion2[x].semesterId; 
          let studentGradesBySemester = this.findByMatchingProperties(this.profile.studentGrades, { semesterId: semesterId } );

          let gradeCommentsByAreaMap = new Map();          
          for (let i = 0; i < this.areas.length; i++) {
            let area = this.areas[i];
            let areaClone = _.cloneDeep(area);

            let studentGradeCommentsBySemester = this.findByMatchingProperties(this.profile.studentGradeComments, { semesterId: semesterId, areaId: area.id } );

            // set grade comments by student/teacher for each focus area
            if(studentGradeCommentsBySemester && studentGradeCommentsBySemester.length > 0) {
              gradeCommentsByAreaMap.set(area.id, { 'id': studentGradeCommentsBySemester[0].id,
                                               'studentComment': studentGradeCommentsBySemester[0].studentComment, 
                                               'teacherComment': studentGradeCommentsBySemester[0].teacherComment 
                                             } 
                                          );
            } else {
              gradeCommentsByAreaMap.set(area.id, { 'id': null, 'studentComment': null, 'teacherComment': null } );
            }   

            for (let j = 0; j < areaClone.focusAreas.length; j++) {
              let focusArea = areaClone.focusAreas[j];
              focusArea.semesterId = semesterId;      

              // Assign teacher and student grade marks in semester matrix
              for (let k = 0; k < focusArea.rubrics.length; k++) {
                let rubric = focusArea.rubrics[k];
                rubric.semesterId = semesterId;
                let matchingRubricCell = studentGradesBySemester.find(studentGradeItem => studentGradeItem.rubricId == rubric.id);
                
                if(matchingRubricCell) {
                  rubric.isTeacherSelected = matchingRubricCell.isTeacherGrade;
                  rubric.isStudentSelected = matchingRubricCell.isStudentGrade;
                  rubric.studentGradeId = matchingRubricCell.id;
                }
              }
    
            }
    
            areaGrades[i] = areaClone;   
            areaGrades[i].semesterId = semesterId;
          }
    
          this.gradesMapBySemester.set(semesterId, areaGrades); 
          this.gradeCommentsMapBySemester.set(semesterId, gradeCommentsByAreaMap) ;
        }
      }

    } // end if semesters

    //console.log(this.gradesMapBySemester);

  }

   
  private getGradesAndHabitsMarks(): Array<any> {
    let studentHabitGradeMarks: Array<any> = new Array<any>();

    for(let x = 0; x < this.semesters.length; x++) {
      for (let i = 0; i < this.grades.length; i++) {
        for (let j = 0; j < this.habits.length; j++) {
          let semesterId = this.semesters[x].semesterId;
          let habitGradeItem = this.habitGradesMapBySemester.get(semesterId)[i][j];

          if (habitGradeItem.isTeacherSelected || habitGradeItem.isStudentSelected) {
            //console.log(habitGradeItem);

            studentHabitGradeMarks.push (
              {
                'id': habitGradeItem.id,
                'grade': this.grades[i], 
                'habit': this.habits[j], 
                'semesterId': habitGradeItem.semesterId,
                'isTeacherGrade': habitGradeItem.isTeacherSelected, 
                'isStudentGrade': habitGradeItem.isStudentSelected
              }
            );
          }
        }
      }
    }

    return studentHabitGradeMarks;
  }

  /* get teacher comments for student habitGrades for semester */
  private getHabitGradeComments(){
    let habitGradeComments: Array<any> = new Array<any>();
    for(let x = 0; x < this.semesters.length; x++) {
        for (let i = 0; i < this.habits.length; i++) {
          let semesterId = this.semesters[x].semesterId;
          let habitCommentItem = this.habitCommentsMap.get(semesterId)[i];

          if (habitCommentItem) {
            habitGradeComments.push (
              {
                'id': habitCommentItem.id,
                'habit': this.habits[i],
                'semesterId': semesterId,
                'gradeComments': habitCommentItem
              }
            );
          }
        }      
    }

    return habitGradeComments;
  }

  getSkillAreas() {
    this.httpClient.get(this.apiEndPoint + 'api/getSkillAreas', { params: this.getUrlParametersJSON() })
      .subscribe(data => {
        if (data['error']) {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
        } else {
          this._skillAreasSubject.next(data['data']);
        }
      },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  isStudentSkillFocusAreaExist(focusAreaId: number, semesterId : number) {
    return this.profile.studentSkillFocusAreas.find(item => item.skillFocusAreaId == focusAreaId && item.semesterId == semesterId);
  }

  getfocusLevelMarksAndSkillAreas() {
    let skillFocusLevelMarks: Array<any> = new Array<any>();   

    /* If no previous studentSkillFocusAreas, use the focusArea data */
    if (this.profile.studentSkillFocusAreas) {
      let studentSkillAreasForAllSemesters = _.flatten(Array.from(this.skillGradesMapBySemester.values()) );

      studentSkillAreasForAllSemesters.forEach(skillArea => {
        skillArea.skillFocusAreas.forEach(focusArea => {
          let isSkillFocusAreaMarked: boolean =  false;

          let dualMarkedSkillFocusLevel = focusArea.skillFocusLevels.filter(focusLevel => {
            return focusLevel.isStudentSelected == true && focusLevel.isTeacherSelected == true;
          })[0];

          if (dualMarkedSkillFocusLevel) {
            skillFocusLevelMarks.push({
              'id': dualMarkedSkillFocusLevel.studentSkillFocusLevelId,
              'semesterId': dualMarkedSkillFocusLevel.semesterId,
              'skillFocusLevelId': dualMarkedSkillFocusLevel.id,
              'teacherId': dualMarkedSkillFocusLevel.teacherId,
              'isTeacherGrade': dualMarkedSkillFocusLevel.isTeacherSelected,
              'isStudentGrade': dualMarkedSkillFocusLevel.isStudentSelected
            });
            isSkillFocusAreaMarked = true;
            
          } else {

            let teacherMarkedSkillFocusLevel = focusArea.skillFocusLevels.filter(focusLevel => {
              return focusLevel.isTeacherSelected;
            })[0];
            let studentMarkedSkillFocusLevel = focusArea.skillFocusLevels.filter(focusLevel => {
              return focusLevel.isStudentSelected;
            })[0];

            if (teacherMarkedSkillFocusLevel) {
              skillFocusLevelMarks.push({
                'id': teacherMarkedSkillFocusLevel.studentSkillFocusLevelId,
                'semesterId': teacherMarkedSkillFocusLevel.semesterId,
                'skillFocusLevelId': teacherMarkedSkillFocusLevel.id, 
                'teacherId': teacherMarkedSkillFocusLevel.teacherId,
                'isTeacherGrade': teacherMarkedSkillFocusLevel.isTeacherSelected, 
                'isStudentGrade': null
              });
            }
            if (studentMarkedSkillFocusLevel) {
              skillFocusLevelMarks.push({
                'id': studentMarkedSkillFocusLevel.studentSkillFocusLevelId,
                'semesterId': studentMarkedSkillFocusLevel.semesterId,
                'skillFocusLevelId': studentMarkedSkillFocusLevel.id, 
                'teacherId': null,
                'isTeacherGrade': null,
                'isStudentGrade': studentMarkedSkillFocusLevel.isStudentSelected
              });
            }

            if(teacherMarkedSkillFocusLevel || studentMarkedSkillFocusLevel) {
              isSkillFocusAreaMarked = true;
            }

          }

            // Edit comments for existing StudentSkillFocusAreas
            let profileStudentSkillFocusArea = this.isStudentSkillFocusAreaExist(focusArea.id, focusArea.semesterId);
            if (profileStudentSkillFocusArea) {
              profileStudentSkillFocusArea.teacherComments = focusArea.comments;
            } else {
              // StudentSkillFocusArea does not exist and add it to profile.
              if (focusArea && isSkillFocusAreaMarked) {
                this.addStudentSkillFocusAreaItem(focusArea);
              }
            }         
          
        });

      });
      
    }

    return skillFocusLevelMarks;
  }
  
  addStudentSkillFocusAreaItem(focusArea){
    let studentSkillFocusAreaItem: StudentSkillFocusArea = new StudentSkillFocusArea();
    studentSkillFocusAreaItem.teacherComments = focusArea.comments;
    studentSkillFocusAreaItem.skillFocusAreaId = focusArea.id;
    studentSkillFocusAreaItem.semesterId = focusArea.semesterId;
    this.profile.studentSkillFocusAreas.push(studentSkillFocusAreaItem);
  }

  saveTeacherReview() {
    
    this.httpClient.post(this.apiEndPoint + 'api/saveTeacherReview?' + this.getUrlParameters().rawParams,
      {
        'studentId': this.studentId,
        'habitsGrades': this.getGradesAndHabitsMarks(),
        'habitGradeComments': this.getHabitGradeComments(),
        'focusAreas': this.profile.studentSkillFocusAreas,
        'focusLevels': this.getfocusLevelMarksAndSkillAreas(),
        'studentSectionCompletions': this.profile.studentSectionCompletions,
        'studentJobReflections': this.profile.studentJobReflections,
        'studentJobs': this.profile.studentJobs,
        'studentCareerGoals': this.profile.studentCareerGoals,
        'studentIndustryGoals': this.profile.studentIndustryGoals
      }
    )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
      
  }

  
  getSupervisorTeachersList() {
    this.httpClient.get(this.apiEndPoint + 'api/getTeacherList', { params: this.getUrlParametersJSON() })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this.teachers = data['data'];
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  getSupervisorsList() {
    this.httpClient.get(this.apiEndPoint + 'api/getSupervisorList', { params: this.getUrlParametersJSON() })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this.supervisors = data['data'];
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  addSupervisors() {
    this.httpClient.post(this.apiEndPoint + 'api/saveSupervisors?' + this.getUrlParameters().rawParams, this.supervisors)
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            this.supervisors = data['data'];
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  markMySection(weightageId) {
    let studentSectionCompletion = this.createStudentSectionCompletion(weightageId);
    this.profile.studentSectionCompletions.push(studentSectionCompletion);
  }

  unmarkMySection(weightageId) {
    let studentSectionCompletion = this.findStudentSectionCompletionById(weightageId);
    let index = this.profile.studentSectionCompletions.indexOf(studentSectionCompletion);
    this.profile.studentSectionCompletions.splice(index, 1);
  }

  onChangeMarkMySection(section, approved) {
    let weightageId = this.getWeightageIdForSectionName(section);
    if (weightageId) {
      if (approved == true) {
        this.markMySection(weightageId);
      } else {
        this.unmarkMySection(weightageId);
      }
    }
  }

  createStudentSectionCompletion(weightageId) {
    let studentSectionCompletion = new StudentSectionCompletion();
    studentSectionCompletion.myPlanWeightageId = weightageId;
    studentSectionCompletion.completed = true;
    return studentSectionCompletion;
  }

  findStudentSectionCompletionById(weightageId) {
    return this.profile.studentSectionCompletions.find(item => item.myPlanWeightageId == weightageId);
  }

  getAllMyPlanWeightages() {
    this.httpClient.get(this.apiEndPoint + 'api/getAllMyPlanWeightages', { params: this.getUrlParametersJSON() })
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this._myPlanWeightagesSubject.next(data['data']);
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  getWeightageIdForSectionName(sectionName) {
    let myPlanWeightages = this._myPlanWeightagesSubject.getValue();
    return myPlanWeightages.find(weightage => weightage.section == sectionName).id;
  }

  setMyStudentSectionEditable() {
    if (this.isSupervisor || this.isAdmin || this.isParent) {

    } else if (this.isTeacher) {
      this.myStudentSectionEditable.isDisabledMySkills = false;
    } else {  // Student
      this.myStudentSectionEditable.isDisabledMyprofile = false;
      this.myStudentSectionEditable.isDisabledMyGoals = false;
      this.myStudentSectionEditable.isDisabledMyPlacement = false;
      this.myStudentSectionEditable.isDisabledMyExperiences = false;
      this.myStudentSectionEditable.isDisabledMyPortfolio = false;
      this.myStudentSectionEditable.isDisabledMySkills = false;
    }
  }

  saveStudentReview() {    

    this.httpClient.post(this.apiEndPoint + 'api/saveStudentReview?' + this.getUrlParameters().rawParams,
      {
        'studentId': this.profile.id,
        'habitsGrades': this.getGradesAndHabitsMarks(),
        'focusAreas': this.profile.studentSkillFocusAreas,
        'focusLevels': this.getfocusLevelMarksAndSkillAreas()
      }
    )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
      
  }

  getDocumentData(studentDocumentId) {
    return this.httpClient.get(this.apiEndPoint + 'api/getDocumentData?studentDocumentId=' + studentDocumentId, 
                                { params: this.getUrlParametersJSON() });
  }

  generateMySedaPortfolio() {
    const headers = new HttpHeaders({
        'Content-type': 'application/pdf',
        'Accept': 'application/pdf'
    });

    return this.httpClient.get(this.apiEndPoint + 'api/generateMySedaPortfolio?studentId=' + this.profile.id, {
      headers: headers,
      params: this.getUrlParametersJSON(),
      responseType: 'arraybuffer'
    });
  }

  loadChartData(screenWidth: number) {
    let chartWidth  = (screenWidth / 2.5);
    let chartHeight = (chartWidth * 0.60);

    let skillAreasArray = _.flatten(Array.from(this.skillGradesMapBySemester.values()) );
    this.skillAreasGroupBySkillArea = _.groupBy(skillAreasArray, 'skillArea');

    let myMap = new Map(Object.entries(this.skillAreasGroupBySkillArea));
    let chartMap = new Map();

      /* Group by array according to hierarchy (SkillArea -> skillFousArea - > skillFocusLevel) */
      myMap.forEach(function(focusAreas:any, skillArea) {
        let skillFocusAreasOuter:  Array<any> =  [];
        focusAreas.forEach(function(focusArea) {
          focusArea.skillFocusAreas.forEach(function(skillFocusAreaItem) {
            let selectedLevelByTeacher = skillFocusAreaItem.skillFocusLevels.find(function(focusLevelItem) {
              if (focusLevelItem && focusLevelItem.isTeacherSelected === true) {
                return focusLevelItem;
              }
            });
            // teacherSelectedSkillFocusLevel is the new property to hold teacher marked level
            skillFocusAreaItem.teacherSelectedSkillFocusLevel = selectedLevelByTeacher;

            if (skillFocusAreaItem.teacherSelectedSkillFocusLevel) {
              skillFocusAreasOuter.push(skillFocusAreaItem);
            }

          });
        });

        if (skillFocusAreasOuter.length > 0) {
          let skillFocusAreasByFocusArea = _.groupBy(skillFocusAreasOuter, 'focusAreaSummary');
          chartMap.set(skillArea, skillFocusAreasByFocusArea);
        }

      });
      
      //console.log(chartMap);
      this.drawChart(chartMap, chartWidth, chartHeight);
  }

  drawChart(chartMap: Map<any, any>, chartWidth: number, chartHeight: number) {
    //  ===================== Google line chart config options ==============================================================
    
    this.lineCharts.length = 0;
    chartMap.forEach((skillAreasMap: any, skillArea: string) => {

      let focusAreaCountForEachSkillArea = Object.keys(skillAreasMap).length;
      
      if (focusAreaCountForEachSkillArea >= 2) {

        let firstLineLegend  = Object.entries(skillAreasMap)[0][0];
        let secondLineLegend = Object.entries(skillAreasMap)[1][0];
  
        const skillLevelValueArray = Object.values(skillAreasMap);
        const firstFocusAreaValues  = skillLevelValueArray[0];        
        const secondFocusAreaValues = skillLevelValueArray[1];  
        
        let chartData =  {
          chartType: 'LineChart',
          dataTable: [  
              ['Semesters']              
          ],

          options: {
                colors: ['#00ccff', '#00609F'],
                backgroundColor: 'none',
                legend: { textStyle: { fontSize: 10,  bold: 'true' } },
                width: chartWidth ,
                height: chartHeight ,
                chartArea: { left: '10%', right: '22%', top: '10%', bottom: '20%', width: '68%', height: '50%' },
                hAxis: {
                  title: 'Year/Semester',
                  titleTextStyle : { bold: 'true' }
                },
                vAxis: {
                  title: 'Level',
                  titleTextStyle : { bold: 'true' },
                  ticks: [0, 1, 2, 3, 4, 5, 6]
                },
                series: {
                  0: { pointShape: 'circle', pointSize: 15, dataOpacity: 0.3 },
                  1: { pointShape: 'circle', pointSize: 5 }
              }
          },
  
        };

        // SkillFocusAreas - define legends for chart
        if(firstFocusAreaValues) {
          chartData.dataTable[0][1] = firstLineLegend;         
        }
        if(secondFocusAreaValues) {
          chartData.dataTable[0][2] = secondLineLegend;
        }

        // SkillFocusLevelValues selected by teacher - define chart points
        for (let i = 0; i < this.semesters.length; i++) {
          let semesterItem = this.semesters[i];
          if (semesterItem) {
            chartData.dataTable.push (
              [ 
                semesterItem.semesterYear + ' Semester ' + semesterItem.semesterValue, 
                this.findSkillLevelForSemesterByFocusArea(firstFocusAreaValues, semesterItem.semesterId),
                this.findSkillLevelForSemesterByFocusArea(secondFocusAreaValues, semesterItem.semesterId)
              ]
            );
          }
        }
        
        this.lineCharts.push( {'name': skillArea, 'data': chartData } );
      }

    });
    
  // =================== end line chart config options =====================================================
  }

  getSkillLevelForSemesterByFocusArea(focusArea) {
    if (focusArea && focusArea.teacherSelectedSkillFocusLevel && focusArea.teacherSelectedSkillFocusLevel.skillLevelValue) {
          return focusArea.teacherSelectedSkillFocusLevel.skillLevelValue;
    }
    return null;
  }

  findSkillLevelForSemesterByFocusArea(skillFocusAreas, semesterId) {
    let matchingFocusAreaItem =  skillFocusAreas.find(focusAreaItem => {
      return (focusAreaItem && focusAreaItem.teacherSelectedSkillFocusLevel && focusAreaItem.teacherSelectedSkillFocusLevel.skillLevelValue && focusAreaItem.semesterId == semesterId)
    });  
    if (matchingFocusAreaItem) {
      return matchingFocusAreaItem.teacherSelectedSkillFocusLevel.skillLevelValue;
    } else {
      return null;
    }    
  }

  getSemesters() {
    this.httpClient.get(this.apiEndPoint + 'api/getSemesters', { params: this.getUrlParametersJSON() } )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this.semesters = data['data'];
            if(this.semesters && this.semesters.length > 0) {
              this.setCurrentAndPreviousSemester();
            }
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  setCurrentAndPreviousSemester() {
    const currentDateTime = new Date().getTime();
    for(let i= 0; i< this.semesters.length; i++) {      
      let startDate = new Date(this.semesters[i].startDate).getTime();
      let endDate = new Date(this.semesters[i].endDate).getTime();
      if(currentDateTime >= startDate  &&  currentDateTime <= endDate ) {
        this.currentSemesterId = this.semesters[i].semesterId;
        if(this.semesters.length > 1) {
          this.previousSemesterId = this.semesters[i-1].semesterId;
        }
      }
    };
  }

  getStudentConversations() {
    const url = this.profile.id ? this.apiEndPoint + 'api/getStudentConversations?selectedStudentId='+ this.profile.id : this.apiEndPoint + 'api/getStudentConversations';
    this.httpClient.get(url, { params: this.getUrlParametersJSON() } )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else if (data['data']) {
            this._studentConversationsSubject.next(data['data']);   
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  saveConversation() {
    if (this.conversation) {
      this.conversation.studentId = this.profile.id;
      this.conversation.teacherId = this.profile.classTeacherId;
      this.httpClient.post(this.apiEndPoint + 'api/saveConversation?' + this.getUrlParameters().rawParams, this.conversation)
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            if(this.profile){
              this.getStudentConversations();
            }
            
           // this.conversation = new StudentConversation(data['data']);
           /*
            this.conversation = new StudentConversation();
            this.conversation.studentId = this.profile.id;
            this.conversation.teacherId = this.profile.classTeacherId;      
            */
            this.resetConversation();        
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );

    }
    
  }

  resetConversation(){
    this.conversation = new StudentConversation();
    this.conversation.conversationStartDate = new Date();
    this.conversation.studentId = this.profile.id;
    this.conversation.teacherId = this.profile.classTeacherId;
  }

  getAreas() {
    this.httpClient.get(this.apiEndPoint + 'api/latest/getAreas', { params: this.getUrlParametersJSON() })
      .subscribe(data => {
        if (data['error']) {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
        } else {
          this._areasSubject.next(data['data']);
        }
      },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
  }

  /* returns the maximum width of rubrics matrix for area type */
  getMaxRubricLengthByAreaType(areaType: string) {
    let maxRubricLength = 0;
    for (let i = 0; i < this.areas.length; i++) {
      if (this.areas[i].areaType.areaTypeName == areaType) {
        for(let j=0; j < this.areas[i].focusAreas.length; j++) {
          let focusAreaRubricLength = this.areas[i].focusAreas[j].rubrics.length;
          if(focusAreaRubricLength > maxRubricLength) {
            maxRubricLength = focusAreaRubricLength;
          }          
        }
      }     
    }
    return maxRubricLength;
 }

 saveTeacherReviewV2() {          
    this.httpClient.post(this.apiEndPoint + 'api/latest/saveTeacherReview?' + this.getUrlParameters().rawParams,
      {
        'studentId': this.studentId,       
        'studentGrades': this.getStudentGrades(),
        'studentGradeComments': this.getStudentGradeComments('v2'),
        'studentSectionCompletions': this.profile.studentSectionCompletions,
        'studentJobReflections': this.profile.studentJobReflections,
        'studentJobs': this.profile.studentJobs,
        'studentCareerGoals': this.profile.studentCareerGoals,
        'studentIndustryGoals': this.profile.studentIndustryGoals
      }
    )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );
              
  }

  saveStudentReviewV2() {        
    this.httpClient.post(this.apiEndPoint + 'api/latest/saveStudentReview?' + this.getUrlParameters().rawParams,
      {
        'studentId': this.profile.id,
        'studentGrades': this.getStudentGrades(),
        'studentGradeComments': this.getStudentGradeComments('v2')
      }
    )
      .subscribe(
        data => {
          if (data['error']) {
            this.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
          } else {
            this.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Saved successfully' });
          }
        },
        err => {
          this.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
          console.log('Error ' + err);
        }
      );      
            
  }

  getStudentGrades() {
    let studentGradeMarks: Array<any> = new Array<any>();   

    /* If no previous studentGrades, use the focusArea data */
    if (this.profile.studentGrades) {
      let studentAreasForAllSemesters = _.flatten(Array.from(this.gradesMapBySemester.values()) );

      studentAreasForAllSemesters.forEach(skillArea => {
        skillArea.focusAreas.forEach(focusArea => {

          let dualMarkedRubric = focusArea.rubrics.filter(rubricItem => {
            return rubricItem.isStudentSelected == true && rubricItem.isTeacherSelected == true;
          })[0];

          if (dualMarkedRubric) {
            studentGradeMarks.push({
              'id': dualMarkedRubric.studentGradeId,
              'semesterId': dualMarkedRubric.semesterId,
              'rubricId': dualMarkedRubric.id,
              'teacherId': dualMarkedRubric.teacherId,
              'isTeacherGrade': dualMarkedRubric.isTeacherSelected,
              'isStudentGrade': dualMarkedRubric.isStudentSelected
            });
            
          } else {

            let teacherMarkedRubric = focusArea.rubrics.filter(rubricItem => {
              return rubricItem.isTeacherSelected;
            })[0];
            let studentMarkedRubric = focusArea.rubrics.filter(rubricItem => {
              return rubricItem.isStudentSelected;
            })[0];

            if (teacherMarkedRubric) {
              studentGradeMarks.push({
                'id': teacherMarkedRubric.studentGradeId,
                'semesterId': teacherMarkedRubric.semesterId,
                'rubricId': teacherMarkedRubric.id, 
                'teacherId': teacherMarkedRubric.teacherId,
                'isTeacherGrade': teacherMarkedRubric.isTeacherSelected, 
                'isStudentGrade': null
              });
            }

            if (studentMarkedRubric) {
              studentGradeMarks.push({
                'id': studentMarkedRubric.studentGradeId,
                'semesterId': studentMarkedRubric.semesterId,
                'rubricId': studentMarkedRubric.id, 
                'teacherId': null,
                'isTeacherGrade': null,
                'isStudentGrade': studentMarkedRubric.isStudentSelected
              });
            }

          }

        });

      });
      
    }

    // console.log(studentGradeMarks);
    return studentGradeMarks;
  }


  /* get student and teacher comments for student grades for app version v2 */
  private getStudentGradeComments(selectedAppVersion: string) {
    let areaComments: Array<any> = new Array<any>();

    /* Filter semesters with appVersion - v2 (2020 onwards) */
    let semestersForAppVersion2 = this.semesters.filter(semester => {
      return semester.appVersion == selectedAppVersion;
    });

    if (semestersForAppVersion2 && semestersForAppVersion2.length > 0) {
      for(let x = 0; x < semestersForAppVersion2.length; x++) {

        let semesterId = semestersForAppVersion2[x].semesterId; 
        let areaCommentsForSemester = this.gradeCommentsMapBySemester.get(semesterId);

          for (let entry of Array.from(areaCommentsForSemester.entries())) {
            let areaId = entry[0];
            let areaComment = entry[1];
            if(areaComment.studentComment || areaComment.teacherComment) {
              areaComments.push (
                {
                  'id': areaComment.id,
                  'areaId': areaId,
                  'semesterId': semesterId,
                  'studentComment': areaComment.studentComment,
                  'teacherComment': areaComment.teacherComment
                }
              );
            }
          }

      }
    }

    return areaComments;
  }



}

