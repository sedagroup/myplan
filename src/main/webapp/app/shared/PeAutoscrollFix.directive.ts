import { Directive, Input, OnChanges, SimpleChange, ElementRef } from '@angular/core';

@Directive({
    selector: '[pEditorScrollFix]'
})
export class PeAutoscrollFixDirective implements OnChanges {
    @Input("pEditorScrollFix") content: string;

    ngOnChanges(changes: { [property: string]: SimpleChange }) {
        let change = changes['content'];
        let elemPosition = this.element.nativeElement.getBoundingClientRect().top + document.body.scrollTop;
        let clientHeight = document.documentElement.clientHeight;

        if (change.isFirstChange() || elemPosition > clientHeight) {
            this.element.nativeElement.style.display = 'none';
            setTimeout(() => {
                this.element.nativeElement.style.display = '';
            });
        }
    }

    constructor(private element: ElementRef) {
    }
}
