import { Component, Input } from '@angular/core';
import { NgForm } from '@angular/forms';

import { ApiService } from './../../../shared/api.service';
import { StudentJobReflection } from './../../../interfaces/model';

@Component({
  selector: 'job-reflection',
  templateUrl: './studentJobReflection.component.html',
  styleUrls: ['./studentJobReflection.component.css']
})
export class StudentJobReflectionComponent {
  @Input() title: String;
  @Input() description: String;
  @Input() form: NgForm;

  constructor(public service: ApiService) {
  }

  addStudentJobReflection() {
    let jobReflection = new StudentJobReflection();
    this.service.profile.studentJobReflections.push(jobReflection);
  }
}
