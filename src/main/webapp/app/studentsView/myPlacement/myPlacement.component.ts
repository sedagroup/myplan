import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../shared/api.service';

@Component({
    selector: 'my-placement',
    templateUrl: './myPlacement.component.html',
    styleUrls: ['./myPlacement.component.scss']
})

export class MyPlacementComponent {

    constructor(public service: ApiService, private router: Router) {
     }

    goToTeacherView() {
        this.router.navigate(['/teacher']);
    }
    

}
