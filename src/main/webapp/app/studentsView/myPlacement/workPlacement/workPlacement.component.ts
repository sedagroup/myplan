
import { Component, Input } from '@angular/core';
import { ApiService } from '../../../shared/api.service';
import { NgForm } from '@angular/forms';
import { StudentJob } from '../../../interfaces/model';

@Component({
  selector: 'work-placement',
  templateUrl: './workPlacement.component.html',
  styleUrls: ['./workPlacement.component.scss']
})
export class WorkPlacementComponent {
  @Input() description: String;
  @Input() type: String;
  @Input() form: NgForm;
  @Input() title: String;

  orgTypes: Array<String> = [];

  constructor(public service: ApiService) {
    this.orgTypes = ['School', 'AFL', 'Cricket', 'Football', 'Netball', 'Tennis', 'Basketball', 'Aquatics',
      'All Abilities', 'Charity', 'Community', 'Local Council', 'Other'];
  }

  addPlacement(type: String) {
    let job = new StudentJob();
    job.jobType = type;
    this.service.profile.studentJobs.push(job);
  }
}
