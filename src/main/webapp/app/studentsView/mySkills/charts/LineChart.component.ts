import { Component } from '@angular/core';
import { ApiService } from '../../../shared/api.service';

@Component({
  selector: 'my-line-chart',
  templateUrl: './lineChart.component.html',
  styleUrls: ['./lineChart.component.scss']
})
export class LineChartComponent {
  
  constructor(public service: ApiService) {
  }

}
