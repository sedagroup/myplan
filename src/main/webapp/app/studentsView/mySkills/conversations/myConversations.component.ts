import { Component, Input, OnInit } from '@angular/core';
import { StudentConversation } from '../../../interfaces/model';
import { ApiService } from '../../../shared/api.service';
import { NgForm } from '@angular/forms';


@Component({
  selector:'my-conversations',
  templateUrl:'./myConversations.component.html',
  styleUrls:['./myConversations.component.scss']
}) 

export class MyConversationsComponent implements OnInit{
    @Input() form: NgForm;
    @Input() conversation: StudentConversation;
    @Input() studentId: number;
    isConversationReadonly: boolean = false;

    constructor(public service: ApiService) {      
    }

    ngOnInit(){
      if(!this.service.conversation) {
        this.service.resetConversation();
      }    
    }
    
    loadConversation(studentConversation: StudentConversation) {
      this.service.conversation = studentConversation;
      this.isConversationReadonly = this.checkConversationReadOnly();
    }

    checkConversationReadOnly(): boolean {
       let isDisabled = false;
       if(!this.service.isTeacher && this.service.conversation.isFinal === true) {
        isDisabled = true;
      } 
      return isDisabled;
    }
    

}