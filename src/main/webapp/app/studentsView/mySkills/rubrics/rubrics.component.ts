import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from './../../../shared/api.service';

@Component({
    selector: 'my-rubrics',
    templateUrl: './rubrics.component.html'
 })

export class RubricsComponent {
    @Input() appVersion : string;

    constructor(public service: ApiService, private router: Router) {
    }

    goToTeacherView() {
        this.router.navigate(['/teacher']);
    } 

 }
