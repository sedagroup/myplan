import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../../../../../shared/api.service';

@Component({
    selector: 'personal-habits',
    templateUrl: './personalHabits.component.html',
    styleUrls: ['./personalHabits.component.scss']
})

export class PersonalHabitsComponent implements OnInit {
    @Input() semesterId: number;

    constructor(public service: ApiService) { }

    ngOnInit(){
        this.service.semesterId = this.semesterId;
    }

    cellClicked(semesterId: string, gradeIndex: number, habitIndex: number) {

        if (this.semesterId != this.service.currentSemesterId) {
            return false;
        }
        
       let selectedSemesterId = parseInt(semesterId, 10);

        if (this.service.isTeacher && !(this.service.isSupervisor) ) {
            /* Teacher mark */
            let habitGradeItem = this.service.habitGradesMapBySemester.get(selectedSemesterId)[gradeIndex][habitIndex];
            habitGradeItem.isTeacherSelected = !habitGradeItem.isTeacherSelected;
            this.clearRowSelectionForUserType('teacher', selectedSemesterId, gradeIndex, habitIndex);

        } else if (!this.service.isAdmin && !this.service.isTeacher && !this.service.isParent && !this.service.isSupervisor) {
            // TODO - create status for student (eg : isStudent) and change this condition to isStudent

            /* Student self mark */
            // if teacher already marked the row, disable student marking            
            for (let i = 0; i < this.service.grades.length; i++) {
                if (this.service.habitGradesMapBySemester.get(selectedSemesterId)[i][habitIndex].isTeacherSelected) {
                    return false;
                }
            }
            
            let habitGradeItem = this.service.habitGradesMapBySemester.get(selectedSemesterId)[gradeIndex][habitIndex];
            habitGradeItem.isStudentSelected = !habitGradeItem.isStudentSelected;
            this.clearRowSelectionForUserType('student', selectedSemesterId, gradeIndex, habitIndex);
        }
    }

    /* Clear the habit grade row selection for logged in 'Student' or 'Teacher' for selected semester */
    clearRowSelectionForUserType(userType: string, selectedsemesterId: number, gradeIndex: number, habitIndex: number) {
        for (let i = 0; i < this.service.grades.length; i++) {
            if (gradeIndex != i) {
                let habitGradeMatrixBySemester = this.service.habitGradesMapBySemester.get(selectedsemesterId);
                if (userType == 'student') {
                    habitGradeMatrixBySemester[i][habitIndex].isStudentSelected = false;
                } else if (userType == 'teacher') {
                    habitGradeMatrixBySemester[i][habitIndex].isTeacherSelected = false;
                    /* clear the previousTeacherSelection cell when row is clicked */
                    habitGradeMatrixBySemester[i][habitIndex].isPreviousSemesterMarked = habitGradeMatrixBySemester[i][habitIndex].isTeacherSelected;
                }
            }
        }
    }

    isCommentsReadonly() {
        if (this.service.isTeacher) {
            return false;
        }
        return true;
    }

}
