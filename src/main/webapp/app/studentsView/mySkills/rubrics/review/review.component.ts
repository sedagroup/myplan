import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../../../../shared/api.service';

@Component({
    selector: 'review',
    template: `<h3 class="text-center">SEDA Personal Habits: {{title}}</h3>
    <personal-habits semesterId="{{semesterId}}"></personal-habits>
    <h3 class="text-center">SEDA Work Skills: {{title}}</h3>
    <work-skills semesterId="{{semesterId}}"></work-skills>`
})

export class ReviewComponent implements OnInit {
    @Input() title: string;
    @Input() semesterId: number;

    constructor(public service: ApiService) {
     }

    ngOnInit() {
        this.service.semesterId = this.semesterId;
    }
}
