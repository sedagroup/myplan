import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../../../../../shared/api.service';

@Component({
    selector: 'work-skills',
    templateUrl: 'workSkills.component.html',
    styleUrls: ['./workSkills.component.scss']
})

export class WorkSkillsComponent implements OnInit {
    @Input() semesterId: number;

    constructor(public service: ApiService) {
    }

    ngOnInit() {
        this.service.semesterId = this.semesterId;
    }

    cellClicked(focusArea: any, focusLevel: any) {

        if (this.semesterId != this.service.currentSemesterId) {
            return false;
        }        

        /* If role is teacher and not a supervisor */
        if (this.service.isTeacher && !(this.service.isSupervisor)) {
            this.clearRowSelectionForUserType('teacher', focusArea, focusLevel);
            focusLevel.isTeacherSelected = !focusLevel.isTeacherSelected;        
        
        } else if (!this.service.isAdmin && !this.service.isTeacher && !this.service.isParent && !this.service.isSupervisor) {
             // TODO - create status for student (eg : isStudent) and change this condition to isStudent

            /* If role is student - self mark */

            // if teacher already marked the row, disable student marking
            let teacherSelectedFocusLevelItem = focusArea.skillFocusLevels.find(focusLevelItem => {
                if (focusLevelItem.isTeacherSelected == true) {
                    return focusLevelItem;
                }
            });

            if (teacherSelectedFocusLevelItem) {
                return false;
            }

            this.clearRowSelectionForUserType('student', focusArea, focusLevel);
            focusLevel.isStudentSelected = !focusLevel.isStudentSelected;
        }
    }

    clearRowSelectionForUserType(userType, focusArea, currentFocusLevel) {
        focusArea.skillFocusLevels.forEach(focusLevelItem => {
            if (userType == 'student' && focusLevelItem.id != currentFocusLevel.id) {
                    focusLevelItem.isStudentSelected = false;
            } else if (userType == 'teacher' && focusLevelItem.id != currentFocusLevel.id) {
                focusLevelItem.isTeacherSelected = false;
            }
            /* clear the previousTeacherSelection cell when row is clicked */
            focusLevelItem.isPreviousSemesterMarked = false;
        });
    }

    isCommentsReadonly() {
        if (this.service.isTeacher) {
            return false;
        }
        return true;
    }
}
