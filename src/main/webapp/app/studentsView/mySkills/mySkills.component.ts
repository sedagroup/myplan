import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'my-skills',
  templateUrl: './mySkills.component.html',
  styleUrls: ['../../shared/styles/tabmenu.scss']
})
export class MySkillsComponent implements OnInit  {

  displayPastProgress: boolean = false;

  constructor(public service: ApiService) {
  }

  ngOnInit() {

    this.service.habitGradesObservable.subscribe(habitGrades => {
      this.service.setValuesHabitGrades(habitGrades);
    });

    this.service.skillAreasObservable.subscribe(skillAreas => {
      this.service.setValuesSkillFocusLevels(skillAreas, window.screen.width);
    });

    this.service.studentConversationsObservable.subscribe(conversations => {
      this.service.profile.studentConversations = conversations;
    });

    // skills and habits - rubrics v2
    this.service.areasObservable.subscribe(areas => {
      this.service.setRubricValues(areas);
    });
    
  }

  toggleSkillsTabs(){
    this.displayPastProgress = ! this.displayPastProgress;
    console.log(this.displayPastProgress);
  }
}
