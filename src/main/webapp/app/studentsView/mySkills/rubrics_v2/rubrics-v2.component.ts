import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../../shared/api.service';

@Component({
    selector: 'my-rubrics-v2',
    templateUrl: './rubrics-v2.component.html'
 })

export class RubricsV2Component {
    @Input() appVersion : string;

    constructor(public service: ApiService, private router: Router) {
    }

    goToTeacherView() {
        this.router.navigate(['/teacher']);
    }    

 }
