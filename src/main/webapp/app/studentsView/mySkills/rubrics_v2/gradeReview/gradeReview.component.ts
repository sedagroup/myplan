import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../../../../shared/api.service';

@Component({
    selector: 'grade-review',
    template: `
               <h3 class="text-center">SEDA Personal Habits: {{title}}</h3>
               <area-type semesterId="{{semesterId}}" areaType="Habit"></area-type>
               <div class="spacer20"></div>
               <h3 class="text-center">SEDA Work Skills: {{title}}</h3>
               <area-type semesterId="{{semesterId}}" areaType="Skill"></area-type>
               `
})

export class GradeReviewComponent implements OnInit {
    @Input() title: string;
    @Input() semesterId: number;
  
    constructor(public service: ApiService) {
     }

    ngOnInit() {
        this.service.semesterId = this.semesterId;
    }

}
