import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'AppVersionFilter',
  pure: false
})
export class AppVersionPipe implements PipeTransform {
  transform(items: any[], applicationVersion: string) {
    if (!items) { return items };
    return items.filter(item => item.appVersion == applicationVersion);
  }
}
