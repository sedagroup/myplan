import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'AreaTypeFilter',
  pure: false
})
export class AreaTypePipe implements PipeTransform {
  transform(areaItems: any[], type: string) {
    if (!areaItems) { return areaItems };
    return areaItems.filter(areaItem => areaItem.areaType.areaTypeName == type);
  }
}
