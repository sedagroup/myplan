import { Component, Input, OnInit } from '@angular/core';
import { ApiService } from '../../../../../shared/api.service';

@Component({
    selector: 'area-type',
    templateUrl: 'areaType.component.html',
    styleUrls: ['./areaType.component.scss']
})

export class AreaTypeComponent implements OnInit {
    @Input() semesterId: number;
    @Input() areaType: string;

    areaTypeHeadings = [ 
            { 
            id: 29, 
            headingName: 'Beginning', 
            headingItems:[  
                            { title: 'Independence', description: 'Relies on others to direct skill development and learning.' } ,
                            { title: 'Building knowledge', description: 'Focuses on one piece of information at a time.'} ,
                            { title: 'Application', description: 'Participates when directed by others. Bases some work on ‘guesstimates’.'} ,
                        ]
            },

            { 
            id: 30, 
            headingName: 'Emerging', 
            headingItems:[
                    { title: 'Independence', description: 'Follows instructions and rules independently.' } ,
                    { title: 'Building knowledge', description: 'Makes connections between ideas and information with the support of others.'} ,
                    { title: 'Application', description: 'Applies knowledge and skills in familiar contexts.' } ,
                ] 
            },

            { 
                id: 31, 
                headingName: 'Developing', 
                headingItems: [
                    { title: 'Independence', description: 'Works independently; able to identify and take responsibility for own learning and skill development.' } ,
                    { title: 'Building knowledge', description: 'Creates connections between new and pre-existing knowledge and is able to understand the bigger picture.'} ,
                    { title: 'Application', description: 'Takes an active role in developing skills, applies knowledge in different contexts.' } ,
                ] 
            },

            { 
                id: 32, 
                headingName: 'Consolidating', 
                headingItems: [
                    { title: 'Independence', description: 'Values collaborating with others to achieve outcomes, taking into account their own and others’ skills and knowledge.' } ,
                    { title: 'Building knowledge', description: 'Finds connections and patterns between information. Looks for ways to continuously improve through self-reflection.'} ,
                    { title: 'Application', description: 'Uses knowledge and experience to anticipate issues and plan contingencies and responses.' } ,
                ] 
            },

            { 
                id: 33, 
                headingName: 'Mastering',                 
                headingItems: [
                    { title: 'Independence', description: 'Takes responsibility for their own and others’ learning and skill development. Leads and models behaviour to others.' } ,
                    { title: 'Building knowledge', description: 'Uses higher-order thinking skills to critique and weigh up the value of information.'} ,
                    { title: 'Application', description: 'Strategises how to approach new initiatives. Able to adjust plans based of new information and challenges.' } ,
                ] 
            }
    ];

    constructor(public service: ApiService) {
    }

    ngOnInit() {
        this.service.semesterId = this.semesterId;
    }

    cellClicked(focusArea: any, rubric: any) {

        /*
        if (this.semesterId != this.service.currentSemesterId) {
            return false;
        }      
        */  

        /* If role is teacher and not a supervisor */
        if (this.service.isTeacher && !(this.service.isSupervisor)) {
            this.clearRubricsRowSelectionForUserType('teacher', focusArea, rubric);
            rubric.isTeacherSelected = !rubric.isTeacherSelected;        
        
        } else if (!this.service.isAdmin && !this.service.isTeacher && !this.service.isParent && !this.service.isSupervisor) {

            /* If role is student - self mark */

            // if teacher already marked the row, disable student marking
            let teacherSelectedRubricItem = focusArea.rubrics.find(rubricItem => {
                if (rubricItem.isTeacherSelected == true) {
                    return rubricItem;
                }
            });

            if (teacherSelectedRubricItem) {
                return false;
            }

            this.clearRubricsRowSelectionForUserType('student', focusArea, rubric);
            rubric.isStudentSelected = !rubric.isStudentSelected;
        }
    }


    clearRubricsRowSelectionForUserType(userType, focusArea, currentRubric) {
        focusArea.rubrics.forEach(rubricItem => {
            if (userType == 'student' && rubricItem.id != currentRubric.id) {
                rubricItem.isStudentSelected = false;
            } else if (userType == 'teacher' && rubricItem.id != currentRubric.id) {
                rubricItem.isTeacherSelected = false;
            }

            /* clear the previousTeacherSelection cell when row is clicked */
            rubricItem.isPreviousSemesterMarked = false;
        });
    }

    isCommentsReadonly() {
        if (this.service.isTeacher) {
            return false;
        }
        return true;
    }


}
