import { Component, Input } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { ApiService } from '../../../shared/api.service';
import { StudentCareerGoal } from '../../../interfaces/model';

@Component({
  selector: 'career-goals',
  templateUrl: './careerGoals.component.html'
})
export class CareerGoalsComponent {

  @Input() goal: any;
  @Input() goalTitle: String;
  @Input() goalDescription: String;
  @Input() form: NgForm;

  constructor(public service: ApiService) {
  }

  addCareerGoal() {
    this.service.profile.studentCompletionStatus.markMyCareerGoal = false;
    this.service.profile.studentCareerGoals.push(new StudentCareerGoal());
  }

  remove(careerGoal: any, models: Array<NgModel>) {
    this.service.removeFromCollectionByElement(this.service.profile.studentCareerGoals, careerGoal, models);
  }

}
