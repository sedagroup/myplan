import { Component, Input } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { ApiService } from '../../../shared/api.service';

@Component({
  selector: 'goalTable',
  templateUrl: './goalTable.component.html'
})
export class GoalTableComponent {

  @Input() goal: any;
  @Input() goalType: String;
  @Input() goalTitle: String;
  @Input() goalDescription: String;
  @Input() form: NgForm;

  constructor(public service: ApiService) {

  }

  addPath() {
    this.goal.studentGoalPaths.push({ goalType: this.goalType });
  }

  remove(goalPlan: any, models: Array<NgModel>) {
    this.service.removeFromCollectionByElement(this.goal.studentGoalPaths, goalPlan, models);
  }
}
