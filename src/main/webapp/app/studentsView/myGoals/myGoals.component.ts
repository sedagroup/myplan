import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'my-goals',
  templateUrl: './myGoals.component.html',
  styleUrls: ['./myGoals.component.scss']
})
export class MyGoalsComponent {
  @Input() form: NgForm;

  constructor(public service: ApiService, private router: Router) {
  }

  goToTeacherView() {
    this.router.navigate(['/teacher']);
  }

}
