import { Component, Input } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { ApiService } from '../../../shared/api.service';
import { StudentIndustryGoal } from '../../../interfaces/model';

@Component({
  selector: 'industry-goals',
  templateUrl: './industryGoals.component.html'
})
export class IndustryGoalsComponent {

  @Input() goal: any;
  @Input() goalTitle: String;
  @Input() goalDescription: String;
  @Input() form: NgForm;

  constructor(public service: ApiService) {
  }

  addIndustryGoal() {
    this.service.profile.studentCompletionStatus.markMyIndustryGoal = false;
    this.service.profile.studentIndustryGoals.push(new StudentIndustryGoal());
  }

  remove(industryGoal: any, models: Array<NgModel>) {
    this.service.removeFromCollectionByElement(this.service.profile.studentIndustryGoals, industryGoal, models);
  }

}
