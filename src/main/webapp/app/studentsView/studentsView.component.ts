import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../shared/api.service';



@Component({
  selector: 'student-view',
  templateUrl: './studentsView.component.html',
  styleUrls: ['../shared/styles/tabmenu.scss']
})

export class StudentsViewComponent implements OnInit {

  descriptionMyPlacements = '<p>Work Placement forms an integral part of the SEDA program with one day a week for most of the year allocated to a students work placement. The placement is an ideal way for a student to gain valuable experience within the industry. SEDA encourages all students to complete work placements related to their employment goals and interests. The information below should include a list of work placements students undertake throughout their time with SEDA.</p>';
  descriptionEmployment = 'The information entered here should be a list of all the paid jobs you have undertaken whether it be casual, part time or full time.';
  descriptionMyExperiences = `<p>In the MyExperiences section you are required to record details of your industry experiences. This is an opportunity for you to collate your industry experiences and develop evidence of how you have gone above and beyond.</p>
  <p> <strong> Your teacher will approve your entry upon the receipt of something in writing from the employer/supervisor. This must be uploaded into your Portfolio. </strong> </p>
<p>To recognise the industry hours students complete within a year while enrolled with SEDA students will be provided with a Certificate of Achievement based on the number of industry hours they accumulate. These include:</p>`;


  constructor(public service: ApiService, private router: Router) {
    this.service.getStudentConversations();
    this.service.getAllMyPlanWeightages();
  }

  ngOnInit() {
    this.service.setMyStudentSectionEditable();
     setTimeout( () => {
       this.service.myPlanWeightagesObservable.subscribe(myPlanWeightages => {
        this.service.profile.setStudentCompletionStatus(myPlanWeightages);
    });

    }, 3000);
  }

  goToTeacherView() {
    this.router.navigate(['/teacher']);
  }

}
