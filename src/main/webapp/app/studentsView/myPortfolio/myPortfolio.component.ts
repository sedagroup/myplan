import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/api.service';

@Component({
  selector: 'my-portfolio',
  templateUrl: './myPortfolio.component.html',
  styleUrls: ['../../shared/styles/tabmenu.scss']
})
export class MyPortfolioComponent implements OnInit {

  constructor(public service: ApiService) { }

  ngOnInit() {
  }

}
