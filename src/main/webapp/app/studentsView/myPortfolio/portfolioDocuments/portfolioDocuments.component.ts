import { Component } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ApiService } from '../../../shared/api.service';

@Component({
  selector: 'portfolio-documents',
  templateUrl: './portfolioDocuments.component.html',
  styleUrls: ['./portfolioDocuments.component.scss']
})
export class PortfolioDocumentsComponent {
  constructor(public service: ApiService, private sanitizer: DomSanitizer) { }

  getDocumentData(stDocument: any) {
    this.service.getDocumentData(stDocument.id).subscribe(
      data => {
        if (data['error']) {
          this.service.errorMessages.push({ severity: 'error', summary: 'Error', detail: data['errorMessage'] });
        } else if (data['data']) {
          // TODO: Check security with out bypassSecurityTrustUrl
          // let url = this.sanitizer.bypassSecurityTrustUrl('data:'+ stDocument.contentType+';base64,'+ data['data']);
          let url = 'data:' + stDocument.contentType + ';base64,' + data['data'];

          const anchor = document.createElement('a');
          anchor.href = url;
          anchor.target = '_blank';
          anchor.download = stDocument.name;
          anchor.click();
        }
      },
      err => {
        this.service.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
        console.log('Error ' + err);
      }
    );
  }

  onUpload() {
    this.service.successMessages.push({ severity: 'success', summary: 'Success', detail: 'Document saved successfully' });
    this.service.getProfileByStudentId(); // Call the service again to get the latest changes.
  }

  onBeforeUpload(event: any, documentType: String) {
    event.formData.append('documentType', documentType);
  }

  getSanitazeDocURL(document: any): SafeUrl {
    return this.sanitizer.bypassSecurityTrustUrl('data:' + document.contentType + ';base64,' + document.documentData);
  }


  getDocumentsByType(type: String): Array<any> {
    let result = this.service.profile.studentDocuments.filter(document => {
      return document.document.documentType == type;
    });
    return result;
  }

  deleteDocument(document: any) {
    this.service.deleteDocument(document);
  }
}
