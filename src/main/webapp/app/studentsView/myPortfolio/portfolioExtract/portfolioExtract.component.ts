import { Component } from '@angular/core';
import { ApiService } from '../../../shared/api.service';

@Component({
  selector: 'portfolio-extract',
  templateUrl: './portfolioExtract.component.html',
  styleUrls: ['./portfolioExtract.component.scss']
})
export class PortfolioExtractComponent {
  constructor(public service: ApiService) {
  }

  downloadMySedaPortfolio() {
    this.service.generateMySedaPortfolio().subscribe(
      data => {
            const fileBlob = data;
            const blob = new Blob([fileBlob], {
              type: 'application/pdf' // must match the Accept type
            });

            let pdfUrl = (window.URL || window['webkitURL']).createObjectURL(blob);
            const anchor = document.createElement('a');
            anchor.href = pdfUrl;
            anchor.target = '_blank';
            anchor.download = "MyPLAN-" + this.service.profile.studentName;
            document.body.appendChild(anchor);
            anchor.click();
      });
  }
}
