
import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { ApiService } from '../../shared/api.service';
import { StudentJob } from '../../interfaces/model';

@Component({
  selector: 'my-experience',
  templateUrl: './myExperience.component.html',
  styleUrls: ['./myExperience.component.scss']
})
export class MyExperienceComponent {
  @Input() description: String;
  @Input() type: String;
  @Input() form: NgForm;
  @Input() title: String;

  orgTypes: Array<String> = [];

  constructor(public service: ApiService, private router: Router) {
  }

  addExperience(type: String) {
    let job = new StudentJob();
    job.jobType = type;
    job.endDate = null;
    this.service.profile.studentJobs.push(job);
  }

  goToTeacherView() {
    this.router.navigate(['/teacher']);
  }

}
