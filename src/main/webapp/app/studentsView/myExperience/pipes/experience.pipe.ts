import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'experienceFilter',
  pure: false
})
export class ExperiencePipe implements PipeTransform {
  transform(items: any[], type: String) {
    if (!items) { return items };
    return items.filter(item => item.jobType == type);
  }
}
