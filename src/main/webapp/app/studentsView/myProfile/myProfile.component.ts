import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../shared/api.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'my-profile',
  templateUrl: './myProfile.component.html',
  styleUrls: ['./myProfile.component.scss']
})

export class MyProfileComponent {
  @Input() form: NgForm;

  constructor(public service: ApiService, private router: Router) {
    this.service.form = this.form;
  }
  
  addMember() {
    this.service.profile.familyMembers.push({});
  }

  addNetwork() {
    this.service.profile.studentNetworks.push({});
  }

  goToTeacherView() {
    this.router.navigate(['/teacher']);
  }

}
