import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'admin-add-supervisor',
  templateUrl: './adminAddSupervisor.component.html',
  styleUrls: ['./adminAddSupervisor.component.scss']
})
export class AdminAddSupervisorComponent implements OnInit  {
  roles: Array<any>;

  constructor(public service: ApiService) {
    this.roles = [
                   {id: 1, roleName: 'Admin' }, 
                   {id: 2, roleName: 'Supervisor' } 
                 ];
  }

  ngOnInit() {
    this.service.getSupervisorsList();
  }

  addSupervisor() {
     this.service.supervisors.push({});
  }
}
