
import { RouterModule, Routes } from '@angular/router';

import { LandingPageComponent } from './landing/landing.component';
import { TeacherListComponent } from './teachersView/teacherList.component';
import { ParentsViewComponent } from './parentsView/parentsView.component';
import { StudentsViewComponent } from './studentsView/studentsView.component';
import { SupervisorTeachersListComponent } from './supervisorView/supervisorTeachersList.component';
import { AdminAddSupervisorComponent } from './adminView/adminAddSupervisor.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'teacher', component: TeacherListComponent },
  { path: 'parent', component: ParentsViewComponent },
  { path: 'student', component: StudentsViewComponent },
  { path: 'supervisor', component: SupervisorTeachersListComponent },
  { path: 'admin', component: SupervisorTeachersListComponent },
  { path: 'admin/supervisor/add', component: AdminAddSupervisorComponent },
  { path: 'teacher', component: TeacherListComponent },
];

export const routing = RouterModule.forRoot(routes);
