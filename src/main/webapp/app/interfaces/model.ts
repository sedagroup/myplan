export class Profile {
    id: number;
    studentName: String;
    mybio: String;
    
    classTeacherId: number;
    lastConversationDate: Date;
    
    familyMembers: Array<any> = [];
    studentNetworks: Array<any> = [];
    studentCareerGoals: Array<StudentCareerGoal> = [];
    studentIndustryGoals: Array<StudentIndustryGoal> = [];
    
    studentJobs: Array<StudentJob> = [];
    studentDocuments: Array<any> = [];

    studentHabitGrades: Array<any> = [];
    studentHabitGradeComments: Array<any> = [];
    studentSkillFocusLevels: Array<any> = [];
    studentSkillFocusAreas: Array<any> = [] ;
    studentJobReflections: Array<StudentJobReflection> = [];
    studentSectionCompletions: Array<StudentSectionCompletion> = [];
    studentConversations: Array<any> = [];

    // rubrics 2020
    studentGrades: Array<StudentGrade> = [];
    studentGradeComments: Array<StudentGradeComment> = [];
    

    studentCompletionStatus: MyStudentCompletionStatus = {
        markMyName: false,
        markMyBio: false,
        markMyCareerGoal: false,
        markMyIndustryGoal: false,
        markMyPlacementGoal: false,
    };

    studentCompletionSummary: StudentCompletionSummary = {
        myProfileStudentCompleted: '-',
        myProfileTeacherApproved: '-',
        myGoalStudentCompleted: '-',
        myGoalTeacherApproved: '-',
        myExperienceStudentCompleted: '-',
        myExperienceTeacherApproved: '-',
        myPlacementStudentCompleted: '-',
        myPlacementTeacherApproved: '-',
        mySkillSummaries: [],
    };

    constructor(profile?: Profile) {
        if (profile) {
            this.id = profile.id;
            this.studentName = profile.studentName;
            this.mybio = profile.mybio;          
            this.familyMembers = profile.familyMembers;
            this.studentNetworks = profile.studentNetworks;
            this.studentCareerGoals = profile.studentCareerGoals;
            this.studentIndustryGoals = profile.studentIndustryGoals;
            this.setStudentJobs(profile.studentJobs);
            this.setStudentJobReflections(profile.studentJobReflections);
            this.classTeacherId = profile.classTeacherId;       
            this.studentDocuments = profile.studentDocuments;            
            this.studentHabitGrades = profile.studentHabitGrades;
            this.studentHabitGradeComments = profile.studentHabitGradeComments;
            this.studentSkillFocusLevels = profile.studentSkillFocusLevels;
            this.studentSkillFocusAreas = profile.studentSkillFocusAreas;           
            this.setStudentSectionCompletion(profile.studentSectionCompletions);
            this.setStudentCompletionSummary(profile.studentCompletionSummary);      
            
            // habits 2020
            this.studentGrades = profile.studentGrades;
            this.studentGradeComments = profile.studentGradeComments;
        }
    }

    setStudentJobs(studentJobs: Array<StudentJob>) {
        this.studentJobs = [];
        if (studentJobs) {
            studentJobs.forEach(job => this.studentJobs.push(new StudentJob(job)));
        }
    }

    setStudentJobReflections(studentJobReflections: Array<StudentJobReflection>) {
        this.studentJobReflections = [];
        if (studentJobReflections) {
            studentJobReflections.forEach(reflection => this.studentJobReflections.push(new StudentJobReflection(reflection)));
        }
    }

    setStudentSectionCompletion(studentSectionCompletions: Array<StudentSectionCompletion>) {
        this.studentSectionCompletions = [];
        if (studentSectionCompletions) {
            studentSectionCompletions.forEach(studentCompletion =>
                                                    this.studentSectionCompletions.push(new StudentSectionCompletion(studentCompletion)));
        }
    }

    setStudentCompletionSummary(studentCompletionSummary: StudentCompletionSummary) {
        this.studentCompletionSummary.myProfileStudentCompleted = studentCompletionSummary.myProfileStudentCompleted;
        this.studentCompletionSummary.myProfileTeacherApproved = studentCompletionSummary.myProfileTeacherApproved;
        this.studentCompletionSummary.myGoalStudentCompleted = studentCompletionSummary.myGoalStudentCompleted;
        this.studentCompletionSummary.myGoalTeacherApproved = studentCompletionSummary.myGoalTeacherApproved;
        this.studentCompletionSummary.myExperienceStudentCompleted = studentCompletionSummary.myExperienceStudentCompleted;
        this.studentCompletionSummary.myExperienceTeacherApproved = studentCompletionSummary.myExperienceTeacherApproved;
        this.studentCompletionSummary.myPlacementStudentCompleted = studentCompletionSummary.myPlacementStudentCompleted;
        this.studentCompletionSummary.myPlacementTeacherApproved = studentCompletionSummary.myPlacementTeacherApproved;
        this.studentCompletionSummary.mySkillSummaries = studentCompletionSummary.mySkillSummaries;
    }

    setStudentCompletionStatus(myPlanWeightages) {
        this.studentSectionCompletions.forEach(studentSectionCompletion => {
            if (studentSectionCompletion.completed == true) {
                let section = this.getWeightageSectionNameForId(studentSectionCompletion.myPlanWeightageId, myPlanWeightages);
                if (section == 'Name') {
                    this.studentCompletionStatus.markMyName = true;
                } else if (section == 'MyBIO') {
                    this.studentCompletionStatus.markMyBio = true;
                } else if (section == 'Career Goal') {
                    this.studentCompletionStatus.markMyCareerGoal = true;
                } else if (section == 'Industry Goal') {
                    this.studentCompletionStatus.markMyIndustryGoal = true;
                } else if (section == 'Placement Goal') {
                    this.studentCompletionStatus.markMyPlacementGoal = true;
                }
            }
        });
    }

    getWeightageSectionNameForId(weightageId, myPlanWeightages) {
        for (let i = 0; i < myPlanWeightages.length; i++) {
            if (myPlanWeightages[i]) {
                if (myPlanWeightages[i].id == weightageId) {
                    return myPlanWeightages[i].section;
                }
            }
        }
    }
}

export class StudentCareerGoal {
    id: number;
    studentId: number;
    career: String;
    careerPath: String;
    teacherFeedback: String;

    constructor(studentCareerGoal?: StudentCareerGoal) {
        if (studentCareerGoal) {
            this.id = studentCareerGoal.id;
            this.studentId = studentCareerGoal.studentId;
            this.career = studentCareerGoal.career;
            this.careerPath = studentCareerGoal.careerPath;
            this.teacherFeedback = studentCareerGoal.teacherFeedback;
        }
    }
}

export class StudentIndustryGoal {
    id: number;
    studentId: number;
    goal: String;
    industry: String;
    teacherFeedback: String;

    constructor(studentIndustryGoal?: StudentIndustryGoal) {
        if (studentIndustryGoal) {
            this.id = studentIndustryGoal.id;
            this.studentId = studentIndustryGoal.studentId;
            this.goal = studentIndustryGoal.goal;
            this.industry = studentIndustryGoal.industry;
            this.teacherFeedback = studentIndustryGoal.teacherFeedback;
        }
    }
}

export class StudentJob {
    id: number;
    employerName: String;
    jobType: String;
    jobTitle: String;
    placementGoal: String;
    startDate: Date;
    endDate: Date;
    jobDuties: String;
    totalHours: number;
    startTime: string;
    endTime: string;
    organizationTypeId: number;
    teacherComments: String;
    approved: boolean;

    constructor(studentJob?: StudentJob) {
        if (studentJob) {
            this.id = studentJob.id;
            this.employerName = studentJob.employerName;
            this.jobType = studentJob.jobType;
            this.jobTitle = studentJob.jobTitle;
            this.placementGoal = studentJob.placementGoal;
            this.setStartDate(studentJob.startDate);
            this.setEndDate(studentJob.endDate);
            this.jobDuties = studentJob.jobDuties;
            this.totalHours = studentJob.totalHours;
            this.startTime = studentJob.startTime;
            this.endTime = studentJob.endTime;
            this.organizationTypeId = studentJob.organizationTypeId;
            this.teacherComments = studentJob.teacherComments;
            this.approved = studentJob.approved;
        }
    }

    setStartDate(startDate: any) {
        this.startDate = new Date(startDate);
    }

    setEndDate(endDate: any) {
        if (endDate != null) {
            this.endDate = new Date(endDate);
        }
    }
}

export class StudentJobReflection {
    id: number;
    reflectionDate: Date;
    comments: String;
    approved: boolean;
    transferrableSkill: String;
    isCommunication: boolean;
    isTeamwork: boolean;
    isProblemSolving: boolean;
    isSelfAwareness: boolean;
    isPersonalOrganisation: boolean;

    constructor(studentJobReflection?: StudentJobReflection) {
        if (studentJobReflection) {
            this.id = studentJobReflection.id;
            this.setReflectionDate(studentJobReflection.reflectionDate);
            this.comments = studentJobReflection.comments;
            this.transferrableSkill = studentJobReflection.transferrableSkill;
            this.approved = studentJobReflection.approved;
            this.isCommunication = studentJobReflection.isCommunication;
            this.isTeamwork = studentJobReflection.isTeamwork;
            this.isProblemSolving = studentJobReflection.isProblemSolving;
            this.isSelfAwareness = studentJobReflection.isSelfAwareness;
            this.isPersonalOrganisation = studentJobReflection.isPersonalOrganisation;
        }
    }

    setReflectionDate(reflectionDate: any) {
        this.reflectionDate = new Date(reflectionDate);
    }
}

export class Document {
    id: number;
    documentType: String;
    documentName: String;

    constructor(document?: Document) {
        if (document) {
            this.id = document.id;
            this.documentType = document.documentType;
            this.documentName = document.documentName;
        }
    }
}

export class MyPlanWeightage {
    id: number;
    sectionGroup: String;
    section: String;
    weightage: number;
    weightageType: String;

    constructor(myPlanWeightage?: MyPlanWeightage) {
        if (myPlanWeightage) {
            this.id = myPlanWeightage.id;
            this.sectionGroup = myPlanWeightage.sectionGroup;
            this.section = myPlanWeightage.section;
            this.weightage = myPlanWeightage.weightage;
            this.weightageType = myPlanWeightage.weightageType;
        }
    }
}

export class StudentSectionCompletion {
    id: number;
    myPlanWeightageId: number;
    completed: boolean;
    updatedByUser: String;

    constructor(studentSectionCompletion?: StudentSectionCompletion) {
        if (studentSectionCompletion) {
            this.id = studentSectionCompletion.id;
            this.myPlanWeightageId = studentSectionCompletion.myPlanWeightageId;
            this.completed = studentSectionCompletion.completed;
            this.updatedByUser = studentSectionCompletion.updatedByUser;
        }
    }
}

export interface MyStudentCompletionStatus {
    markMyName: boolean;
    markMyBio: boolean;
    markMyCareerGoal: boolean;
    markMyIndustryGoal: boolean;
    markMyPlacementGoal: boolean;
}

export interface StudentCompletionSummary {
    myProfileStudentCompleted: String;
    myProfileTeacherApproved: String;
    myGoalStudentCompleted: String;
    myGoalTeacherApproved: String;
    myExperienceStudentCompleted: String;
    myExperienceTeacherApproved: String;
    myPlacementStudentCompleted: String;
    myPlacementTeacherApproved: String;
    mySkillSummaries: Array<any>;
}

export interface MyStudentSectionEditable {
    isDisabledMyprofile: boolean;
    isDisabledMyGoals: boolean;
    isDisabledMyPlacement: boolean;
    isDisabledMyExperiences: boolean;
    isDisabledMyPortfolio: boolean;
    isDisabledMySkills: boolean;
  }

  export class StudentSkillFocusArea {
    id: number;
    semesterId: number;
    studentSkillDate: Date;
    updatedByUser: string;
    updatedDate: Date;
    skillFocusAreaId: number;
    teacherComments: string;
  }

  export interface Semester {
    semesterId: number;
    semesterValue: number;
    semesterYear: number;
    startDate: Date;
    endDate: Date;
    appVersion: string;
  }

  export class StudentConversation {
    id: number;
    studentId: number;
    teacherId : number;
    topic: string;
    studentComment: string;
    teacherComment: string;
    conversationStartDate: Date;
    isFinal: boolean;

    constructor(studentConversation?: StudentConversation) {
        if (studentConversation) {
            this.id = studentConversation.id;
            this.studentId = studentConversation.studentId;
            this.teacherId = studentConversation.teacherId;
            this.topic = studentConversation.topic;
            this.studentComment = studentConversation.studentComment;
            this.teacherComment = studentConversation.teacherComment;
            this.conversationStartDate = studentConversation.conversationStartDate;
            this.isFinal = studentConversation.isFinal;
        }
    }
}

// ----- 2020 Rubrics -----------------------------------------------------------------------------

export class StudentGrade {
    id: number;
    studentId: number;
    rubricId: number;
    semesterId: number;
    isTeacherGrade: boolean;
    isStudentGrade: boolean;
    teacherId: number;
    studentMarkDate: Date;
    teacherMarkDate: Date;
  }

  export class StudentGradeComment {
      id: number;
      studentId: number;
      focusAreaId: number;
      semesterId: number;
      studentComment: string;
      teacherComment: string;
      teacherId: number;
      studentCommentDate: Date;
      teacherCommentDate: Date;
  }

// ----- End - 2020 Rubrics ------------------------------------------------------------------------
