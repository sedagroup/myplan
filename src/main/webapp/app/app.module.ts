
// Services
import { ApiService } from './shared';
import { routing } from './app.routing';
import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

// Modules

// core modules
import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Third party modules
import { CalendarModule, FileUploadModule, GrowlModule, DialogModule, SharedModule, EditorModule } from 'primeng/primeng';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';
import { Ng2GoogleChartsModule } from 'ng2-google-charts';

// Pipes
import { ExperiencePipe } from './studentsView/myExperience/pipes/experience.pipe';
import { AreaTypePipe } from './studentsView/mySkills/rubrics_v2/gradeReview/pipes/areaType.pipe';
import { AppVersionPipe } from './studentsView/mySkills/rubrics_v2/gradeReview/pipes/appVersion.pipe';

// Directives
import { RegisterFormModelDirective } from './shared/registerForm.directive';
/* Directive to fix focusing to wysiwyg editor on page load */
import { PeAutoscrollFixDirective } from './shared/PeAutoscrollFix.directive';

// Components
// Third party components
import { NavbarComponent, ExtendedInput, StickyComponent } from './common';

// Application components
import { AppComponent } from './app.component';

import { LandingPageComponent } from './landing/landing.component';
import { TeacherListComponent } from './teachersView/teacherList.component';
import { ParentsViewComponent } from './parentsView/parentsView.component';
import { AdminAddSupervisorComponent } from './adminView/adminAddSupervisor.component';
import { SupervisorTeachersListComponent } from './supervisorView/supervisorTeachersList.component';

import { StudentsViewComponent } from './studentsView/studentsView.component';
import { MyProfileComponent } from './studentsView/myProfile/myProfile.component';

import { MyGoalsComponent } from './studentsView/myGoals/myGoals.component';
import { CareerGoalsComponent } from './studentsView/myGoals/career/careerGoals.component';
import { IndustryGoalsComponent } from './studentsView/myGoals/industry/industryGoals.component';
import { MyPlacementComponent } from './studentsView/myPlacement/myPlacement.component';
import { StudentJobReflectionComponent } from './studentsView/myPlacement/studentJobReflections/studentJobReflection.component';
import { WorkPlacementComponent } from './studentsView/myPlacement/workPlacement/workPlacement.component';
import { MyExperienceComponent } from './studentsView/myExperience/myExperience.component';
import { MyPortfolioComponent } from './studentsView/myPortfolio/myPortfolio.component';
import { PortfolioDocumentsComponent } from './studentsView/myPortfolio/portfolioDocuments/portfolioDocuments.component';
import { PortfolioExtractComponent } from './studentsView/myPortfolio/portfolioExtract/portfolioExtract.component';

import { MySkillsComponent } from './studentsView/mySkills/mySkills.component';
import { RubricsComponent } from './studentsView/mySkills/rubrics/rubrics.component';
import { ReviewComponent } from './studentsView/mySkills/rubrics/review/review.component';
import { PersonalHabitsComponent } from './studentsView/mySkills/rubrics/review/personalHabits/personalHabits.component';
import { WorkSkillsComponent } from './studentsView/mySkills/rubrics/review/workSkills/workSkills.component';
import { LineChartComponent } from './studentsView/mySkills/charts/LineChart.component';

import { GradeReviewComponent } from './studentsView/mySkills/rubrics_v2/gradeReview/gradeReview.component';
import { RubricsV2Component } from './studentsView/mySkills/rubrics_v2/rubrics-v2.component';
import { AreaTypeComponent } from './studentsView/mySkills/rubrics_v2/gradeReview/areaType/areaType.component';

import { MyConversationsComponent } from './studentsView/mySkills/conversations/myConversations.component';



@NgModule({
   imports: [
      BrowserModule,
      HttpClientModule,
      HttpModule,
      FormsModule,
      ReactiveFormsModule,
      CalendarModule,
      routing,
      FileUploadModule,
      SharedModule,
      EditorModule,
      GrowlModule,
      DialogModule,
      BrowserAnimationsModule,
      LoadingBarHttpClientModule,
      PopoverModule.forRoot(),
      LazyLoadImagesModule,
      Ng2GoogleChartsModule
   ],
   declarations: [
      AppComponent,
      StickyComponent,
      NavbarComponent,
      ExtendedInput,
      LandingPageComponent,
      TeacherListComponent,
      ParentsViewComponent,
      SupervisorTeachersListComponent,
      AdminAddSupervisorComponent,
      StudentsViewComponent,
      MyProfileComponent,
      MyGoalsComponent,
      CareerGoalsComponent,
      IndustryGoalsComponent,
      MyPlacementComponent,
      WorkPlacementComponent,
      StudentJobReflectionComponent,
      MyExperienceComponent,
      MyPortfolioComponent,
      PortfolioDocumentsComponent,
      PortfolioExtractComponent,
      MySkillsComponent,
      ReviewComponent,
      GradeReviewComponent,
      LineChartComponent,
      RubricsComponent,
      RubricsV2Component,
      PersonalHabitsComponent,
      WorkSkillsComponent,
      AreaTypeComponent,
      ExperiencePipe,
      AreaTypePipe,
      AppVersionPipe,
      RegisterFormModelDirective,
      PeAutoscrollFixDirective,
      MyConversationsComponent
   ],
   providers: [
      ApiService
   ],
   bootstrap: [
      AppComponent
   ]
})

export class AppModule {
  constructor(public appRef: ApplicationRef) { }
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
