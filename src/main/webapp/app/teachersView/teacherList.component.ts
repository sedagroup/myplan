import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Rx'

import { Profile } from '../interfaces/model';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'teacher-list',
  templateUrl: './teacherList.component.html',
  styleUrls: ['./teacherList.component.scss']
})
export class TeacherListComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  private selectedTeacherId: string;

  constructor(public service: ApiService, private router: Router, private route: ActivatedRoute) {
    this.service.isTeacher = true;
  }
  checkStudentProfile(student: any) {
    this.service.profile = new Profile(student.profile);
    this.service.studentId = student.id;
    this.service.studentName = student.name;
    this.router.navigate(['/student']);
  }

  ngOnInit() {
    this.subscription = this.route.queryParams.subscribe(
      (queryParam: any) => this.selectedTeacherId = queryParam['teacherId']
    );
    this.selectedTeacherId = this.selectedTeacherId === undefined ? null : this.selectedTeacherId;
    this.service.getTeacherStudentList(this.selectedTeacherId);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
