import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../shared/api.service';

@Component({
  selector: 'landingPage',
  template: ``
})
export class LandingPageComponent implements OnInit {
  showStudentsView: Boolean = false;

  constructor(private router: Router, public service: ApiService) {
  }

  ngOnInit() {
    this.service.getDocuments();
    this.service.getUserType().subscribe(
      data => {
        if (data.error) {
          this.service.errorMessages.push({ severity: 'error', summary: 'Error', detail: data.errorMessage });
        } else if (data.data) {
          this.service.getSemesters();  
          if (data.data == 'student') {
            this.service.getProfileByStudentId();
          }
          this.router.navigate(['/' + data.data]); // The request return the route to redirect the user
        }
      },
      err => {
        this.service.errorMessages.push({ severity: 'error', summary: 'Error', detail: err });
        console.log('Error ' + err);
      }
    );
  }
}
