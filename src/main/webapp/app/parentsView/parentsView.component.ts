import { Component } from '@angular/core';
import { ApiService } from '../shared/api.service';
import { Profile } from '../interfaces/model';
import { Router } from '@angular/router';

@Component({
  selector: 'parent-view',
  templateUrl: './parentsView.component.html',
  styleUrls: ['./parentsView.component.scss']
})
export class ParentsViewComponent {
  constructor(public service: ApiService, private router: Router) {
    service.isParent = true;
    service.getParentStudentList();
  }
  checkStudentProfile(student: any) {
    this.service.profile = new Profile(student.profile);
    this.service.studentId = student.id;
    this.service.studentName = student.name;
    this.router.navigate(['/student']);
  }
}
