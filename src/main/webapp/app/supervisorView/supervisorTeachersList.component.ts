import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ApiService } from './../shared/api.service';

@Component({
  selector: 'supervisor-list',
  templateUrl: './supervisorTeachersList.component.html',
  styleUrls: ['./supervisorTeachersList.component.scss']
})

export class SupervisorTeachersListComponent implements OnInit {

  constructor(public service: ApiService, private router: Router) {
    if (this.router.url == '/admin') {
      service.isAdmin = true;
    }
    service.isSupervisor = true;
   }

  ngOnInit() {
    this.service.getSupervisorTeachersList();

  }

}
