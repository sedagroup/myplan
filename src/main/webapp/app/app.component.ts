import { Component } from '@angular/core';
import { Message } from 'primeng/primeng';
import { ApiService } from './shared/api.service';

import '../style/app.scss';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  entryImages: String;
  errorMessages: Message[] = [];
  successMessages: Message[] = [];

  constructor(public service: ApiService) {
    service.errorMessages = this.errorMessages;
    service.successMessages = this.successMessages;
    this.entryImages = 'MyPlan/dist/img/';
    console.log(process.env.NODE_ENV);
    if (process.env.NODE_ENV === 'test') {
      this.entryImages = '/img/';
    }
  }
}
