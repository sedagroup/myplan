package au.com.sedagroup.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.xhtmlrenderer.extend.FSImage;
import org.xhtmlrenderer.extend.ReplacedElement;
import org.xhtmlrenderer.extend.ReplacedElementFactory;
import org.xhtmlrenderer.extend.UserAgentCallback;
import org.xhtmlrenderer.layout.LayoutContext;
import org.xhtmlrenderer.pdf.ITextFSImage;
import org.xhtmlrenderer.pdf.ITextImageElement;
import org.xhtmlrenderer.render.BlockBox;
import org.xhtmlrenderer.simple.extend.FormSubmissionListener;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Image;

public class ImageReplacedElementFactory implements ReplacedElementFactory {

	private final ReplacedElementFactory superFactory;
	private String logoAbsolutePath;
	private String stripeAbsolutePath;

	private Logger logger = LoggerFactory.getLogger(ImageReplacedElementFactory.class);

	public ImageReplacedElementFactory(ReplacedElementFactory superFactory) {
		this.superFactory = superFactory;
	}

	@Override
	public ReplacedElement createReplacedElement(LayoutContext layoutContext, BlockBox blockBox,
			UserAgentCallback userAgentCallback, int cssWidth, int cssHeight) {

		Element element = blockBox.getElement();
		if (element == null) {
			return null;
		}

		String nodeName = element.getNodeName();
		String className = element.getAttribute("class");

		if ("div".equals(nodeName) && className.contains("logo")) {
			// System.out.println("getLogoAbsolutePath() : ===== " + this.getLogoAbsolutePath());
			InputStream input = null;
			FSImage fsImage = null;
			try {
				File imageFile = new File(this.getLogoAbsolutePath());
				input = new FileInputStream(imageFile);

				byte[] bytes = IOUtils.toByteArray(input);
				Image image = Image.getInstance(bytes);
				fsImage = new ITextFSImage(image);

				if (fsImage != null) {
					if ((cssWidth != -1) || (cssHeight != -1)) {
						fsImage.scale(cssWidth, cssHeight);
					}
					return new ITextImageElement(fsImage);
				}

			} catch (IOException e) {
				logger.error(ExceptionUtils.getStackTrace(e));
			} catch (BadElementException e) {
				logger.error(ExceptionUtils.getStackTrace(e));
			} finally {
				IOUtils.closeQuietly(input);
			}
		}

		return superFactory.createReplacedElement(layoutContext, blockBox, userAgentCallback, cssWidth, cssHeight);
	}

	@Override
	public void reset() {
		superFactory.reset();
	}

	@Override
	public void remove(Element e) {
		superFactory.remove(e);
	}

	@Override
	public void setFormSubmissionListener(FormSubmissionListener listener) {
		superFactory.setFormSubmissionListener(listener);
	}

	public String getLogoAbsolutePath() {
		return logoAbsolutePath;
	}

	public void setLogoAbsolutePath(String logoIAbsolutePath) {
		this.logoAbsolutePath = logoIAbsolutePath;
	}

	public String getStripeAbsolutePath() {
		return stripeAbsolutePath;
	}

	public void setStripeAbsolutePath(String stripeAbsolutePath) {
		this.stripeAbsolutePath = stripeAbsolutePath;
	}

}
