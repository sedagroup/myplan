package au.com.sedagroup.util;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.beanutils.PropertyUtils;

import au.com.sedagroup.model.StudentHabitGrade;
import au.com.sedagroup.model.StudentJob;
import au.com.sedagroup.model.StudentProfile;

public class Util {
	
	public static String MYEXPERIENCES_TYPE = "MyExperiences";
	public static String MYPLACEMENTS_TYPE = "MyPlacements";
	
	public static List<String> profileCalcMyProfile = new ArrayList<String>(Stream.of("studentName","familyMembers","studentNetworks","mybio").collect(Collectors.toList()));
	public static List<String> profileCalcMyGoals = new ArrayList<String>(Stream.of("studentCareerGoals", "studentIndustryGoals").collect(Collectors.toList()));
	public static List<String> profileCalcMySkills = new ArrayList<String>(Stream.of("studentHabitGrades","studentSkillFocusLevels").collect(Collectors.toList()));
	public static List<String> profileCalcMyExperience = new ArrayList<String>(Stream.of("studentJobReflections").collect(Collectors.toList()));
	

	public static int getProgressPercentage(Object profile,List<String> fields) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		int fieldsFilled=0;
		if (profile!=null){
			for(String fieldName : fields) {
				Object value = PropertyUtils.getProperty(profile, fieldName);
				if (value!=null && value.getClass().equals(org.hibernate.collection.internal.PersistentSet.class)){
					if(!((org.hibernate.collection.internal.PersistentSet)value).isEmpty()){
						fieldsFilled++;
					}
				} else if(value!=null && !value.toString().isEmpty()){
					fieldsFilled++;
				}
			}
		}
		return (fieldsFilled*100)/fields.size();
	}
	
	public static int getProgressPercentageByNumberOfObjects(Object profile,List<String> fields,int total) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		int fieldsFilled=0;
		if(profile!=null){
			for(String fieldName : fields){
				Object value = PropertyUtils.getProperty(profile, fieldName);
				if(value!=null && value.getClass().equals(org.hibernate.collection.internal.PersistentSet.class)){
					if(!((org.hibernate.collection.internal.PersistentSet)value).isEmpty()){
						fieldsFilled+=((org.hibernate.collection.internal.PersistentSet)value).size();
					}
				}else if(value!=null && !value.toString().isEmpty()){
					fieldsFilled++;
				}
			}
		}
		return (fieldsFilled*100)/total;
	}
	
	// return count of student jobs-hours
	public static Integer getStudentJobHoursByJobType(Set<StudentJob> studentJobs, String value) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Integer totalHours = 0;
		
		for (StudentJob job: studentJobs) {
			if (job.getJobType().equals(value)) {
				if(job.getTotalHours() != null) {
					totalHours += job.getTotalHours();
				}
			}
		}
		
		return totalHours;
	}
	
	/**
	 * 
	 * @param studentJobs
	 * @param jobType
	 * @return
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 */
	public static Integer getApprovedStudentJobHoursByJobType(Set<StudentJob> studentJobs, String jobType) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Integer totalHours = 0;
		
		for (StudentJob job: studentJobs) {
			if (job.getJobType().equals(jobType) && job.getApproved() != null && job.getApproved().booleanValue()) {
				if(job.getTotalHours() != null) {
					totalHours += job.getTotalHours();
				}
			}
		}
		
		return totalHours;
	}
	
	// return count of student jobs-count
	public static Integer getStudentJobCountByJobType(Set<StudentJob> studentJobs, String value) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Integer count = 0;
		
		for (StudentJob job : studentJobs) {
			if (job.getJobType().equals(value)) {
				count++;
			}
		}
		
		return count;
	}
	
}
