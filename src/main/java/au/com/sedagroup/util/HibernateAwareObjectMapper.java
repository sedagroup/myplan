package au.com.sedagroup.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module.Feature;

public class HibernateAwareObjectMapper extends ObjectMapper {
    public HibernateAwareObjectMapper() {
    	//Configure project to not ignore transient annotations
    	Hibernate5Module hm = new Hibernate5Module();
    	hm.disable(Feature.USE_TRANSIENT_ANNOTATION);
        registerModule(hm);
    }
}
