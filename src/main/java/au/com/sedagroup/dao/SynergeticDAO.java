package au.com.sedagroup.dao;

import java.util.List;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import au.com.sedagroup.dto.Student;
import au.com.sedagroup.dto.StudentContactDetails;
import au.com.sedagroup.dto.TeacherSelectedSkillFocusArea;
import au.com.sedagroup.model.Supervisor;
import au.com.sedagroup.model.Teacher;

public class SynergeticDAO extends JdbcDaoSupport {
	
	/**
	 * Check if the id passed as parameter is a student.
	 * @param id
	 * @return
	 */
	public String getUsertype(int id){		
		return getJdbcTemplate().queryForObject("select SEDAMyPlan.dbo.uGetUsertypeById(?)", String.class, id);
	}
	
	/**
	 * Get class code of a teacher.
	 * @param staffId
	 * @return
	 */
	public List<String> getTeacherClass(int staffId){
		
		/**
		 * Uncomment this for NSW to remove duplicate students for the teachers class
		 *  
		String teacherClassQuery = " SELECT DISTINCT sc.ClassCode FROM SynergyOne.dbo.SubjectClassStaff scs "+
                	   " INNER JOIN SynergyOne.dbo.SubjectClasses sc ON scs.ClassCode = sc.ClassCode "+
			           " INNER JOIN SynergyOne.dbo.uvConfig uv ON scs.FileYear = uv.FileYear AND scs.FileSemester = uv.FileSemester "+
                       " WHERE sc.Description LIKE '%Class Homepage%' AND scs.StaffID = ? ";*/
       
       
        
		
		  String teacherClassQuery = " SELECT classCode FROM SynergyOne.dbo.SubjectClassStaff scs "+
				  					 " INNER JOIN SynergyOne.dbo.uvConfig uv ON scs.FileYear = uv.FileYear AND scs.FileSemester = uv.FileSemester "+
				                     " WHERE scs.ClassCode NOT LIKE '%.%' AND scs.ClassCode NOT LIKE '%VET%' AND scs.StaffID = ? ";
		 
		List<String> teacherClass = getJdbcTemplate().queryForList(teacherClassQuery, String.class, staffId);
		return teacherClass;
	}
	
	/**
	 * Get list of students by a class.
	 * @param classCode
	 * @return
	 */
	public List<Student> getStudentsByClass(String classCode){
		 String studentsByClassQuery = " SELECT sc.StudentID 'id', sc.StudentNameExternal 'name' FROM SynergyOne.dbo.vStudentSubjectClass sc "+
	                    " INNER JOIN SynergyOne.dbo.uvConfig uv ON sc.FileYear = uv.FileYear AND sc.FileSemester = uv.FileSemester "+
				        " WHERE sc.StudentStatus <> 'LFT' AND ClassCode= ? ";

		return getJdbcTemplate().query(studentsByClassQuery, new BeanPropertyRowMapper<Student>(Student.class), classCode);
	}
	
	/**
	 * Get a list of students by parent id
	 * @param parentId
	 * @return
	 */
	public List<Student> getStudentsByParentId(int parentId){
		String studentsByParentIdQuery = " SELECT s.ID, com.Given1+' '+com.Given2+' '+com.Surname 'name' FROM SynergyOne.dbo.Constituencies c "+
										 " INNER JOIN SynergyOne.dbo.Relationships re ON c.ID = re.ID "+
				                         " INNER JOIN SynergyOne.dbo.Students s ON re.RelatedID = s.ID "+
										 " INNER JOIN SynergyOne.dbo.Community com ON s.ID = com.ID WHERE ConstitCode='@PC' AND c.ID = ? ";
		
		return getJdbcTemplate().query(studentsByParentIdQuery, new BeanPropertyRowMapper<Student>(Student.class), parentId);
	}
	
	/**
	 * Get the list of all teachers
	 * @return
	 */
	public List<Teacher> getAllTeachers(){
		String allTeachersQuery =  "SELECT t.id, t.firstName, t.surname, t.ClassCampus FROM "+
						" ( SELECT DISTINCT sc.StaffID id, c.Preferred firstName, c.Surname surname, sc.ClassCampus "+
						"   FROM SynergyOne.dbo.SubjectClassStaff sc " + 
						"	INNER JOIN SynergyOne.dbo.uvConfig u ON sc.FileYear = u.FileYear AND sc.FileSemester = u.FileSemester " + 
						"	INNER JOIN SynergyOne.dbo.Community c ON sc.StaffID = c.ID " + 
						"	WHERE sc.ClassCode NOT LIKE '%.%'  ) t " + 
						"ORDER BY t.ClassCampus, t.firstName, t.surname " ;
		return getJdbcTemplate().query(allTeachersQuery, new BeanPropertyRowMapper<Teacher>(Teacher.class));
	}
	
	/**
	 * Get whether the currentDate is between termStartDate and termEndDate
	 * @param studentId
	 * @return
	 */
	public Boolean isValidTermDate(Integer studentId) {
		// TODO - find the usage - remove or put the function inside MyPlan
		return getJdbcTemplate().queryForObject("SELECT SCAR.dbo.getValidTermDate(?)", Boolean.class, studentId);
	}
	
	
	/**
	 * Get the list of active Admins/Supervisors
	 * @return
	 */
	public List<Supervisor> getSupervisors(){
		String adminsAndSupervisorsQuery =  "SELECT t.id, t.firstName, t.surname, t.roleId FROM "+
						" ( SELECT DISTINCT c.id, c.Preferred firstName, c.Surname surname, u.roleId "+
						"   FROM SEDAMyPlan.dbo.Users u " + 
						"	INNER JOIN SynergyOne.dbo.Community c ON u.SynergyId = c.ID " + 
						"	WHERE u.DeletedAt IS NULL ) t " + 
						" ORDER BY t.firstName, t.surname " ;
		
		return getJdbcTemplate().query(adminsAndSupervisorsQuery, new BeanPropertyRowMapper<Supervisor>(Supervisor.class));
	}
	
	public List<TeacherSelectedSkillFocusArea> getTeacherSelectedSkillFocusAreas(Integer studentId){
		String query =  " SELECT DISTINCT sa.Id AS skillAreaId, sa.SkillArea, sfa.FocusAreaSummary, ssfa.SemesterId, sfl.SkillLevel, sfl.SkillLevelValue, ssfl.StudentId, ssfl.isTeacherGrade "+
				" FROM SEDAMyPlan.dbo.StudentSkillFocusLevels ssfl " +
				" INNER JOIN SEDAMyPlan.dbo.SkillFocusLevels sfl ON sfl.Id = ssfl.SkillFocusLevelId " +
				" INNER JOIN SEDAMyPlan.dbo.SkillFocusAreas  sfa ON sfa.id = sfl.SkillFocusAreaId " +
				" INNER JOIN SEDAMyPlan.dbo.SkillAreas sa ON sa.Id = sfa.SkillAreaId " +
				" GROUP BY sa.SkillArea, skillAreaId,  sfa.FocusAreaSummary, ssfl.SemesterId, sfl.SkillLevel, sfl.SkillLevelValue, ssfl.StudentId, ssfl.isTeacherGrade, sa.Id " +
				" HAVING ssfl.StudentId = ? AND ssfl.isTeacherGrade = 1 " +
				" ORDER BY sa.SkillArea, sfa.FocusAreaSummary, ssfl.SemesterId ";
		
		return getJdbcTemplate().query(query, new BeanPropertyRowMapper<TeacherSelectedSkillFocusArea>(TeacherSelectedSkillFocusArea.class), studentId);
	}
	
	/**
	 * Get Student Contact Details
	 * @param studentId
	 * @return
	 */
	public List<StudentContactDetails> getStudentContactDetailsById(int studentId){		
		String query = "SELECT (CASE WHEN c.OccupEmail IS NULL OR c.OccupEmail ='' THEN c.Email ELSE c.OccupEmail END) AS Email, c.MobilePhone AS phone FROM SynergyOne.dbo.Community c WHERE c.ID = ? ";
		return getJdbcTemplate().query(query, new BeanPropertyRowMapper<StudentContactDetails>(StudentContactDetails.class), studentId);
	}
	
	public Integer getClassTeacherByStudentId(int studentId) {		
		/*
		 * * Use this query for NSW
		 * TODO - check if joining SubjectClasses is necessary 
		 * 
		String classTeacherByStudentIdQuery = " SELECT s.ID StaffId FROM SynergyOne.dbo.vStudentSubjectClass u "+
                          " INNER JOIN SynergyOne.dbo.SubjectClasses sc on sc.FileYear = u.FileYear and sc.FileSemester = u.FileSemester and sc.ClassCode = u.ClassCode "+
                          " INNER JOIN SynergyOne.dbo.Staff s ON s.ID = u.staffId "+
                          " INNER JOIN SynergyOne.dbo.uvConfig uv ON uv.FileYear = u.FileYear AND uv.FileSemester = u.FileSemester "+
                          " WHERE sc.Description LIKE '%Class Homepage%'  AND u.StudentID = ? ";
                          */

		 String classTeacherByStudentIdQuery =
		 " SELECT s.ID StaffId FROM SynergyOne.dbo.vStudentSubjectClass u "+
		 " INNER JOIN SynergyOne.dbo.Staff s ON s.ID = u.staffId "+
		 " INNER JOIN SynergyOne.dbo.uvConfig uv ON uv.FileYear = u.FileYear AND uv.FileSemester = u.FileSemester "+
		 " WHERE ClassCode NOT LIKE '%.%' AND u.StudentID = ? ";
		
		return (Integer) getJdbcTemplate().queryForObject(classTeacherByStudentIdQuery, Integer.class, studentId);
	}
	
}
