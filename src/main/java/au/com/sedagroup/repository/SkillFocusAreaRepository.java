package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import au.com.sedagroup.model.SkillFocusArea;

public interface SkillFocusAreaRepository extends CrudRepository<SkillFocusArea, Integer>{
	
	@Query(value="select sfa from SkillFocusArea sfa where sfa.deletedAt is null")
	List<SkillFocusArea> findAllActive();
}
