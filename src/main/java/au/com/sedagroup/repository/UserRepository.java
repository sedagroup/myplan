package au.com.sedagroup.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import au.com.sedagroup.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	@Query(value = "select u from User u where u.synergyId = :synergyId and u.deletedAt is null")
	User findByUserId(@Param("synergyId") Integer synergyId);
}
