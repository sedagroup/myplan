package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.AreaType;

public interface AreaTypeRepository extends CrudRepository<AreaType, Integer>{
} 
