package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentDocument;


public interface StudentDocumentRepository extends CrudRepository<StudentDocument, Integer>{

	public StudentDocument findById(Integer id);
}
