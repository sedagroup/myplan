package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentProfile;

public interface StudentProfileRepository extends CrudRepository<StudentProfile, Integer>{
	
}
