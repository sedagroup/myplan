package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;
import au.com.sedagroup.model.StudentDocumentData;

public interface StudentDocumentDataRepository extends CrudRepository<StudentDocumentData, Integer>{
}
