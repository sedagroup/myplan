package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.Rubric;

public interface RubricRepository extends CrudRepository<Rubric, Integer>{

}
