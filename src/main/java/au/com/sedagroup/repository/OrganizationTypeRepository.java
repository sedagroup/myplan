package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.OrganizationType;

public interface OrganizationTypeRepository extends CrudRepository<OrganizationType, Integer>{ 
	public List<OrganizationType> findAllByOrderByOrganizationTypeAsc();	
}
