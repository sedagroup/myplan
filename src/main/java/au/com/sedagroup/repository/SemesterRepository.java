package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import au.com.sedagroup.model.Semester;

public interface SemesterRepository extends CrudRepository<Semester, Integer>{ 
	
	public List<Semester> findAllByOrderBySemesterIdAsc();
	
}
