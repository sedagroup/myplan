package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.MyPlanWeightage;

public interface MyPlanWeightageRepository extends CrudRepository<MyPlanWeightage, Integer>{ 
	MyPlanWeightage findBySection(String sectionName);
}

