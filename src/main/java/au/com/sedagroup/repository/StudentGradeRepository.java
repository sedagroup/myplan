package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentGrade;

public interface StudentGradeRepository extends CrudRepository<StudentGrade, Integer>{ 
}
