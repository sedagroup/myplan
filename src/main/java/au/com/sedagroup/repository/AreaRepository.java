package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.Area;

public interface AreaRepository extends CrudRepository<Area, Integer>{
	List<Area> findAllByOrderBySortOrderAsc();
	List<Area> findAllByOrderByAreaTypeId();
	// List<HabitSkill> findAllByHabitSkillType();
} 
