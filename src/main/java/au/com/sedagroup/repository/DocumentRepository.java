package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.Document;



public interface DocumentRepository extends CrudRepository<Document, Integer>{ 
	Document findByDocumentType(String documentType);
}
