package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import au.com.sedagroup.model.Habit;

public interface HabitRepository extends CrudRepository<Habit, Integer>{ 
	public List<Habit> findAllByOrderByIdAsc();
	
	public final static String FIND_TEACHER_APPROVED_STUDENT_HABIT_GRADES = " SELECT COUNT(shg.id) FROM StudentHabitGrade shg " +
																			" INNER JOIN Habit hb ON hb.id = shg.habit.id "+ 
																			" WHERE shg.studentProfile.id = :studentId AND shg.semesterId = :semesterId " + 
																			" AND shg.isTeacherGrade = true ";
	@Query(FIND_TEACHER_APPROVED_STUDENT_HABIT_GRADES)
	public Integer findTeacherApprovedStudentHabitGrades(@Param("studentId") Integer studentId, @Param("semesterId") Integer semesterId);
		
}
