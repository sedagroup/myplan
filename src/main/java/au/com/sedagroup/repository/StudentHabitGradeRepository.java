package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentHabitGrade;

public interface StudentHabitGradeRepository extends CrudRepository<StudentHabitGrade, Integer> {

}
