package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.SkillArea;

public interface SkillAreaRepository extends CrudRepository<SkillArea, Integer>{
	List<SkillArea> findAllByOrderByIdAsc();
}
