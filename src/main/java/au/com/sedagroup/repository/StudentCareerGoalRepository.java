package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentCareerGoal;

public interface StudentCareerGoalRepository extends CrudRepository<StudentCareerGoal, Integer>{ 
}
