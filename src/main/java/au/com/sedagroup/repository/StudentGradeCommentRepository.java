package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentGradeComment;

public interface StudentGradeCommentRepository extends CrudRepository<StudentGradeComment, Integer> {

}
