package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.FocusArea;
import au.com.sedagroup.model.SkillFocusArea;

public interface FocusAreaRepository extends CrudRepository<FocusArea, Integer>{
	
	@Query(value="select fa from FocusArea fa where fa.deletedAt is null")
	List<SkillFocusArea> findAllActive();
}
