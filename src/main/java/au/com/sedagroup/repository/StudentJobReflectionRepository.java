package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentJobReflection;

public interface StudentJobReflectionRepository extends CrudRepository<StudentJobReflection, Integer>{

}
