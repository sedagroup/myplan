package au.com.sedagroup.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import au.com.sedagroup.model.StudentSectionCompletion;

public interface StudentSectionCompletionRepository extends CrudRepository<StudentSectionCompletion, Integer>{ 
	
   public final static String FIND_TEACHER_APPROVED_PECENTAGE_BY_SECTION_GROUP = "SELECT SUM(wt.weightage) " +
	  " FROM StudentSectionCompletion st " +
	  " INNER JOIN MyPlanWeightage wt ON wt.id = st.myPlanWeightageId " +
	  " WHERE st.studentProfile.id = :studentId  AND st.completed = 1 AND st.deletedAt IS NULL " +
	  " AND wt.sectionGroup = :sectionGroup";
	
	public final static String FIND_TEACHER_APPROVED_RECORD_BY_SECTION = "SELECT COUNT(st.id) FROM StudentSectionCompletion st " +
			  " INNER JOIN MyPlanWeightage wt ON wt.id = st.myPlanWeightageId " +
			  " WHERE st.studentProfile.id = :studentId AND st.completed = 1 AND st.deletedAt IS NULL " +
			  " AND wt.section = :section GROUP BY st.studentProfile.id";
	
	 @Query(FIND_TEACHER_APPROVED_PECENTAGE_BY_SECTION_GROUP)
	 public String findPecentageCompletionBySectionGroup(@Param("studentId") Integer studentId, @Param("sectionGroup") String sectionGroup);
	 
	 @Query(FIND_TEACHER_APPROVED_RECORD_BY_SECTION)
	 public String findTeacherApprovedRecordsBySection(@Param("studentId") Integer studentId, @Param("section") String section);
	 
	 StudentSectionCompletion findById(Integer id);
}
