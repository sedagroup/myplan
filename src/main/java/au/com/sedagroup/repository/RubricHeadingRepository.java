package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import au.com.sedagroup.model.Habit;
import au.com.sedagroup.model.RubricHeading;

public interface RubricHeadingRepository extends CrudRepository<RubricHeading, Integer>{ 
	public List<RubricHeading> findAllByOrderByIdAsc();
}
