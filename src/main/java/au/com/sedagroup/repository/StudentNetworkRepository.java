package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentNetwork;

public interface StudentNetworkRepository extends CrudRepository<StudentNetwork, Integer>{

}
