package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentHabitGradeComment;

public interface StudentHabitGradeCommentRepository extends CrudRepository<StudentHabitGradeComment, Integer> {

}
