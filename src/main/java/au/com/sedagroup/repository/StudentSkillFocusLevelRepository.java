package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentSkillFocusLevel;

public interface StudentSkillFocusLevelRepository extends CrudRepository<StudentSkillFocusLevel, Integer>{

}
