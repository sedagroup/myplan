package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.model.StudentSkillFocusArea;

public interface StudentSkillFocusAreaRepository extends CrudRepository<StudentSkillFocusArea, Integer>{
	StudentSkillFocusArea findBySkillFocusAreaIdAndStudentProfile(Integer skillFocusAreaId, StudentProfile profile);

}
