package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.FamilyMember;

public interface FamilyMemberRepository extends CrudRepository<FamilyMember, Integer>{

}
