package au.com.sedagroup.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.Grade;

public interface GradeRepository extends CrudRepository<Grade, Integer>{ 
	public List<Grade> findAllByOrderBySortValueAsc();
}
