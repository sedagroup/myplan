package au.com.sedagroup.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import au.com.sedagroup.model.StudentConversation;
import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.model.User;

public interface StudentConversationRepository extends CrudRepository<StudentConversation, Integer>{
		
	@Query(value = "select MAX(sc.UpdatedDate) from SEDAMyPlan.dbo.StudentConversations sc where sc.StudentId = :studentId and sc.deletedAt is null", nativeQuery = true)
	Date findMaxUpdatedDateByStudentId(@Param("studentId") Integer studentId);
	
	@Query(value = "select * from SEDAMyPlan.dbo.StudentConversations sc where sc.StudentId = :studentId", nativeQuery = true)
	List<StudentConversation> findByStudentIdOrderByConversationStartDateDesc(@Param("studentId") Integer studentId);
		
}
