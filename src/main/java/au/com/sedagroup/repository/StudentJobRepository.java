package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentJob;

public interface StudentJobRepository extends CrudRepository<StudentJob, Integer>{

}
