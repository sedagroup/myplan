package au.com.sedagroup.repository;

import org.springframework.data.repository.CrudRepository;

import au.com.sedagroup.model.StudentIndustryGoal;

public interface StudentIndustryGoalRepository extends CrudRepository<StudentIndustryGoal, Integer>{ 
}
