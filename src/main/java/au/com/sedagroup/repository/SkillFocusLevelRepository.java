package au.com.sedagroup.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import au.com.sedagroup.model.SkillFocusLevel;

public interface SkillFocusLevelRepository extends CrudRepository<SkillFocusLevel, Integer>{ 
	
	public final static String FIND_TEACHER_APPROVED_STUDENT_SKILL_FOUCUSLEVELS = " SELECT COUNT(skfl.id) FROM SkillFocusLevel skfl "+
																				  " INNER JOIN StudentSkillFocusLevel stfl ON skfl.id = stfl.skillFocusLevel.id "+
																				  " WHERE stfl.studentProfile.id = :studentId AND stfl.semesterId = :semesterId "+
																				  " AND stfl.isTeacherGrade = true ";
	
	@Query(FIND_TEACHER_APPROVED_STUDENT_SKILL_FOUCUSLEVELS)
	public Integer findTeacherApprovedStudentSkillFocusLevels(@Param("studentId") Integer studentId, @Param("semesterId") Integer semeseterId);
}
