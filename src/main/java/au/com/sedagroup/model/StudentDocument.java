package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentDocuments", schema = "dbo", catalog = "SEDAMyPlan")
@DynamicUpdate
@DynamicInsert
public class StudentDocument extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 996182573856046045L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DocumentId", nullable = false)
	private Document document;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;
	
	@Column(name = "ContentType", length = 150)
	private String contentType;

	@Column(name = "DocumentName", length = 200)
	private String name;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;
	
	// FetchType.LAZY is a must here. docData may have large size of data which we do not need in the front end
	// http://justonjava.blogspot.com/2010/09/lazy-one-to-one-and-one-to-many.html
	// The simplest one is to fake one-to-many relationship. This will work because lazy loading of collection is much easier then lazy loading of single nullable property but generally this solution is very inconvenient if you use complex JPQL/HQL queries. 
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentDocument", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<StudentDocumentData> studentDocumentDataList = new HashSet<StudentDocumentData>(0);
	
	public Set<StudentDocumentData> getStudentDocumentDataList() {
		return studentDocumentDataList;
	}

	public void setStudentDocumentDataList(Set<StudentDocumentData> studentDocumentDataList) {
		this.studentDocumentDataList = studentDocumentDataList;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Document getDocument() {
		return document;
	}

	public void setDocument(Document document) {
		this.document = document;
	}

	public StudentProfile getStudentProfile() {
		return studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
