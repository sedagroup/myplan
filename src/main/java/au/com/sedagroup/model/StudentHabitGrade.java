package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentHabitGrades", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentHabitGrade extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 8907482791959700937L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "GradeId", nullable = false)
	private Grade grade;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "HabitId", nullable = false)
	private Habit habit;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "GradedDate", nullable = false, length = 23)
	private Date gradedDate;

	@Column(name = "SemesterId", nullable = false)
	private Integer semesterId;

	@Column(name = "TeacherId", nullable = true)
	private Integer teacherId;

	@Column(name = "IsTeacherGrade", nullable = true)
	private Boolean isTeacherGrade;

	@Column(name = "IsStudentGrade", nullable = true)
	private Boolean isStudentGrade;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	public StudentHabitGrade() {
	}

	public StudentHabitGrade(Grade grade, Habit habit, Date gradedDate, int semesterId, Boolean isTeacherGrade, Boolean isStudentGrade) {
		this.grade = grade;
		this.habit = habit;
		this.gradedDate = gradedDate;
		this.semesterId = semesterId;
		this.isTeacherGrade = isTeacherGrade;
		this.isStudentGrade = isStudentGrade;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Grade getGrade() {
		return this.grade;
	}

	public void setGrade(Grade grade) {
		this.grade = grade;
	}

	public Habit getHabit() {
		return this.habit;
	}

	public void setHabit(Habit habit) {
		this.habit = habit;
	}

	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

	public Date getGradedDate() {
		return this.gradedDate;
	}

	public void setGradedDate(Date gradedDate) {
		this.gradedDate = gradedDate;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public Boolean getIsTeacherGrade() {
		return isTeacherGrade;
	}

	public void setIsTeacherGrade(Boolean isTeacherGrade) {
		this.isTeacherGrade = isTeacherGrade;
	}

	public Boolean getIsStudentGrade() {
		return isStudentGrade;
	}

	public void setIsStudentGrade(Boolean isStudentGrade) {
		this.isStudentGrade = isStudentGrade;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
