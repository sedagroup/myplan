package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentSkillFocusAreas", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentSkillFocusArea extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -8778196466712002618L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	// To get the skillFocusAreaId in the response result(since JsonIgnore is applied for join column)
	@Column(name = "SkillFocusAreaId", insertable = false, updatable = false)
	private Integer skillFocusAreaId;
	
	@Column(name = "SemesterId", nullable = true)
	private int semesterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "StudentSkillDate", nullable = false, length = 23)
	private Date studentSkillDate;

	@Column(name = "TeacherComments", length = 1000)
	private String teacherComments;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SkillFocusAreaId", nullable = false)
	@JsonIgnore
	private SkillFocusArea skillFocusArea;
	
	public StudentSkillFocusArea() {
	}

	public StudentSkillFocusArea(SkillFocusArea skillFocusArea, int semesterId, Date studentSkillDate, String teacherComments) {
		this.skillFocusArea = skillFocusArea;
		this.semesterId = semesterId;
		this.studentSkillDate = studentSkillDate;
		this.teacherComments = teacherComments;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSkillFocusAreaId() {
		return this.skillFocusAreaId;
	}

	public void setSkillFocusAreaId(Integer skillFocusAreaId) {
		this.skillFocusAreaId = skillFocusAreaId;
	}
	
	public int getSemesterId() {
		return semesterId;
	}
	
	public void setSemesterId(int semesterId) {
		this.semesterId = semesterId;
	}

	public Date getStudentSkillDate() {
		return this.studentSkillDate;
	}

	public void setStudentSkillDate(Date studentSkillDate) {
		this.studentSkillDate = studentSkillDate;
	}

	public String getTeacherComments() {
		return teacherComments;
	}

	public void setTeacherComments(String teacherComments) {
		this.teacherComments = teacherComments;
	}

	public SkillFocusArea getSkillFocusArea() {
		return skillFocusArea;
	}

	public void setSkillFocusArea(SkillFocusArea skillFocusArea) {
		this.skillFocusArea = skillFocusArea;
	}

	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

}
