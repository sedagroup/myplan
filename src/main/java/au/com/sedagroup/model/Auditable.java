package au.com.sedagroup.model;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class Auditable<U> {

    @CreatedBy
    @Column(name = "CreatedByUser", nullable= true, updatable = false)
    private U createdByUser;

    @CreatedDate
    @Column(name = "CreatedDate", nullable= true, updatable = false)
    private Date createdDate;

    @LastModifiedBy
    @Column(name = "UpdatedByUser", nullable= true)
    private U updatedByUser;

    @LastModifiedDate
    @Column(name = "UpdatedDate", nullable= true)
    private Date updatedDate;

    protected U getCreatedByUser() {
		return createdByUser;
	}

	protected void setCreatedByUser(U createdByUser) {
		this.createdByUser = createdByUser;
	}

	protected Date getCreatedDate() {
		return createdDate;
	}
	
	protected void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	protected U getUpdatedByUser() {
		return updatedByUser;
	}

	protected void setUpdatedByUser(U updatedByUser) {
		this.updatedByUser = updatedByUser;
	}

	protected Date getUpdatedDate() {
		return updatedDate;
	}

	protected void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}
