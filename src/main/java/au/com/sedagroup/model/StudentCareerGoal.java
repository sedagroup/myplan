package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentCareerGoals", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentCareerGoal extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;
	
	@Column(name = "Career", length = 1000, nullable = false)
	private String career;
	
	@Column(name = "CareerPath", length = 1000, nullable = false)
	private String careerPath;	
	
	@Column(name = "TeacherFeedback", length = 1000, nullable = true)
	private String teacherFeedback;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;	
	
		    		  
	public StudentCareerGoal() {
	}

	public StudentCareerGoal(StudentProfile studentProfile, String career, String careerPath) {
		this.studentProfile = studentProfile;
		this.career = career;
		this.careerPath = careerPath;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

	public String getCareer() {
		return career;
	}
	
	public void setCareer(String career) {
		this.career = career;
	}
	
	public String getCareerPath() {
		return careerPath;
	}
	
	public void setCareerPath(String careerPath) {
		this.careerPath = careerPath;
	}

	public String getTeacherFeedback() {
		return teacherFeedback;
	}

	public void setTeacherFeedback(String teacherFeedback) {
		this.teacherFeedback = teacherFeedback;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	
}
