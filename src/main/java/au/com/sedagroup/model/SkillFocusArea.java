package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SkillFocusAreas", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SkillFocusArea extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -2322037636939547205L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "FocusArea", nullable = false, length = 1000)
	private String focusArea;
	
	@Column(name = "FocusAreaSummary", nullable = true, length = 1000)
	private String focusAreaSummary;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SkillAreaId", nullable = false)
	private SkillArea skillArea;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "skillFocusArea")
	@OrderBy(clause = "skillLevelValue ASC")
	private Set<SkillFocusLevel> skillFocusLevels = new HashSet<SkillFocusLevel>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "skillFocusArea")
	private Set<StudentSkillFocusArea> studentSkillFocusAreas = new HashSet<StudentSkillFocusArea>(0);

	public SkillFocusArea() {
	}

	public SkillFocusArea(SkillArea skillArea, String focusArea) {
		this.skillArea = skillArea;
		this.focusArea = focusArea;
	}

	public SkillFocusArea(SkillArea skillArea, String focusArea, Date deletedAt,
			Set<SkillFocusLevel> skillFocusLevels, Set<StudentSkillFocusArea> studentSkillFocusAreas) {
		this.skillArea = skillArea;
		this.focusArea = focusArea;
		this.deletedAt = deletedAt;
		this.skillFocusLevels = skillFocusLevels;
		this.studentSkillFocusAreas = studentSkillFocusAreas;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SkillArea getSkillArea() {
		return this.skillArea;
	}

	public void setSkillArea(SkillArea skillArea) {
		this.skillArea = skillArea;
	}

	public String getFocusArea() {
		return this.focusArea;
	}

	public void setFocusArea(String focusArea) {
		this.focusArea = focusArea;
	}
	
	public String getFocusAreaSummary() {
		return focusAreaSummary;
	}
	
	public void setFocusAreaSummary(String focusAreaSummary) {
		this.focusAreaSummary = focusAreaSummary;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Set<SkillFocusLevel> getSkillFocusLevels() {
		return this.skillFocusLevels;
	}

	public void setSkillFocusLevels(Set<SkillFocusLevel> skillFocusLevels) {
		this.skillFocusLevels = skillFocusLevels;
	}

	public Set<StudentSkillFocusArea> getStudentSkillFocusAreas() {
		return studentSkillFocusAreas;
	}

	public void setStudentSkillFocusAreas(Set<StudentSkillFocusArea> studentSkillFocusAreas) {
		this.studentSkillFocusAreas = studentSkillFocusAreas;
	}

}
