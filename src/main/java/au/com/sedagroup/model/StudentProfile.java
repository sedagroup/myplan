package au.com.sedagroup.model;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.OrderBy;

import au.com.sedagroup.util.Util;

@Entity
@Table(name = "StudentProfiles", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentProfile extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 3319156966066466401L;

	@Id
	@Column(name = "Id", unique = true, nullable = false)
	private int id;
	
	@Column(name = "StudentName", length = 1000)
	private String studentName;

	@Column(name = "Mybio", length = 4000)
	private String mybio;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;
	
	@Transient
	private Integer classTeacherId;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<FamilyMember> familyMembers = new HashSet<FamilyMember>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<StudentNetwork> studentNetworks = new HashSet<StudentNetwork>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "updatedDate DESC")
	private Set<StudentCareerGoal> studentCareerGoals = new HashSet<StudentCareerGoal>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "updatedDate DESC")
	private Set<StudentIndustryGoal> studentIndustryGoals = new HashSet<StudentIndustryGoal>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "startDate DESC")
	private Set<StudentJob> studentJobs = new HashSet<StudentJob>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "reflectionDate DESC")
	private Set<StudentJobReflection> studentJobReflections = new HashSet<StudentJobReflection>(0);

	// Here we do not save/update studentDocuments through the profile.save, So do not put CascadeType.ALL
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", orphanRemoval = true)
	private Set<StudentDocument> studentDocuments = new HashSet<StudentDocument>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<StudentSectionCompletion> studentSectionCompletions = new HashSet<StudentSectionCompletion>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "semesterId, habitId , gradeId ASC")
	private Set<StudentHabitGrade> studentHabitGrades = new HashSet<StudentHabitGrade>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "semesterId, habitId ASC")
	private Set<StudentHabitGradeComment> studentHabitGradeComments = new HashSet<StudentHabitGradeComment>(0);

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "semesterId, skillFocusAreaId ASC")
	private Set<StudentSkillFocusArea> studentSkillFocusAreas = new HashSet<StudentSkillFocusArea>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "semesterId, skillFocusLevelId ASC")
	private Set<StudentSkillFocusLevel> studentSkillFocusLevels = new HashSet<StudentSkillFocusLevel>(0);
	
	// ============  new 2020 rubrics - collections ===================================
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "semesterId, rubricId ASC")
	private Set<StudentGrade> studentGrades = new HashSet<StudentGrade>(0);
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "studentProfile", cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy(clause = "semesterId, areaId ASC")
	private Set<StudentGradeComment> studentGradeComments = new HashSet<StudentGradeComment>(0);
	
	// ========== end - new 2020 rubrics - collections ==================================
	
	/*
	@Version
	private short version;
	*/
	
	@Transient
	private Set<StudentConversation>  studentConversations = new HashSet<StudentConversation>(0);	 

	@Transient
	private int mySkillsSummary;

	@Transient
	private StudentCompletionSummary studentCompletionSummary = new StudentCompletionSummary();
	
	@Transient
	private Date lastConversationDate ;

	public StudentProfile() {
	}

	public StudentProfile(int id, String studentName, String mybio) {
		this.id = id;
		this.studentName = studentName;
		this.mybio = mybio;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStudentName() {
		return this.studentName;
	}
	
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getMybio() {
		return mybio;
	}

	public void setMybio(String mybio) {
		this.mybio = mybio;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	
	public Integer getClassTeacherId() {
		return classTeacherId;
	}
	
	public void setClassTeacherId(Integer classTeacherId) {
		this.classTeacherId = classTeacherId;
	}

	public Set<FamilyMember> getFamilyMembers() {
		return this.familyMembers;
	}

	public void setFamilyMembers(Set<FamilyMember> familyMembers) {
		this.familyMembers = familyMembers;
	}

	public Set<StudentHabitGrade> getStudentHabitGrades() {
		return this.studentHabitGrades;
	}

	public void setStudentHabitGrades(Set<StudentHabitGrade> studentHabitGrades) {
		this.studentHabitGrades = studentHabitGrades;
	}
	
	public Set<StudentHabitGradeComment> getStudentHabitGradeComments() {
		return studentHabitGradeComments;
	}
	
	public void setStudentHabitGradeComments(Set<StudentHabitGradeComment> studentHabitGradeComments) {
		this.studentHabitGradeComments = studentHabitGradeComments;
	}

	public Set<StudentSkillFocusLevel> getStudentSkillFocusLevels() {
		return this.studentSkillFocusLevels;
	}

	public void setStudentSkillFocusLevels(Set<StudentSkillFocusLevel> studentSkillFocusLevels) {
		this.studentSkillFocusLevels = studentSkillFocusLevels;
	}

	public Set<StudentSkillFocusArea> getStudentSkillFocusAreas() {
		return studentSkillFocusAreas;
	}

	public void setStudentSkillFocusAreas(Set<StudentSkillFocusArea> studentSkillFocusAreas) {
		this.studentSkillFocusAreas = studentSkillFocusAreas;
	}

	public Set<StudentNetwork> getStudentNetworks() {
		return this.studentNetworks;
	}

	public void setStudentNetworks(Set<StudentNetwork> studentNetworks) {
		this.studentNetworks = studentNetworks;
	}

	public Set<StudentJob> getStudentJobs() {
		return this.studentJobs;
	}

	public void setStudentJobs(Set<StudentJob> studentJobs) {
		this.studentJobs = studentJobs;
	}

	public Set<StudentDocument> getStudentDocuments() {
		return this.studentDocuments;
	}

	public void setStudentDocuments(Set<StudentDocument> studentDocuments) {
		this.studentDocuments = studentDocuments;
	}
	
	public Set<StudentCareerGoal> getStudentCareerGoals() {
		return studentCareerGoals;
	}
	
	public void setStudentCareerGoals(Set<StudentCareerGoal> studentCareerGoals) {
		this.studentCareerGoals = studentCareerGoals;
	}
	
	public Set<StudentIndustryGoal> getStudentIndustryGoals() {
		return studentIndustryGoals;
	}
	
	public void setStudentIndustryGoals(Set<StudentIndustryGoal> studentIndustryGoals) {
		this.studentIndustryGoals = studentIndustryGoals;
	}

	public Set<StudentJobReflection> getStudentJobReflections() {
		return this.studentJobReflections;
	}

	public void setStudentJobReflections(Set<StudentJobReflection> studentJobReflections) {
		this.studentJobReflections = studentJobReflections;
	}

	public Set<StudentSectionCompletion> getStudentSectionCompletions() {
		return this.studentSectionCompletions;
	}

	public void setStudentSectionCompletions(Set<StudentSectionCompletion> studentSectionCompletions) {
		this.studentSectionCompletions = studentSectionCompletions;
	}

	public int getMySkillsSummary() {
		return mySkillsSummary;
	}

	public void setMySkillsSummary(int mySkillsSummary) {
		this.mySkillsSummary = mySkillsSummary;
	}

	public StudentCompletionSummary getStudentCompletionSummary() {
		return studentCompletionSummary;
	}

	public void setStudentCompletionSummary(StudentCompletionSummary studentCompletionSummary) {
		this.studentCompletionSummary = studentCompletionSummary;
	}
	
	
	  public Set<StudentConversation> getStudentConversations() { return
	  studentConversations; }
	  
	  public void setStudentConversations(Set<StudentConversation>
	  studentConversations) { this.studentConversations = studentConversations; }
	 
	
	public Date getLastConversationDate() {
		return lastConversationDate;
	}
	
	public void setLastConversationDate(Date lastConversationDate) {
		this.lastConversationDate = lastConversationDate;
	}

	@Transient
	public String getMyProfileSummary()
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		// return Util.getProgressPercentage(this,Util.profileCalcMyExperience)+"%";
		return null;
	}

	@Transient
	public String getMyGoalsSummary() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		// return Util.getProgressPercentage(!this.studentGoals.isEmpty()?this.studentGoals.stream().findFirst().orElse(null):null,Util.profileCalcMyGoals)+"%";
		return null;
	}

	
	@Transient
	public String getMyExperienceSummary() throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		Set<String> differentJobs = new HashSet<String>();
		for (StudentJob job : this.studentJobs) {
			if (!job.getJobType().equals(Util.MYEXPERIENCES_TYPE)) {
				// Ignore MyExperiences type
				differentJobs.add(job.getJobType());
			}
		}
		return differentJobs.size() + "/2";
	}
	
	@Transient
	public int getMyExperiencesHours() {
		return this.studentJobs.stream().filter(job -> job.getJobType().equals(Util.MYEXPERIENCES_TYPE))
				.mapToInt(job -> job.getTotalHours() != null ? job.getTotalHours() : 0).sum();
	}	
	
	public void addFamilyMember(FamilyMember familyMember) {
		this.familyMembers.add(familyMember);
		familyMember.setStudentProfile(this);
	}
	
	public void addStudentNetwork(StudentNetwork studentNetwork) {
		this.studentNetworks.add(studentNetwork);
		studentNetwork.setStudentProfile(this);		
	}
				
	public void addCareerGoal(StudentCareerGoal careerGoal) {
		this.studentCareerGoals.add(careerGoal);
		careerGoal.setStudentProfile(this);
	}
	
	public void addIndustryGoal(StudentIndustryGoal industryGoal) {
		this.studentIndustryGoals.add(industryGoal);
		industryGoal.setStudentProfile(this);
	}
	
	/* StudentJob entity is used for both MyPlacements and MyExperiences */
	public void addStudentJob(StudentJob studentJob) {
		this.studentJobs.add(studentJob);
		studentJob.setStudentProfile(this);
	}
	
	public void addStudentJobReflection(StudentJobReflection studentJobReflection) {
		this.studentJobReflections.add(studentJobReflection);
		studentJobReflection.setStudentProfile(this);
	}
	
	public void addStudentHabitGrade(StudentHabitGrade studentHabitGrade) {
		this.studentHabitGrades.add(studentHabitGrade);
		studentHabitGrade.setStudentProfile(this);
	}
	
	public void addStudentHabitGradeComment(StudentHabitGradeComment studentHabitGradeComment) {
		this.studentHabitGradeComments.add(studentHabitGradeComment);
		studentHabitGradeComment.setStudentProfile(this);
	}
	
	public void addStudentSkillFocusArea(StudentSkillFocusArea studentSkillFocusArea) {
		this.studentSkillFocusAreas.add(studentSkillFocusArea);
		studentSkillFocusArea.setStudentProfile(this);
	}
	
	public void addStudentSkillFocusLevel(StudentSkillFocusLevel studentSkillFocusLevel) {
		this.studentSkillFocusLevels.add(studentSkillFocusLevel);
		studentSkillFocusLevel.setStudentProfile(this);
	}
	
	public void addStudentSectionCompletion(StudentSectionCompletion studentSectionCompletion) {
		this.studentSectionCompletions.add(studentSectionCompletion);
		studentSectionCompletion.setStudentProfile(this);
	}
	
	/*
	public short getVersion() {
		return version;
	}
	
	public void setVersion(short version) {
		this.version = version;
	}
	*/
	
	// =============== Rubrics 2020 ===========================================================
	
	public Set<StudentGrade> getStudentGrades() {
		return studentGrades;
	}
	
	public void setStudentGrades(Set<StudentGrade> studentGrades) {
		this.studentGrades = studentGrades;
	}
	
	public Set<StudentGradeComment> getStudentGradeComments() {
		return studentGradeComments;
	}
	
	public void setStudentGradeComments(Set<StudentGradeComment> studentGradeComments) {
		this.studentGradeComments = studentGradeComments;
	}	
	
	public void addStudentGradeComment(StudentGradeComment studentGradeComment) {
		this.studentGradeComments.add(studentGradeComment);
		studentGradeComment.setStudentProfile(this);
	}
	
	public void addStudentGrade(StudentGrade studentGrade) {
		this.studentGrades.add(studentGrade);
		studentGrade.setStudentProfile(this);
	}	

	
}
