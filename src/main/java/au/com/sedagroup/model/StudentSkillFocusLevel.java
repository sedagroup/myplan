package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentSkillFocusLevels", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentSkillFocusLevel extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 4282991602678005932L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SkillFocusLevelId", nullable = false)
	private SkillFocusLevel skillFocusLevel;
	
	// To get the skillFocusLevelId in the response result(since JsonIgnore is applied for join column)
	@Column(name = "SkillFocusLevelId", insertable = false, updatable = false)
	private Integer skillFocusLevelId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;
	
	@Column(name = "SemesterId", nullable = false)
	private int semesterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "StudentSkillDate", nullable = false, length = 23)
	private Date studentSkillDate;

	@Column(name = "IsTeacherGrade", nullable = true)
	private Boolean isTeacherGrade;

	@Column(name = "IsStudentGrade", nullable = true)
	private Boolean isStudentGrade;

	@Column(name = "TeacherId", nullable = true)
	private Integer teacherId;

	public StudentSkillFocusLevel() {
	}

	public StudentSkillFocusLevel(SkillFocusLevel skillFocusLevel, int semesterId, Date studentSkillDate, Boolean isTeacherGrade, Boolean isStudentGrade) {
		this.skillFocusLevel = skillFocusLevel;
		this.semesterId = semesterId;
		this.studentSkillDate = studentSkillDate;
		this.isTeacherGrade = isTeacherGrade;
		this.isStudentGrade = isStudentGrade;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SkillFocusLevel getSkillFocusLevel() {
		return this.skillFocusLevel;
	}

	public void setSkillFocusLevel(SkillFocusLevel skillFocusLevel) {
		this.skillFocusLevel = skillFocusLevel;
	}
	
	public Integer getSkillFocusLevelId() {
		return skillFocusLevelId;
	}
	
	public void setSkillFocusLevelId(Integer skillFocusLevelId) {
		this.skillFocusLevelId = skillFocusLevelId;
	}

	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}
	
	public int getSemesterId() {
		return semesterId;
	}
	
	public void setSemesterId(int semesterId) {
		this.semesterId = semesterId;
	}

	public Date getStudentSkillDate() {
		return studentSkillDate;
	}

	public void setStudentSkillDate(Date studentSkillDate) {
		this.studentSkillDate = studentSkillDate;
	}

	public Boolean getIsTeacherGrade() {
		return isTeacherGrade;
	}

	public void setIsTeacherGrade(Boolean isTeacherGrade) {
		this.isTeacherGrade = isTeacherGrade;
	}

	public Boolean getIsStudentGrade() {
		return isStudentGrade;
	}
	
	public void setIsStudentGrade(Boolean isStudentGrade) {
		this.isStudentGrade = isStudentGrade;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

}
