package au.com.sedagroup.model;

public class Teacher {
	private Integer id;
	private String firstName;
	private String surname;
	private String classCode;
	private String classCampus;
	
	public Teacher() {
	}
	
	@Override
	public String toString() {
		return getId()+ " - "+ getFirstName() + " " + getSurname() + " - " + getClassCode() +  " - " + getClassCampus();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getClassCode() {
		return classCode;
	}
	
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	public String getClassCampus() {
		return classCampus;
	}

	public void setClassCampus(String classCampus) {
		this.classCampus = classCampus;
	}

}
