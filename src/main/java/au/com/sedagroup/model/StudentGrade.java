package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentGrades", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentGrade extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -3013745138497811933L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "RubricId", nullable = false)
	private Integer rubricId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = true)
	@JsonIgnore
	private StudentProfile studentProfile;
	
	@Column(name = "TeacherId", nullable = true)
	private Integer teacherId;
	
	@Column(name = "SemesterId", nullable = false)
	private int semesterId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "StudentMarkDate", nullable = true, length = 23)
	private Date studentMarkDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TeacherMarkDate", nullable = true, length = 23)
	private Date teacherMarkDate;

	@Column(name = "IsTeacherGrade", nullable = true)
	private Boolean isTeacherGrade;

	@Column(name = "IsStudentGrade", nullable = true)
	private Boolean isStudentGrade;

	public StudentGrade() {
	}

	public StudentGrade(Integer rubricId, int semesterId, Date studentMarkDate, Date teacherMarkDate, Boolean isTeacherGrade, Boolean isStudentGrade) {
		this.rubricId = rubricId;
		this.semesterId = semesterId;
		this.studentMarkDate = studentMarkDate;
		this.teacherMarkDate = teacherMarkDate;
		this.isTeacherGrade = isTeacherGrade;
		this.isStudentGrade = isStudentGrade;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getRubricId() {
		return rubricId;
	}
	
	public void setRubricId(Integer rubricId) {
		this.rubricId = rubricId;
	}
	
	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}
	
	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	
	public int getSemesterId() {
		return semesterId;
	}
	
	public void setSemesterId(int semesterId) {
		this.semesterId = semesterId;
	}

	public Date getStudentMarkDate() {
		return studentMarkDate;
	}
	
	public void setStudentMarkDate(Date studentMarkDate) {
		this.studentMarkDate = studentMarkDate;
	}
	
	public Date getTeacherMarkDate() {
		return teacherMarkDate;
	}
	
	public void setTeacherMarkDate(Date teacherMarkDate) {
		this.teacherMarkDate = teacherMarkDate;
	}
	
	public Boolean getIsTeacherGrade() {
		return isTeacherGrade;
	}

	public void setIsTeacherGrade(Boolean isTeacherGrade) {
		this.isTeacherGrade = isTeacherGrade;
	}

	public Boolean getIsStudentGrade() {
		return isStudentGrade;
	}
	
	public void setIsStudentGrade(Boolean isStudentGrade) {
		this.isStudentGrade = isStudentGrade;
	}

}
