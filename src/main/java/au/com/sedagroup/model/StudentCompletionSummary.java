package au.com.sedagroup.model;

import java.util.ArrayList;
import java.util.List;

public class StudentCompletionSummary {
	String myProfileStudentCompleted;
	String myProfileTeacherApproved;
	String myGoalStudentCompleted;
	String myGoalTeacherApproved;
	String myExperienceStudentCompleted;
	String myExperienceTeacherApproved;
	String myPlacementStudentCompleted;
	String myPlacementTeacherApproved;
	List<StudentSkillSummary> mySkillSummaries =  new ArrayList<StudentSkillSummary>();
	
	public StudentCompletionSummary(){
		this.myProfileStudentCompleted = "";
		this.myProfileTeacherApproved = "";
		this.myGoalStudentCompleted = "";
		this.myGoalTeacherApproved = "";
		this.myExperienceStudentCompleted = "";
		this.myExperienceTeacherApproved = "";
		this.myPlacementStudentCompleted = "";
		this.myPlacementTeacherApproved = "";
	}
	public String getMyProfileStudentCompleted() {
		return myProfileStudentCompleted;
	}


	public void setMyProfileStudentCompleted(String myProfileStudentCompleted) {
		this.myProfileStudentCompleted = myProfileStudentCompleted;
	}


	public String getMyProfileTeacherApproved() {
		return myProfileTeacherApproved;
	}


	public void setMyProfileTeacherApproved(String myProfileTeacherApproved) {
		this.myProfileTeacherApproved = myProfileTeacherApproved;
	}


	public String getMyGoalStudentCompleted() {
		return myGoalStudentCompleted;
	}


	public void setMyGoalStudentCompleted(String myGoalStudentCompleted) {
		this.myGoalStudentCompleted = myGoalStudentCompleted;
	}


	public String getMyGoalTeacherApproved() {
		return myGoalTeacherApproved;
	}


	public void setMyGoalTeacherApproved(String myGoalTeacherApproved) {
		this.myGoalTeacherApproved = myGoalTeacherApproved;
	}


	public String getMyExperienceStudentCompleted() {
		return myExperienceStudentCompleted;
	}


	public void setMyExperienceStudentCompleted(String myExperienceStudentCompleted) {
		this.myExperienceStudentCompleted = myExperienceStudentCompleted;
	}


	public String getMyExperienceTeacherApproved() {
		return myExperienceTeacherApproved;
	}


	public void setMyExperienceTeacherApproved(String myExperienceTeacherApproved) {
		this.myExperienceTeacherApproved = myExperienceTeacherApproved;
	}	
	
	public String getMyPlacementStudentCompleted() {
		return myPlacementStudentCompleted;
	}
	public void setMyPlacementStudentCompleted(String myPlacementStudentCompleted) {
		this.myPlacementStudentCompleted = myPlacementStudentCompleted;
	}
	public String getMyPlacementTeacherApproved() {
		return myPlacementTeacherApproved;
	}
	public void setMyPlacementTeacherApproved(String myPlacementTeacherApproved) {
		this.myPlacementTeacherApproved = myPlacementTeacherApproved;
	}
	
	public List<StudentSkillSummary> getMySkillSummaries() {
		return mySkillSummaries;
	}
	public void setMySkillSummaries(List<StudentSkillSummary> mySkillSummaries) {
		this.mySkillSummaries = mySkillSummaries;
	}
	
	public void addSkillSummary(StudentSkillSummary skillSummary) {
		this.mySkillSummaries.add(skillSummary);
	}
}
