package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentJobReflections", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class StudentJobReflection extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 7636558279362751292L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;

	@Column(name = "ReflectionDate", nullable = false)
	private Date reflectionDate;

	@Column(name = "Comments", nullable = false)
	private String comments;
	
	@Column(name = "TransferrableSkill", nullable = true)
	private String transferrableSkill;	

	@Column(name = "Approved")
	private Boolean approved;
	
	@Column(name = "isCommunication")
	private Boolean isCommunication;
	
	@Column(name = "isTeamwork")
    private Boolean isTeamwork;
    
	@Column(name = "isProblemSolving")
    private Boolean isProblemSolving;
    
	@Column(name = "isSelfAwareness")
    private Boolean isSelfAwareness;
    
	@Column(name = "isPersonalOrganisation")
    private Boolean isPersonalOrganisation;    

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	public StudentJobReflection() {
	}

	public StudentJobReflection(Integer id, Date reflectionDate, String comments, String transferrableSkill, Boolean approved) {
		super();
		this.id = id;
		this.reflectionDate = reflectionDate;
		this.comments = comments;
		this.transferrableSkill = transferrableSkill;
		this.approved = approved;		
	}

	public StudentJobReflection(Integer id, StudentProfile studentProfile, Date reflectionDate, String comments,  String transferrableSkill, Boolean approved) {
		super();
		this.id = id;
		this.studentProfile = studentProfile;
		this.reflectionDate = reflectionDate;
		this.comments = comments;
		this.transferrableSkill = transferrableSkill;
		this.approved = approved;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

	public Date getReflectionDate() {
		return this.reflectionDate;
	}

	public void setReflectionDate(Date reflectionDate) {
		this.reflectionDate = reflectionDate;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	public String getTransferrableSkill() {
		return transferrableSkill;
	}
	
	public void setTransferrableSkill(String transferrableSkill) {
		this.transferrableSkill = transferrableSkill;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}
	
	public Boolean getIsCommunication() {
		return isCommunication;
	}

	public void setIsCommunication(Boolean isCommunication) {
		this.isCommunication = isCommunication;
	}

	public Boolean getIsTeamwork() {
		return isTeamwork;
	}

	public void setIsTeamwork(Boolean isTeamwork) {
		this.isTeamwork = isTeamwork;
	}

	public Boolean getIsProblemSolving() {
		return isProblemSolving;
	}

	public void setIsProblemSolving(Boolean isProblemSolving) {
		this.isProblemSolving = isProblemSolving;
	}

	public Boolean getIsSelfAwareness() {
		return isSelfAwareness;
	}

	public void setIsSelfAwareness(Boolean isSelfAwareness) {
		this.isSelfAwareness = isSelfAwareness;
	}

	public Boolean getIsPersonalOrganisation() {
		return isPersonalOrganisation;
	}

	public void setIsPersonalOrganisation(Boolean isPersonalOrganisation) {
		this.isPersonalOrganisation = isPersonalOrganisation;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
