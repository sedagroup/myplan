package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentConversations", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentConversation extends Auditable<String> implements java.io.Serializable {
	
	private static final long serialVersionUID = -5294999672889133701L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	//@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name = "StudentId", nullable = false)
	/*
	 * @JsonIgnore
	 * 
	 * @Transient private StudentProfile studentProfile;
	 */
	@Column(name = "TeacherId", nullable = false)
	private Integer teacherId;

	@Column(name = "Topic", length = 1000, nullable = false)
	private String topic;
	
	@Column(name = "StudentComment", length = 3000, nullable = true)
	private String studentComment;
	
	@Column(name = "TeacherComment", length = 3000, nullable = true)
	private String teacherComment;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ConversationStartDate", nullable = false, length = 23)
	private Date conversationStartDate;
	
	@Column(name = "IsFinal", nullable = true)
	private Boolean isFinal;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;
	
	@Column(name = "StudentId", nullable = false)
	private Integer studentId;
	
	public StudentConversation() {
	}
	
	private void StudentConversation(Integer teacherId, String topic, String studentComment, String teacherComment, Date conversationStartDate) {
		this.teacherId = teacherId;
		this.topic = topic;
		this.teacherComment = teacherComment;
		this.studentComment = studentComment;
		this.conversationStartDate = conversationStartDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/*
	 * public StudentProfile getStudentProfile() { return studentProfile; }
	 * 
	 * public void setStudentProfile(StudentProfile studentProfile) {
	 * this.studentProfile = studentProfile; }
	 */

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getStudentComment() {
		return studentComment;
	}
	
	public void setStudentComment(String studentComment) {
		this.studentComment = studentComment;
	}
	
	public String getTeacherComment() {
		return teacherComment;
	}
	
	public void setTeacherComment(String teacherComment) {
		this.teacherComment = teacherComment;
	}
	
	public Date getConversationStartDate() {
		return conversationStartDate;
	}
	
	public void setConversationStartDate(Date conversationStartDate) {
		this.conversationStartDate = conversationStartDate;
	}
	
	public Boolean getIsFinal() {
		return isFinal;
	}

	public void setIsFinal(Boolean isFinal) {
		this.isFinal = isFinal;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	
	public Integer getStudentId() {
		return studentId;
	}
	
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentConversation other = (StudentConversation) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
		
	
	
}
