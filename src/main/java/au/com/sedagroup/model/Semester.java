package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "Semesters", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Semester extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 1322328961733619732L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "SemesterId", unique = true, nullable = false)
	private Integer semesterId;

	@Column(name = "SemesterValue", nullable = false)
	private Integer semesterValue;

	@Column(name = "SemesterYear", nullable = false)
	private Integer semesterYear;
	
	@Column(name = "StartDate", nullable = false)
	private Date startDate;

	@Column(name = "EndDate", nullable = false)
	private Date endDate;
	
	@Column(name = "AppVersion", nullable = false)
	private String appVersion;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	public Semester() {
	}

	public Integer getSemesterId() {
		return this.semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public Integer getSemesterValue() {
		return this.semesterValue;
	}

	public void setSemesterValue(Integer semesterValue) {
		this.semesterValue = semesterValue;
	}

	public Integer getSemesterYear() {
		return this.semesterYear;
	}

	public void setSemesterYear(Integer semesterYear) {
		this.semesterYear = semesterYear;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String getAppVersion() {
		return appVersion;
	}
	
	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
