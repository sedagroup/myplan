package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentGradeComments", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentGradeComment extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -3003825289290828017L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;
	
	@Column(name = "TeacherId", nullable = true)
	private Integer teacherId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AreaId", nullable = false)
	private Area area;
	
	// To get the rubricId in the response result(since JsonIgnore is applied for join column)
	@Column(name = "AreaId", insertable = false, updatable = false)
	private Integer areaId;	
	
	@Column(name = "SemesterId", nullable = false)
	private Integer semesterId;

	@Column(name = "TeacherComment", length = 8000)
	private String teacherComment;

	@Column(name = "StudentComment", length = 8000)
	private String studentComment;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TeacherCommentDate", length = 23)
	private Date teacherCommentDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "StudentCommentDate", length = 23)
	private Date studentCommentDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	public StudentGradeComment() {
	}

	public StudentGradeComment(StudentProfile studentProfile, Integer teacherId, Area area, Integer semesterId, String teacherComment, String studentComment, Date teacherCommentDate, Date studentCommentDate) {
		this.studentProfile = studentProfile;
		this.teacherId = teacherId;
		this.area = area;
		this.semesterId = semesterId;
		this.teacherComment = teacherComment;
		this.studentComment = studentComment;
		this.semesterId = semesterId;
		this.teacherCommentDate = teacherCommentDate;
		this.studentCommentDate = studentCommentDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentProfile getStudentProfile() {
		return studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

	public Integer getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}

	public Area getArea() {
		return area;
	}
	
	public void setArea(Area area) {
		this.area = area;
	}
	
	public Integer getAreaId() {
		return areaId;
	}
	
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	
	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public String getTeacherComment() {
		return teacherComment;
	}

	public void setTeacherComment(String teacherComment) {
		this.teacherComment = teacherComment;
	}

	public String getStudentComment() {
		return studentComment;
	}

	public void setStudentComment(String studentComment) {
		this.studentComment = studentComment;
	}

	public Date getTeacherCommentDate() {
		return teacherCommentDate;
	}

	public void setTeacherCommentDate(Date teacherCommentDate) {
		this.teacherCommentDate = teacherCommentDate;
	}

	public Date getStudentCommentDate() {
		return studentCommentDate;
	}

	public void setStudentCommentDate(Date studentCommentDate) {
		this.studentCommentDate = studentCommentDate;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}
	

}
