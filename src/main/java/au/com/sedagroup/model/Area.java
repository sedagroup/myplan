package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Areas", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Area extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 5899752490228704183L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "AreaName", nullable = false, length = 1000)
	private String areaName;
		
	//@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "AreaTypeId", nullable = false)
	private AreaType areaType;
		
	@Column(name = "SortOrder", nullable = false)
	private int sortOrder;
	
	@Column(name = "ColourCode", length = 50)
	private String colourCode;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "area")
	@OrderBy(clause = "sortOrder ASC")
	private Set<FocusArea> focusAreas = new HashSet<FocusArea>(0);
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "area")
	@OrderBy(clause = "displayOrderValue ASC")
	private Set<RubricHeading> rubricHeadings = new HashSet<RubricHeading>(0);
	
	public Area() {
	}

	public Area(String areaName, AreaType areaType, Set<FocusArea> focusAreas) {
		this.areaName = areaName;
		this.areaType = areaType;
		this.focusAreas = focusAreas;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAreaName() {
		return areaName;
	}
	
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	public AreaType getAreaType() {
		return areaType;
	}
	
	public void setAreaType(AreaType areaType) {
		this.areaType = areaType;
	}
		
	public int getSortOrder() {
		return sortOrder;
	}
	
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
	
	public String getColourCode() {
		return colourCode;
	}
	
	public void setColourCode(String colourCode) {
		this.colourCode = colourCode;
	}
	
	public Set<RubricHeading> getRubricHeadings() {
		return rubricHeadings;
	}
	
	public void setRubricHeadings(Set<RubricHeading> rubricHeadings) {
		this.rubricHeadings = rubricHeadings;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Set<FocusArea> getFocusAreas() {
		return focusAreas;
	}
	
	public void setFocusAreas(Set<FocusArea> focusAreas) {
		this.focusAreas = focusAreas;
	}

}
