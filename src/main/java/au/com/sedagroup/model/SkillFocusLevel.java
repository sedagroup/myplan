package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "SkillFocusLevels", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SkillFocusLevel extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 4413994296947941026L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SkillFocusAreaId", nullable = false)
	private SkillFocusArea skillFocusArea;

	@Column(name = "SkillLevel", nullable = false, length = 8000)
	private String skillLevel;

	@Column(name = "SkillLevelValue")
	private Integer skillLevelValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	public SkillFocusLevel() {
	}

	public SkillFocusLevel(SkillFocusArea skillFocusArea, String skillLevel) {
		this.skillFocusArea = skillFocusArea;
		this.skillLevel = skillLevel;
	}

	public SkillFocusLevel(SkillFocusArea skillFocusArea, String skillLevel, Integer skillLevelValue) {
		this.skillFocusArea = skillFocusArea;
		this.skillLevel = skillLevel;
		this.skillLevelValue = skillLevelValue;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public SkillFocusArea getSkillFocusArea() {
		return this.skillFocusArea;
	}

	public void setSkillFocusArea(SkillFocusArea skillFocusArea) {
		this.skillFocusArea = skillFocusArea;
	}

	public String getSkillLevel() {
		return this.skillLevel;
	}

	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}

	public Integer getSkillLevelValue() {
		return this.skillLevelValue;
	}

	public void setSkillLevelValue(Integer skillLevelValue) {
		this.skillLevelValue = skillLevelValue;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
