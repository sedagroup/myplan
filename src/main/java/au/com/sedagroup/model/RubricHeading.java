package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "RubricHeadings", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RubricHeading extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -1627231797050004931L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "HeadingName", nullable = false, length = 1000)
	private String headingName;
	
	@Column(name = "HeadingDescription", nullable = true, length = 1000)
	private String headingDescription;
	
	@Column(name = "DisplayOrderValue", nullable = false, length = 20)
	private Integer displayOrderValue;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AreaId", nullable = false)
	private Area area;
	
	@Column(name = "AreaId", insertable = false, updatable = false)
	private Integer areaId;	
		
	@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "AreaTypeId", nullable = false)
	private AreaType areaType;
	
	@Column(name = "AreaTypeId", insertable = false, updatable = false)
	private Integer areaTypeId;	
	
	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	public RubricHeading() {
	}

	public RubricHeading(String headingName, Integer displayOrderValue) {
		this.headingName = headingName;
		this.displayOrderValue = displayOrderValue;
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getHeadingName() {
		return headingName;
	}
	
	public void setHeadingName(String headingName) {
		this.headingName = headingName;
	}
	
	public String getHeadingDescription() {
		return headingDescription;
	}
	
	public void setHeadingDescription(String headingDescription) {
		this.headingDescription = headingDescription;
	}
	
	public Integer getDisplayOrderValue() {
		return displayOrderValue;
	}
	
	public void setDisplayOrderValue(Integer displayOrderValue) {
		this.displayOrderValue = displayOrderValue;
	}
	
	public Area getArea() {
		return area;
	}
	
	public void setArea(Area area) {
		this.area = area;
	}
	
	public Integer getAreaId() {
		return areaId;
	}
	
	public void setAreaId(Integer areaId) {
		this.areaId = areaId;
	}
	
	public Integer getAreaTypeId() {
		return areaTypeId;
	}
	
	public void setAreaTypeId(Integer areaTypeId) {
		this.areaTypeId = areaTypeId;
	}
	
	public AreaType getAreaType() {
		return areaType;
	}
	
	public void setAreaType(AreaType areaType) {
		this.areaType = areaType;
	}
	
	public Date getDeletedAt() {
		return deletedAt;
	}
	
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
