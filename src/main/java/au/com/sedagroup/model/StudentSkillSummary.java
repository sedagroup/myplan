package au.com.sedagroup.model;

public class StudentSkillSummary {
	String year;
	Integer semester;
	Integer teacherApproved;
	
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public Integer getSemester() {
		return semester;
	}
	public void setSemester(Integer semester) {
		this.semester = semester;
	}
	public Integer getTeacherApproved() {
		return teacherApproved;
	}
	public void setTeacherApproved(Integer teacherApproved) {
		this.teacherApproved = teacherApproved;
	}
}
