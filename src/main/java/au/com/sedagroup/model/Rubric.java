package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Rubrics", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Rubric extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -6648560865174932175L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "RubricDescription", nullable = false, length = 1000)
	private String rubricDescription;
		
	//@JsonIgnore
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RubricHeadingId", nullable = false)
	private RubricHeading rubricHeading;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FocusAreaId", nullable = false)
	private FocusArea focusArea;
		
	// To get the focusAreaId in the response result(since JsonIgnore is applied for join column)
	@Column(name = "FocusAreaId", insertable = false, updatable = false)
	private Integer focusAreaId;
		
	public Rubric() {
	}
	
	public Rubric(String rubricDescription, RubricHeading rubricHeading) {
		this.rubricDescription = rubricDescription;
		this.rubricHeading = rubricHeading;
	}
	
	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	public String getRubricDescription() {
		return rubricDescription;
	}
	
	public void setRubricDescription(String rubricDescription) {
		this.rubricDescription = rubricDescription;
	}
	
	public RubricHeading getRubricHeading() {
		return rubricHeading;
	}
	
	public void setRubricHeading(RubricHeading rubricHeading) {
		this.rubricHeading = rubricHeading;
	}
	
	public Date getDeletedAt() {
		return deletedAt;
	}
	
	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public FocusArea getFocusArea() {
		return focusArea;
	}
	
	public void setFocusArea(FocusArea focusArea) {
		this.focusArea = focusArea;
	}
	
	public Integer getFocusAreaId() {
		return focusAreaId;
	}
	
	public void setFocusAreaId(Integer focusAreaId) {
		this.focusAreaId = focusAreaId;
	}
	
}
