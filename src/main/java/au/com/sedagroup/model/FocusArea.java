package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OrderBy;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "FocusAreas", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class FocusArea extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -2322037636939547205L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "FocusAreaName", nullable = false, length = 1000)
	private String focusAreaName;
	
	@Column(name = "FocusAreaShortName", nullable = true, length = 200)
	private String focusAreaShortName;
	
	@Column(name = "SortOrder", nullable = false)
	private int sortOrder;	

	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "AreaId", nullable = false)
	private Area area;

	@JsonIgnore
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "focusArea")
	@OrderBy(clause = "rubricHeading ASC")
	private Set<Rubric> rubrics = new HashSet<Rubric>(0);

	public FocusArea() {
	}

	public FocusArea(Area area, String focusAreaName, String focusAreaShortName, int sortOrder) {
		this.area = area;
		this.focusAreaName = focusAreaName;
		this.focusAreaShortName = focusAreaName;
		this.sortOrder = sortOrder;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFocusAreaName() {
		return focusAreaName;
	}
	
	public void setFocusAreaName(String focusAreaName) {
		this.focusAreaName = focusAreaName;
	}
	
	public String getFocusAreaShortName() {
		return focusAreaShortName;
	}
	
	public void setFocusAreaShortName(String focusAreaShortName) {
		this.focusAreaShortName = focusAreaShortName;
	}
	
	public int getSortOrder() {
		return sortOrder;
	}
	
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Set<Rubric> getRubrics() {
		return rubrics;
	}
	
	public void setRubrics(Set<Rubric> rubrics) {
		this.rubrics = rubrics;
	}

}
