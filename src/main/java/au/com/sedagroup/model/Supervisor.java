package au.com.sedagroup.model;

public class Supervisor {
	private Integer id;
	private String firstName;
	private String surname;
	private Integer roleId;
	
	public Supervisor() {
	}
	
	@Override
	public String toString() {
		return getId()+ " - "+ getFirstName() + " " + getSurname() + " - " + getRoleId() ;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public Integer getRoleId() {
		return roleId;
	}
	
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}
