package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentJobs", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class StudentJob extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -2790135228586998003L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId", nullable = false)
	@JsonIgnore
	private StudentProfile studentProfile;

	@Column(name = "EmployerName", length = 250)
	private String employerName;

	@Column(name = "JobType", length = 100)
	private String jobType;

	@Column(name = "JobTitle", length = 250)
	private String jobTitle;
	
	@Column(name = "PlacementGoal", length = 1000, nullable = true)
	private String placementGoal;	

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "StartDate", length = 23)
	private Date startDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EndDate", length = 23)
	private Date endDate;

	@Column(name = "JobDuties", length = 1000)
	private String jobDuties;

	@Column(name = "TotalHours", precision = 18, scale = 0)
	private Integer totalHours;

	@Column(name = "OrganizationTypeId", nullable = true)
	private Integer organizationTypeId;
	
	@Column(name = "TeacherComments", length = 1000)
	private String teacherComments;

	@Column(name = "StartTime", length = 10)
	private String startTime;

	@Column(name = "EndTime", length = 10)
	private String endTime;

	@Column(name = "Approved")
	private Boolean approved;
			
	public StudentJob() {
	}

	public StudentJob(StudentProfile studentProfile, String employerName, String jobType, String jobTitle,
			Date startDate, Integer organizationTypeId) {
		this.studentProfile = studentProfile;
		this.employerName = employerName;
		this.jobType = jobType;
		this.jobTitle = jobTitle;
		this.startDate = startDate;
		this.organizationTypeId = organizationTypeId;
	}

	public StudentJob(StudentProfile studentProfile, String employerName, String jobType, String jobTitle,
			Date startDate, Date endDate, String jobDuties, Integer totalHours, Integer organizationTypeId,
			String teacherComments, String startTime, String endTime, Boolean approved) {
		this.studentProfile = studentProfile;
		this.employerName = employerName;
		this.jobType = jobType;
		this.jobTitle = jobTitle;
		this.startDate = startDate;
		this.endDate = endDate;
		this.jobDuties = jobDuties;
		this.totalHours = totalHours;
		this.organizationTypeId = organizationTypeId;
		this.teacherComments = teacherComments;
		this.startTime = startTime;
		this.endTime = endTime;
		this.approved = approved;
	}

	public StudentJob(Integer id, String employerName, String jobType, String jobTitle, Date startDate, Date endDate,
			String jobDuties, Integer totalHours, Integer organizationTypeId, String teacherComments, String startTime,
			String endTime, Boolean approved) {
		this.id = id;
		this.employerName = employerName;
		this.jobType = jobType;
		this.jobTitle = jobTitle;
		this.startDate = startDate;
		this.endDate = endDate;
		this.jobDuties = jobDuties;
		this.totalHours = totalHours;
		this.organizationTypeId = organizationTypeId;
		this.teacherComments = teacherComments;
		this.startTime = startTime;
		this.endTime = endTime;
		this.approved = approved;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

	public String getEmployerName() {
		return this.employerName;
	}

	public void setEmployerName(String employerName) {
		this.employerName = employerName;
	}

	public String getJobType() {
		return this.jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getPlacementGoal() {
		return placementGoal;
	}
	
	public void setPlacementGoal(String placementGoal) {
		this.placementGoal = placementGoal;
	}
	
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getJobDuties() {
		return this.jobDuties;
	}

	public void setJobDuties(String jobDuties) {
		this.jobDuties = jobDuties;
	}

	public Integer getTotalHours() {
		return this.totalHours;
	}

	public void setTotalHours(Integer totalHours) {
		this.totalHours = totalHours;
	}

	public Integer getOrganizationTypeId() {
		return organizationTypeId;
	}
	
	public void setOrganizationTypeId(Integer organizationTypeId) {
		this.organizationTypeId = organizationTypeId;
	}

	public String getTeacherComments() {
		return teacherComments;
	}

	public void setTeacherComments(String teacherComments) {
		this.teacherComments = teacherComments;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public Boolean getApproved() {
		return approved;
	}

	public void setApproved(Boolean approved) {
		this.approved = approved;
	}
	
}