package au.com.sedagroup.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "StudentDocumentData", schema = "dbo", catalog = "SEDAMyPlan")
@DynamicUpdate
@DynamicInsert
public class StudentDocumentData implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -934187583859081970L;

	@Id
	@GenericGenerator(name = "generator", strategy = "guid")
	@GeneratedValue(generator = "generator")
	@Column(name = "StudentDocumentDataId", unique = true, nullable = false, length = 36)
	private String studentDocumentDataId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentDocumentId")
	@JsonIgnore
	private StudentDocument studentDocument;

	@Column(name = "DocumentData")
	private byte[] documentData;
	
	public String getStudentDocumentDataId() {
		return studentDocumentDataId;
	}

	public void setStudentDocumentDataId(String studentDocumentDataId) {
		this.studentDocumentDataId = studentDocumentDataId;
	}
	
	public StudentDocument getStudentDocument() {
		return studentDocument;
	}

	public void setStudentDocument(StudentDocument studentDocument) {
		this.studentDocument = studentDocument;
	}
	
	public byte[] getDocumentData() {
		return documentData;
	}

	public void setDocumentData(byte[] documentData) {
		this.documentData = documentData;
	}
}
