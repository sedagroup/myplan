package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OrderBy;

@Entity
@Table(name = "SkillAreas", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class SkillArea extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = 5102989715665148505L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "SkillArea", nullable = false, length = 1000)
	private String skillArea;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "skillArea")
	@OrderBy(clause = "id ASC")
	private Set<SkillFocusArea> skillFocusAreas = new HashSet<SkillFocusArea>(0);

	public SkillArea() {
	}

	public SkillArea(String skillArea) {
		this.skillArea = skillArea;
	}

	public SkillArea(String skillArea, Set<SkillFocusArea> skillFocusAreas) {
		this.skillArea = skillArea;
		this.skillFocusAreas = skillFocusAreas;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSkillArea() {
		return this.skillArea;
	}

	public void setSkillArea(String skillArea) {
		this.skillArea = skillArea;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Set<SkillFocusArea> getSkillFocusAreas() {
		return this.skillFocusAreas;
	}

	public void setSkillFocusAreas(Set<SkillFocusArea> skillFocusAreas) {
		this.skillFocusAreas = skillFocusAreas;
	}

}
