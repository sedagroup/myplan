package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "Documents", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Document extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -4065310008415258506L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "DocumentType", nullable = false, length = 10)
	private String documentType;

	@Column(name = "DocumentName", nullable = false, length = 100)
	private String documentName;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DeletedAt", length = 23)
	private Date deletedAt;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "document")
	private Set<StudentDocument> studentDocuments = new HashSet<StudentDocument>(0);

	public Document() {
	}

	public Document(String documentType, String documentName) {
		this.documentType = documentType;
		this.documentName = documentName;
	}

	public Document(String documentType, String documentName, Set<StudentDocument> studentDocuments) {
		this.documentType = documentType;
		this.documentName = documentName;
		this.studentDocuments = studentDocuments;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDocumentType() {
		return this.documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentName() {
		return this.documentName;
	}

	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}

	public Date getDeletedAt() {
		return this.deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

	public Set<StudentDocument> getStudentDocuments() {
		return this.studentDocuments;
	}

	public void setStudentDocuments(Set<StudentDocument> studentDocuments) {
		this.studentDocuments = studentDocuments;
	}

}
