package au.com.sedagroup.model;

import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "FamilyMembers", schema = "dbo", catalog = "SEDAMyPlan")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@DynamicUpdate
@DynamicInsert
public class FamilyMember extends Auditable<String> implements java.io.Serializable {

	private static final long serialVersionUID = -4140401250353163823L;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "Id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "MemberName", length = 1000)
	private String memberName;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StudentId")
	@JsonIgnore
	private StudentProfile studentProfile;
		
	public FamilyMember() {
	}

	public FamilyMember(StudentProfile studentProfile, String memberName) {
		this.studentProfile = studentProfile;
		this.memberName = memberName;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public StudentProfile getStudentProfile() {
		return this.studentProfile;
	}

	public void setStudentProfile(StudentProfile studentProfile) {
		this.studentProfile = studentProfile;
	}

	public String getMemberName() {
		return this.memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	

	
}
