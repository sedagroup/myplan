package au.com.sedagroup.config;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import au.com.sedagroup.interceptor.MySedaLoginInterceptor;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class JpaAuditingConfiguration {

	@Autowired
	HttpServletRequest servletRequest;
	
	/*  Gets the logged in users synergeticId from the session, saves audit columns for entity classes
	 ** If spring security is used get the username - SecurityContextHolder.getContext().getAuthentication().getName()
	*/
    @SuppressWarnings("unchecked")
	@Bean
    public AuditorAware<String> auditorProvider() {         	
    	return () -> (String) servletRequest.getSession().getAttribute(MySedaLoginInterceptor.MY_SEDA_SESSION_ATTRIBUTE);        
    }
        
}
