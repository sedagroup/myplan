package au.com.sedagroup.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import au.com.sedagroup.model.OrganizationType;
import au.com.sedagroup.repository.OrganizationTypeRepository;

@Service
@Transactional
public class LookupService {
					
	@Autowired
	private OrganizationTypeRepository organizationTypeRepository;
				   
	public List<OrganizationType> getOrganizationTypes(){
		return (List<OrganizationType>) organizationTypeRepository.findAllByOrderByOrganizationTypeAsc();
	}
	
}
