package au.com.sedagroup.service;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.sedagroup.dao.SynergeticDAO;
import au.com.sedagroup.dto.GradeReviewDTO;
import au.com.sedagroup.dto.Student;
import au.com.sedagroup.model.Area;
import au.com.sedagroup.model.FocusArea;
import au.com.sedagroup.model.StudentGradeComment;
import au.com.sedagroup.model.StudentGrade;
import au.com.sedagroup.model.Rubric;
import au.com.sedagroup.model.StudentCareerGoal;
import au.com.sedagroup.model.StudentCompletionSummary;
import au.com.sedagroup.model.StudentConversation;
import au.com.sedagroup.model.StudentIndustryGoal;
import au.com.sedagroup.model.StudentJob;
import au.com.sedagroup.model.StudentJobReflection;
import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.model.StudentSectionCompletion;
import au.com.sedagroup.model.StudentSkillSummary;
import au.com.sedagroup.repository.AreaRepository;
import au.com.sedagroup.repository.StudentGradeCommentRepository;
import au.com.sedagroup.repository.StudentGradeRepository;
import au.com.sedagroup.repository.FocusAreaRepository;
import au.com.sedagroup.repository.RubricRepository;
import au.com.sedagroup.repository.SemesterRepository;
import au.com.sedagroup.repository.StudentCareerGoalRepository;
import au.com.sedagroup.repository.StudentConversationRepository;
import au.com.sedagroup.repository.StudentSectionCompletionRepository;
import au.com.sedagroup.repository.StudentIndustryGoalRepository;
import au.com.sedagroup.repository.StudentJobReflectionRepository;
import au.com.sedagroup.repository.StudentJobRepository;
import au.com.sedagroup.repository.StudentProfileRepository;
import au.com.sedagroup.util.Util;

@Service
@Transactional
public class ReviewService {

	@Autowired
	private StudentProfileRepository profileRepo;
	@Autowired
	private SemesterRepository semesterRepo;
	@Autowired
	private StudentConversationRepository studentConversationRepo;	
	@Autowired
	private StudentCareerGoalRepository studentCareerGoalRepo;
	@Autowired
	private StudentIndustryGoalRepository studentIndustryGoalRepo;
	@Autowired
	private StudentJobRepository studentJobRepo;
	@Autowired
	private StudentJobReflectionRepository studentJobReflectionRepo;
	@Autowired
	private StudentSectionCompletionRepository studentSectionCompletionRepo;
	@Autowired
	FocusAreaRepository focusAreaRepo;
	@Autowired
	private AreaRepository areaRepo;
	@Autowired
	private StudentGradeRepository studentGradeRepo;
	@Autowired
	private StudentGradeCommentRepository studentGradeCommentRepo;
	@Autowired
	private RubricRepository rubricRepo;
	@Autowired
	private SynergeticDAO synergeticDao;
	
	@Resource(name = "configuration")
	private Properties properties;
	
	private Logger logger = LoggerFactory.getLogger(ReviewService.class);

	public StudentProfile getProfileByStudentId(int studentId) {
		
		StudentProfile profile = profileRepo.findOne(studentId);
		
		if (profile != null) {
			// Initialize required collections as required, better than loading relationships eagerly.			
			Hibernate.initialize(profile.getFamilyMembers());
			Hibernate.initialize(profile.getStudentNetworks());
			Hibernate.initialize(profile.getStudentDocuments());
			Hibernate.initialize(profile.getStudentCareerGoals());
			Hibernate.initialize(profile.getStudentIndustryGoals());
			Hibernate.initialize(profile.getStudentJobs());
			Hibernate.initialize(profile.getStudentJobReflections());
			Hibernate.initialize(profile.getStudentHabitGrades());
			Hibernate.initialize(profile.getStudentHabitGradeComments());
			Hibernate.initialize(profile.getStudentSkillFocusAreas());
			Hibernate.initialize(profile.getStudentSkillFocusLevels());		
			Hibernate.initialize(profile.getStudentSectionCompletions());
			profile.setClassTeacherId(synergeticDao.getClassTeacherByStudentId(studentId));
			/* Rubrics 2020 - V2 */
			Hibernate.initialize(profile.getStudentGrades());
			Hibernate.initialize(profile.getStudentGradeComments());
		}
		return profile;
	}

	public List<Student> getStudentsProfile(List<Student> students)	throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		
		int totalNumberOfSkillsHabits =  focusAreaRepo.findAllActive().size();
		Date lastConversationDate = null;
		
		for (Student student : students) {
			StudentProfile profile = getProfileByStudentId(student.getId());
			
			if (profile != null) {
				Set<StudentConversation> studentConver= new HashSet<StudentConversation>(studentConversationRepo.findByStudentIdOrderByConversationStartDateDesc(student.getId()));
				profile.setStudentConversations(studentConver);
			}
			
			StudentCompletionSummary studentCompletionSummary = new StudentCompletionSummary();
			
			if (profile != null) {
				
				profile.setMySkillsSummary(Util.getProgressPercentageByNumberOfObjects(profile, Util.profileCalcMySkills, totalNumberOfSkillsHabits));
								
				semesterRepo.findAllByOrderBySemesterIdAsc().stream().forEach(semesterItem -> {					
					StudentSkillSummary studentSkillSummary = new StudentSkillSummary();
					studentSkillSummary.setYear(semesterItem.getSemesterYear().toString());
					studentSkillSummary.setSemester(semesterItem.getSemesterId());
					Integer totalTeacherApprovedSemesters = 0;  //habitRepo.findTeacherApprovedStudentHabitGrades(profile.getId(), semesterItem.getSemesterId()) + skillFocusLevelRepo.findTeacherApprovedStudentSkillFocusLevels(profile.getId(), semesterItem.getSemesterId());
					studentSkillSummary.setTeacherApproved( (totalTeacherApprovedSemesters * 100) / totalNumberOfSkillsHabits);		
					studentCompletionSummary.addSkillSummary(studentSkillSummary);					
				});
				
				lastConversationDate = studentConversationRepo.findMaxUpdatedDateByStudentId(profile.getId());
								
			}

			String myProfileTeacherApproved = "";
			String myProfileStudentCompleted = Integer.toString(Util.getProgressPercentage(profile, Util.profileCalcMyProfile));
			String myGoalsTeacherApproved = "";
			String myGoalsStudentCompleted = Integer.toString(Util.getProgressPercentage(profile, Util.profileCalcMyGoals));
			String myExperienceTeacherApproved = "";
			String myExperienceStudentCompleted = "";
			String myPlacementTeacherApproved = "";
			String myPlacementStudentCompleted = "";
			
			if (profile != null) {
				myExperienceStudentCompleted = Integer.toString(Util.getStudentJobHoursByJobType(profile.getStudentJobs(), Util.MYEXPERIENCES_TYPE));
				myExperienceTeacherApproved  = Integer.toString(Util.getApprovedStudentJobHoursByJobType(profile.getStudentJobs(), Util.MYEXPERIENCES_TYPE));				
				myPlacementStudentCompleted = Integer.toString(Util.getStudentJobCountByJobType(profile.getStudentJobs(), Util.MYPLACEMENTS_TYPE) + profile.getStudentJobReflections().size());
			}

			if ((myProfileTeacherApproved = studentSectionCompletionRepo.findPecentageCompletionBySectionGroup(student.getId(), "MyProfile")) == null) {
				myProfileTeacherApproved = "0";
			}
			if ((myGoalsTeacherApproved = studentSectionCompletionRepo.findPecentageCompletionBySectionGroup(student.getId(), "MyGoals")) == null) {
				myGoalsTeacherApproved = "0";
			}

			String myPlacement = "";
			Integer totalMyPlacementTeacherApproved = 0;
			if ( (myPlacement = studentSectionCompletionRepo.findTeacherApprovedRecordsBySection(student.getId(), "MyPlacement") ) != null) {
				totalMyPlacementTeacherApproved += Integer.parseInt(myPlacement);
			}

			String myReflection = "";
			if ( (myReflection = studentSectionCompletionRepo.findTeacherApprovedRecordsBySection(student.getId(), "MyReflection") ) != null) {
				totalMyPlacementTeacherApproved += Integer.parseInt(myReflection);
			}

			myPlacementTeacherApproved = Integer.toString(totalMyPlacementTeacherApproved);

			studentCompletionSummary.setMyProfileStudentCompleted(myProfileStudentCompleted + "%");
			studentCompletionSummary.setMyProfileTeacherApproved(myProfileTeacherApproved + "%");
			studentCompletionSummary.setMyGoalStudentCompleted(myGoalsStudentCompleted + "%");
			studentCompletionSummary.setMyGoalTeacherApproved(myGoalsTeacherApproved + "%");
			studentCompletionSummary.setMyExperienceStudentCompleted(myExperienceStudentCompleted);
			studentCompletionSummary.setMyExperienceTeacherApproved(myExperienceTeacherApproved);
			studentCompletionSummary.setMyPlacementStudentCompleted(myPlacementStudentCompleted);
			studentCompletionSummary.setMyPlacementTeacherApproved(myPlacementTeacherApproved);
			
			if (profile != null) {
				profile.setStudentCompletionSummary(studentCompletionSummary);
				profile.setLastConversationDate(lastConversationDate);
			}

			student.setProfile(profile);
		}
		
		return students;
	}


	/**
	 * Saves teacher marking for Areas(Habits/Skills) and teacher approvals/comments for MyExperiences/MyPlacements/JobReflections/StudentSectionCompletions
	 * @param userId
	 * @param gradeReviewDto
	 * @throws Exception
	 */
	public void saveTeacherReview(int userId, GradeReviewDTO gradeReviewDto) throws Exception {
				
		int studentId = gradeReviewDto.getStudentId() != null ? gradeReviewDto.getStudentId() : userId;
		StudentProfile profile = this.getProfileByStudentId(studentId);

		if (profile != null) {
			Date currentDate = new Date();
			
			/* Set student career goals - teacher comments */
			profile.getStudentCareerGoals().retainAll(gradeReviewDto.getStudentCareerGoals());
			gradeReviewDto
				.getStudentCareerGoals()
					.stream()
						.forEach(careerGoal -> {
							if(careerGoal.getId() != null) {
								StudentCareerGoal currentStudentCareerGoal = studentCareerGoalRepo.findOne(careerGoal.getId());
								currentStudentCareerGoal.setTeacherFeedback(careerGoal.getTeacherFeedback());
								profile.addCareerGoal(currentStudentCareerGoal);
							}
						}
					);
			
			/* Set student industry goals - teacher comments */
			profile.getStudentIndustryGoals().retainAll(gradeReviewDto.getStudentIndustryGoals());
			gradeReviewDto
				.getStudentIndustryGoals()
					.stream()
						.forEach(industryGoal -> {
							if(industryGoal.getId() != null) {
								StudentIndustryGoal currentStudentIndustryGoal = studentIndustryGoalRepo.findOne(industryGoal.getId());
								currentStudentIndustryGoal.setTeacherFeedback(industryGoal.getTeacherFeedback());
								profile.addIndustryGoal(currentStudentIndustryGoal);
							}							
						}
					);
				
						
			
			/* Set teacher approvals and comments for StudentJobs (MyPlacements, MyExperiences) */
			profile.getStudentJobs().retainAll(gradeReviewDto.getStudentJobs());
			gradeReviewDto
				.getStudentJobs()
					.stream()
						.forEach( studentJob -> {
									if(studentJob.getId() != null) {
										StudentJob currentStudentJob = studentJobRepo.findOne(studentJob.getId());
										currentStudentJob.setApproved(studentJob.getApproved());
										currentStudentJob.setTeacherComments(studentJob.getTeacherComments());
										profile.addStudentJob(currentStudentJob);
									}									
								}
							);
			
			/* Set teacher approvals for Student Job Reflections */
			profile.getStudentJobReflections().retainAll(gradeReviewDto.getStudentJobReflections());		
			gradeReviewDto
				.getStudentJobReflections()
					.stream()
						.forEach( jobReflection -> {
									if(jobReflection.getId() != null) {
										StudentJobReflection currentStudentJobReflection = studentJobReflectionRepo.findOne(jobReflection.getId());
										currentStudentJobReflection.setApproved(jobReflection.getApproved());		
										profile.addStudentJobReflection(currentStudentJobReflection);
									}
								}								
							);
			
						
			
			/* Set Grade marks - teacher marking */
			profile.getStudentGrades().retainAll(gradeReviewDto.getStudentGrades());
			gradeReviewDto
				.getStudentGrades()
					.stream()
						.forEach( studentGrade -> {
									if(studentGrade.getId() == null) {
										profile.addStudentGrade( new StudentGrade( studentGrade.getRubricId(), studentGrade.getSemesterId(), null, currentDate, studentGrade.getIsTeacherGrade(), false ) );
									} else {
										StudentGrade currentStudentGrade = studentGradeRepo.findOne(studentGrade.getId());
										currentStudentGrade.setRubricId(studentGrade.getRubricId());
										currentStudentGrade.setIsTeacherGrade(studentGrade.getIsTeacherGrade());
										currentStudentGrade.setTeacherId(profile.getClassTeacherId());
										currentStudentGrade.setTeacherMarkDate(currentDate);
										profile.addStudentGrade(currentStudentGrade);
									}
								}
							
							);
			
			/* Set habits/skills - teacher comments */
			profile.getStudentGradeComments().retainAll(gradeReviewDto.getStudentGradeComments());
			gradeReviewDto
				.getStudentGradeComments()
					.stream()
						.forEach( comment -> {
									if(comment.getId() == null) {
										Area area = areaRepo.findOne(comment.getAreaId());
										profile.addStudentGradeComment(
															new StudentGradeComment(profile, comment.getTeacherId(), area, comment.getSemesterId(), comment.getTeacherComment(), comment.getStudentComment(), comment.getTeacherCommentDate(), comment.getStudentCommentDate() )
												          );
									} else {
										StudentGradeComment currentComment = studentGradeCommentRepo.findOne(comment.getId());
										currentComment.setTeacherComment(comment.getTeacherComment());
										currentComment.setTeacherCommentDate(currentDate);
										profile.addStudentGradeComment(currentComment);
									}
								}
							);
			
								
			/* Set Student Section Completions */
			profile.getStudentSectionCompletions().retainAll(gradeReviewDto.getStudentSectionCompletions());
			gradeReviewDto
				.getStudentSectionCompletions()
					.stream()
						.forEach( sectionCompletion -> {
									if(sectionCompletion.getId() == null){
										profile.addStudentSectionCompletion( new StudentSectionCompletion(sectionCompletion.getMyPlanWeightageId(), sectionCompletion.getCompleted()));
									} else {
										StudentSectionCompletion currentStudentSectionCompletion = studentSectionCompletionRepo.findOne(sectionCompletion.getId());
										profile.addStudentSectionCompletion(currentStudentSectionCompletion);
									}
								}
							);											
			
			profileRepo.save(profile);
		}
	}
	

	/**
	 * Saves student self marking for Areas(Habits/Skills)
	 * @param userId
	 * @param gradeReviewDto
	 * @throws Exception
	 */
	public void saveStudentReview(int userId, GradeReviewDTO gradeReviewDto) throws Exception {
		
		int studentId = gradeReviewDto.getStudentId() != null ? gradeReviewDto.getStudentId() : userId;
		StudentProfile profile = this.getProfileByStudentId(studentId);
		
		if (profile != null) {
			Date currentDate = new Date();
									
			/* Set Grade marks - student marking */
			profile.getStudentGrades().retainAll(gradeReviewDto.getStudentGrades());
			gradeReviewDto
				.getStudentGrades()
					.stream()
						.forEach( studentGrade -> {
									if(studentGrade.getId() == null) {
										profile.addStudentGrade( new StudentGrade(studentGrade.getRubricId(), studentGrade.getSemesterId(), currentDate, null, false, studentGrade.getIsStudentGrade() )  );
									} else {
										StudentGrade currentStudentGrade = studentGradeRepo.findOne(studentGrade.getId());
										currentStudentGrade.setRubricId(studentGrade.getRubricId());
										currentStudentGrade.setIsStudentGrade(studentGrade.getIsStudentGrade());
										currentStudentGrade.setStudentMarkDate(currentDate);
										profile.addStudentGrade(studentGrade);
									}
								}
							
							);
			
			/* Set habits/skills - student comments */
			profile.getStudentGradeComments().retainAll(gradeReviewDto.getStudentGradeComments());
			gradeReviewDto
				.getStudentGradeComments()
					.stream()
						.forEach( comment -> {
									if(comment.getId() == null) {
										Area area = areaRepo.findOne(comment.getAreaId());
										profile.addStudentGradeComment(
															new StudentGradeComment(profile, comment.getTeacherId(), area, comment.getSemesterId(), comment.getTeacherComment(), comment.getStudentComment(), comment.getTeacherCommentDate(), currentDate )
												          );
									} else {
										StudentGradeComment currentStudentGradeComment = studentGradeCommentRepo.findOne(comment.getId());
										currentStudentGradeComment.setStudentComment(comment.getStudentComment());
										currentStudentGradeComment.setStudentCommentDate(currentDate);
										profile.addStudentGradeComment(currentStudentGradeComment);
									}
								}
							);
			

			profileRepo.save(profile);
		}
	}
	
	
	/**
	 * get areas order by area type
	 * @return List<Area>
	 */
	public List<Area> getAreas() {
		return areaRepo.findAllByOrderByAreaTypeId();
	}
   
}
