package au.com.sedagroup.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.sedagroup.model.Document;
import au.com.sedagroup.repository.DocumentRepository;

@Service
public class DocumentService {
	@Autowired
	private DocumentRepository documentRepository;
	
	public Document save(Document document){
		return documentRepository.save(document);
	}
	public Document getDocumentById(Integer id){
		return documentRepository.findOne(id);
	}
	public List<Document> getAllDocuments(){
		return  (List<Document>) documentRepository.findAll();
	}
}
