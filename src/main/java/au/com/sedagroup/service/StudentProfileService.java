package au.com.sedagroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.repository.StudentProfileRepository;

@Service
@Transactional
public class StudentProfileService {
	@Autowired
	private StudentProfileRepository profileRepository;
	
	public StudentProfile save(StudentProfile studentProfile){
		return profileRepository.save(studentProfile);
	}
	public StudentProfile getStudentProfile(Integer studentId){
		return profileRepository.findOne(studentId);
	}
}
