package au.com.sedagroup.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ServletContextTemplateResolver;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfWriter;

import au.com.sedagroup.dao.SynergeticDAO;
import au.com.sedagroup.dto.ExperiencesDTO;
import au.com.sedagroup.dto.GoalsDTO;
import au.com.sedagroup.dto.PlacementsDTO;
import au.com.sedagroup.dto.ReviewDTO;
import au.com.sedagroup.dto.Student;
import au.com.sedagroup.dto.StudentContactDetails;
import au.com.sedagroup.dto.StudentProfileDTO;
import au.com.sedagroup.exception.StudentProfileNotFoundException;
import au.com.sedagroup.model.Document;
import au.com.sedagroup.model.Grade;
import au.com.sedagroup.model.Habit;
import au.com.sedagroup.model.MyPlanWeightage;
import au.com.sedagroup.model.Semester;
import au.com.sedagroup.model.SkillArea;
import au.com.sedagroup.model.SkillFocusArea;
import au.com.sedagroup.model.SkillFocusLevel;
import au.com.sedagroup.model.StudentCareerGoal;
import au.com.sedagroup.model.StudentCompletionSummary;
import au.com.sedagroup.model.StudentConversation;
import au.com.sedagroup.model.StudentDocument;
import au.com.sedagroup.model.StudentDocumentData;
import au.com.sedagroup.model.StudentSkillFocusArea;
import au.com.sedagroup.model.StudentHabitGrade;
import au.com.sedagroup.model.StudentHabitGradeComment;
import au.com.sedagroup.model.StudentIndustryGoal;
import au.com.sedagroup.model.StudentJob;
import au.com.sedagroup.model.StudentJobReflection;
import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.model.StudentSectionCompletion;
import au.com.sedagroup.model.StudentSkillFocusLevel;
import au.com.sedagroup.model.StudentSkillSummary;
import au.com.sedagroup.model.Supervisor;
import au.com.sedagroup.model.User;
import au.com.sedagroup.repository.DocumentRepository;
import au.com.sedagroup.repository.GradeRepository;
import au.com.sedagroup.repository.HabitRepository;
import au.com.sedagroup.repository.MyPlanWeightageRepository;
import au.com.sedagroup.repository.SemesterRepository;
import au.com.sedagroup.repository.SkillAreaRepository;
import au.com.sedagroup.repository.SkillFocusAreaRepository;
import au.com.sedagroup.repository.SkillFocusLevelRepository;
import au.com.sedagroup.repository.StudentCareerGoalRepository;
import au.com.sedagroup.repository.StudentConversationRepository;
import au.com.sedagroup.repository.StudentSectionCompletionRepository;
import au.com.sedagroup.repository.StudentSkillFocusAreaRepository;
import au.com.sedagroup.repository.StudentSkillFocusLevelRepository;
import au.com.sedagroup.repository.StudentDocumentRepository;
import au.com.sedagroup.repository.StudentHabitGradeCommentRepository;
import au.com.sedagroup.repository.StudentHabitGradeRepository;
import au.com.sedagroup.repository.StudentIndustryGoalRepository;
import au.com.sedagroup.repository.StudentJobReflectionRepository;
import au.com.sedagroup.repository.StudentJobRepository;
import au.com.sedagroup.repository.StudentProfileRepository;
import au.com.sedagroup.repository.UserRepository;
import au.com.sedagroup.util.ImageReplacedElementFactory;
import au.com.sedagroup.util.Util;

@Service
@Transactional
public class ApplicationService {

	@Autowired
	private StudentProfileRepository profileRepo;
	@Autowired
	private DocumentRepository documentRepo;
	@Autowired
	private StudentDocumentRepository studentDocumentRepo;
	@Autowired
	private GradeRepository gradeRepo;
	@Autowired
	private HabitRepository habitRepo;
	@Autowired
	private SkillAreaRepository skillAreaRepo;
	@Autowired
	private SkillFocusLevelRepository skillFocusLevelRepo;
	@Autowired
	private SkillFocusAreaRepository skillFocusAreaRepo;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private MyPlanWeightageRepository MyPlanWeightageRepo;
	@Autowired
	private StudentSectionCompletionRepository studentSectionCompletionRepo;
	@Autowired
	private SemesterRepository semesterRepo;
	@Autowired
	private StudentConversationRepository studentConversationRepo;
	@Autowired
	private SynergeticDAO synergeticDao;
	@Autowired
	private StudentCareerGoalRepository studentCareerGoalRepo;
	@Autowired
	private StudentIndustryGoalRepository studentIndustryGoalRepo;
	@Autowired
	private StudentHabitGradeRepository studentHabitGradeRepo;
	@Autowired
	private StudentSkillFocusAreaRepository studentSkillFocusAreaRepo;
	@Autowired
	private StudentSkillFocusLevelRepository studentSkillFocusLevelRepo;
	@Autowired
	private StudentHabitGradeCommentRepository studentHabitGradeCommentRepo;
	@Autowired
	private StudentJobRepository studentJobRepo;
	@Autowired
	private StudentJobReflectionRepository studentJobReflectionRepo;
	@Autowired
	private ReviewService reviewService;
	
	@Resource(name = "configuration")
	private Properties properties;
	
	private Logger logger = LoggerFactory.getLogger(ApplicationService.class);
		
	public StudentProfile saveStudentProfile(Integer userId, StudentProfileDTO studentProfileDTO) throws StudentProfileNotFoundException {
		StudentProfile profile = profileRepo.findOne(studentProfileDTO.getStudentId());
		
		if(profile == null){
			throw new StudentProfileNotFoundException("Profile not found for studentId : " + studentProfileDTO.getStudentId());
		}
		
		/* Set Student profile properties */
		profile.setStudentName(studentProfileDTO.getStudentName());
		profile.setMybio(studentProfileDTO.getMybio());
		
		/* Set family members */
		profile.getFamilyMembers().clear();
		studentProfileDTO.getFamilyMembers().stream().forEach(familyMember -> profile.addFamilyMember(familyMember));	

		/* Set student networks */
		profile.getStudentNetworks().clear();
		studentProfileDTO.getStudentNetworks().stream().forEach(network -> profile.addStudentNetwork(network));		
				
		profileRepo.save(profile);		
		return getProfileByStudentId(studentProfileDTO.getStudentId());
	}

	public StudentProfile getProfileByStudentId(int studentId) {
		
		StudentProfile profile = profileRepo.findOne(studentId);
		
		if (profile != null) {
			// This way is faster than doing it as eager relations			
			Hibernate.initialize(profile.getFamilyMembers());
			Hibernate.initialize(profile.getStudentNetworks());
			Hibernate.initialize(profile.getStudentDocuments());
			Hibernate.initialize(profile.getStudentCareerGoals());
			Hibernate.initialize(profile.getStudentIndustryGoals());
			Hibernate.initialize(profile.getStudentJobs());
			Hibernate.initialize(profile.getStudentJobReflections());
			Hibernate.initialize(profile.getStudentHabitGrades());
			Hibernate.initialize(profile.getStudentHabitGradeComments());
			Hibernate.initialize(profile.getStudentSkillFocusAreas());
			Hibernate.initialize(profile.getStudentSkillFocusLevels());
			Hibernate.initialize(profile.getStudentSectionCompletions());
			profile.setClassTeacherId(synergeticDao.getClassTeacherByStudentId(studentId));			
		}
		return profile;
	}
	
	public void saveMyGoals(GoalsDTO goalsDTO) {
		StudentProfile profile = this.getProfileByStudentId(goalsDTO.getStudentId());		
		/* Set career goals */
		profile.getStudentCareerGoals().clear();
		goalsDTO.getStudentCareerGoals().stream().forEach(careerGoal -> profile.addCareerGoal(careerGoal));
		/* Set industry goals */
		profile.getStudentIndustryGoals().clear();
		goalsDTO.getStudentIndustryGoals().stream().forEach(industryGoal -> profile.addIndustryGoal(industryGoal));		
		profileRepo.save(profile);
		// TODO - only reload goals from db ??
	}
	
	public void saveMyPlacements(PlacementsDTO placementsDTO) {
		StudentProfile profile = this.getProfileByStudentId(placementsDTO.getStudentId());
		/* Set student work placements */
	    profile.getStudentJobs().clear();
		placementsDTO.getStudentWorkPlacements().stream().forEach(workPlacement -> profile.addStudentJob(workPlacement));
		/* Set student job reflections */
		profile.getStudentJobReflections().clear();
		placementsDTO.getStudentJobReflections().stream().forEach(studentJobReflection -> profile.addStudentJobReflection(studentJobReflection));
		profileRepo.save(profile);
	}
	
	public void saveMyExperiences(ExperiencesDTO experiencesDTO) {
		StudentProfile profile = this.getProfileByStudentId(experiencesDTO.getStudentId());
		/* Set student work experiences */
		profile.getStudentJobs().clear();
		experiencesDTO.getJobExperiences().stream().forEach(jobExperience -> profile.addStudentJob(jobExperience));
		profileRepo.save(profile);
	}	

	public List<Document> getDocuments() {
		return (List<Document>) documentRepo.findAll();
	}

	public Document getDocumentByType(String documentType) {
		return documentRepo.findByDocumentType(documentType);
	}

	public String deleteDocument(Integer documentId) {
		StudentDocument document  = studentDocumentRepo.findById(documentId);
		document.getStudentDocumentDataList(); // Load/Init StudentDocumentDataList before deleting
		studentDocumentRepo.delete(document);
		return "Sucess";
	}

	public List<Student> getStudentsProfile(List<Student> students)	throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		
		// TODO - change this - after marking semester 1 - 2020
		int totalNumberOfSkillsHabits = this.getAllHabits().size() + skillFocusAreaRepo.findAllActive().size();
		Date lastConversationDate = null;
		
		for (Student student : students) {
			//StudentProfile profile = getProfileByStudentId(student.getId());
			StudentProfile profile = reviewService.getProfileByStudentId(student.getId());
			
			if(profile != null) {
				Set<StudentConversation> studentConver= new HashSet<StudentConversation>(studentConversationRepo.findByStudentIdOrderByConversationStartDateDesc(student.getId()));
				profile.setStudentConversations(studentConver);
			}
			
			StudentCompletionSummary studentCompletionSummary = new StudentCompletionSummary();
			
			if (profile != null) {
				profile.setMySkillsSummary(Util.getProgressPercentageByNumberOfObjects(profile, Util.profileCalcMySkills, totalNumberOfSkillsHabits));
								
				semesterRepo.findAllByOrderBySemesterIdAsc().stream().forEach(semesterItem -> {
					
					StudentSkillSummary studentSkillSummary = new StudentSkillSummary();
					studentSkillSummary.setYear(semesterItem.getSemesterYear().toString());
					studentSkillSummary.setSemester(semesterItem.getSemesterId());
					Integer totalTeacherApprovedSemesters =   habitRepo.findTeacherApprovedStudentHabitGrades(profile.getId(), semesterItem.getSemesterId()) 
															+ skillFocusLevelRepo.findTeacherApprovedStudentSkillFocusLevels(profile.getId(), semesterItem.getSemesterId());
					studentSkillSummary.setTeacherApproved( (totalTeacherApprovedSemesters * 100) / totalNumberOfSkillsHabits);		
					studentCompletionSummary.addSkillSummary(studentSkillSummary);
					
				});
				
				lastConversationDate = studentConversationRepo.findMaxUpdatedDateByStudentId(profile.getId());

								
			}

			String myProfileTeacherApproved = "";
			String myProfileStudentCompleted = Integer.toString(Util.getProgressPercentage(profile, Util.profileCalcMyProfile));
			String myGoalsTeacherApproved = "";
			String myGoalsStudentCompleted = Integer.toString(Util.getProgressPercentage(profile, Util.profileCalcMyGoals));
			String myExperienceTeacherApproved = "";
			String myExperienceStudentCompleted = "";
			String myPlacementTeacherApproved = "";
			String myPlacementStudentCompleted = "";
			
			if (profile != null) {
				myExperienceStudentCompleted = Integer.toString(Util.getStudentJobHoursByJobType(profile.getStudentJobs(), Util.MYEXPERIENCES_TYPE));
				myExperienceTeacherApproved  = Integer.toString(Util.getApprovedStudentJobHoursByJobType(profile.getStudentJobs(), Util.MYEXPERIENCES_TYPE));				
				myPlacementStudentCompleted = Integer.toString(Util.getStudentJobCountByJobType(profile.getStudentJobs(), Util.MYPLACEMENTS_TYPE) + profile.getStudentJobReflections().size());
			}

			if ((myProfileTeacherApproved = studentSectionCompletionRepo.findPecentageCompletionBySectionGroup(student.getId(), "MyProfile")) == null) {
				myProfileTeacherApproved = "0";
			}
			if ((myGoalsTeacherApproved = studentSectionCompletionRepo.findPecentageCompletionBySectionGroup(student.getId(), "MyGoals")) == null) {
				myGoalsTeacherApproved = "0";
			}

			String myPlacement = "";
			Integer totalMyPlacementTeacherApproved = 0;
			if ( (myPlacement = studentSectionCompletionRepo.findTeacherApprovedRecordsBySection(student.getId(), "MyPlacement") ) != null) {
				totalMyPlacementTeacherApproved += Integer.parseInt(myPlacement);
			}

			String myReflection = "";
			if ( (myReflection = studentSectionCompletionRepo.findTeacherApprovedRecordsBySection(student.getId(), "MyReflection") ) != null) {
				totalMyPlacementTeacherApproved += Integer.parseInt(myReflection);
			}

			myPlacementTeacherApproved = Integer.toString(totalMyPlacementTeacherApproved);

			studentCompletionSummary.setMyProfileStudentCompleted(myProfileStudentCompleted + "%");
			studentCompletionSummary.setMyProfileTeacherApproved(myProfileTeacherApproved + "%");
			studentCompletionSummary.setMyGoalStudentCompleted(myGoalsStudentCompleted + "%");
			studentCompletionSummary.setMyGoalTeacherApproved(myGoalsTeacherApproved + "%");
			studentCompletionSummary.setMyExperienceStudentCompleted(myExperienceStudentCompleted);
			studentCompletionSummary.setMyExperienceTeacherApproved(myExperienceTeacherApproved);
			studentCompletionSummary.setMyPlacementStudentCompleted(myPlacementStudentCompleted);
			studentCompletionSummary.setMyPlacementTeacherApproved(myPlacementTeacherApproved);
			
			

			if (profile != null) {
				profile.setStudentCompletionSummary(studentCompletionSummary);
				profile.setLastConversationDate(lastConversationDate);
			}

			student.setProfile(profile);
		}
		return students;
	}

	public List<Grade> getAllGrades() {
		return (List<Grade>) gradeRepo.findAllByOrderBySortValueAsc();
	}

	public List<Habit> getAllHabits() {
		return (List<Habit>) habitRepo.findAllByOrderByIdAsc();
	}

	public List<SkillArea> getSkillAreas() {
		return skillAreaRepo.findAllByOrderByIdAsc();
	}

	public SkillFocusArea findSkillFocusAreaById(int skillFocusAreaId) {
		return skillFocusAreaRepo.findOne(skillFocusAreaId);
	}

	public SkillFocusLevel findSkillFocusLevelById(int skillFocusLevelId) {
		return skillFocusLevelRepo.findOne(skillFocusLevelId);
	}

	public Grade findGradeById(int gradeId) {
		return gradeRepo.findOne(gradeId);
	}

	public Habit findHabitById(int id) {
		return habitRepo.findOne(id);
	}

	/**
	 * Saves teacher marking for Habits/Skills and teacher approvals/comments for MyExperiences/MyPlacements/JobReflections and StudentSectionCompletions
	 * @param userId
	 * @param reviewDto
	 * @throws Exception
	 */
	public void saveTeacherReview(int userId, ReviewDTO reviewDto) throws Exception {
				
		int studentId = reviewDto.getStudentId() != null ? reviewDto.getStudentId() : userId;
		StudentProfile profile = this.getProfileByStudentId(studentId);

		if (profile != null) {
			Date currentDate = new Date();
			
			/* Set student career goals - teacher comments */
			profile.getStudentCareerGoals().retainAll(reviewDto.getStudentCareerGoals());
			reviewDto
				.getStudentCareerGoals()
					.stream()
						.forEach(careerGoal -> {
							if(careerGoal.getId() != null) {
								StudentCareerGoal currentStudentCareerGoal = studentCareerGoalRepo.findOne(careerGoal.getId());
								currentStudentCareerGoal.setTeacherFeedback(careerGoal.getTeacherFeedback());
								profile.addCareerGoal(currentStudentCareerGoal);
							}
						}
					);
			
			/* Set student industry goals - teacher comments */
			profile.getStudentIndustryGoals().retainAll(reviewDto.getStudentIndustryGoals());
			reviewDto
				.getStudentIndustryGoals()
					.stream()
						.forEach(industryGoal -> {
							if(industryGoal.getId() != null) {
								StudentIndustryGoal currentStudentIndustryGoal = studentIndustryGoalRepo.findOne(industryGoal.getId());
								currentStudentIndustryGoal.setTeacherFeedback(industryGoal.getTeacherFeedback());
								System.out.println(industryGoal.getTeacherFeedback());
								profile.addIndustryGoal(currentStudentIndustryGoal);
							}							
						}
					);
				
			
			/* Set student Habit Grades - teacher marking */	
			profile.getStudentHabitGrades().retainAll(reviewDto.getHabitsGrades());
			reviewDto
				.getHabitsGrades()
					.stream()
						.forEach(habitGrade -> {
									if(habitGrade.getId() == null) {
										profile.addStudentHabitGrade( new StudentHabitGrade( habitGrade.getGrade(), habitGrade.getHabit(), currentDate, habitGrade.getSemesterId(), habitGrade.getIsTeacherGrade(), habitGrade.getIsStudentGrade() ) );	
									} else {
										StudentHabitGrade currentHabitGrade = studentHabitGradeRepo.findOne(habitGrade.getId());
										currentHabitGrade.setIsTeacherGrade(habitGrade.getIsTeacherGrade());
										profile.addStudentHabitGrade(currentHabitGrade);										
									}														
								}
							);
			
			/* Set student habit grade comments entered by teacher */
			profile.getStudentHabitGradeComments().retainAll(reviewDto.getHabitGradeComments());
			reviewDto
				.getHabitGradeComments()
					.stream()
						.forEach( habitGradeComment -> {
									if(habitGradeComment.getId() == null) {
										profile.addStudentHabitGradeComment(new StudentHabitGradeComment(habitGradeComment.getHabit(), habitGradeComment.getGradeComments(), habitGradeComment.getSemesterId()));
									} else {
										StudentHabitGradeComment currentStudentHabitGradeComment = studentHabitGradeCommentRepo.findOne(habitGradeComment.getId());
										currentStudentHabitGradeComment.setGradeComments(habitGradeComment.getGradeComments());
										profile.addStudentHabitGradeComment(currentStudentHabitGradeComment);
									}
								}
							);
			
			/*  Set student skill focus areas - teacher marking
			 ** This does not delete studentSkillFocusArea record once entered, teacher comment is retained.
			 */
			profile.getStudentSkillFocusAreas().retainAll(reviewDto.getFocusAreas());
			reviewDto
				.getFocusAreas()
					.stream()
						.forEach(focusArea -> {
									if(focusArea.getId() == null ){
										profile.addStudentSkillFocusArea( new StudentSkillFocusArea( this.findSkillFocusAreaById(focusArea.getSkillFocusAreaId()), focusArea.getSemesterId(), currentDate, focusArea.getTeacherComments() ) );
									} else {
										StudentSkillFocusArea currentSkillFocusArea = studentSkillFocusAreaRepo.findOne(focusArea.getId());
										currentSkillFocusArea.setTeacherComments(focusArea.getTeacherComments());
										currentSkillFocusArea.setStudentSkillDate(currentDate);
										profile.addStudentSkillFocusArea(currentSkillFocusArea);
									}
								}
							);
			
			/* Set student skill focus levels - teacher marking */
			profile.getStudentSkillFocusLevels().retainAll(reviewDto.getFocusLevels());
			reviewDto
				.getFocusLevels()
					.stream()
						.forEach( focusLevel -> {
									if(focusLevel.getId() == null) {
										SkillFocusLevel skillFocusLevel = skillFocusLevelRepo.findOne(focusLevel.getSkillFocusLevelId());
										profile.addStudentSkillFocusLevel( new StudentSkillFocusLevel( skillFocusLevel, focusLevel.getSemesterId(), currentDate, focusLevel.getIsTeacherGrade(), focusLevel.getIsStudentGrade() ) );
									} else {
										StudentSkillFocusLevel currentSkillFocusLevel = studentSkillFocusLevelRepo.findOne(focusLevel.getId());
										currentSkillFocusLevel.setIsTeacherGrade(focusLevel.getIsTeacherGrade());
										currentSkillFocusLevel.setStudentSkillDate(currentDate);
										profile.addStudentSkillFocusLevel(currentSkillFocusLevel);
									}
								}
							
							);
						
			
						
			/* Set teacher approvals and comments for StudentJobs (MyPlacements, MyExperiences) */
			profile.getStudentJobs().retainAll(reviewDto.getStudentJobs());
			reviewDto
				.getStudentJobs()
					.stream()
						.forEach( studentJob -> {
									if(studentJob.getId() != null) {
										StudentJob currentStudentJob = studentJobRepo.findOne(studentJob.getId());
										currentStudentJob.setApproved(studentJob.getApproved());
										currentStudentJob.setTeacherComments(studentJob.getTeacherComments());
										profile.addStudentJob(currentStudentJob);
									}									
								}
							);
			
			/* Set teacher approvals for Student Job Reflections */
			profile.getStudentJobReflections().retainAll(reviewDto.getStudentJobReflections());		
			reviewDto
				.getStudentJobReflections()
					.stream()
						.forEach( jobReflection -> {
									if(jobReflection.getId() != null) {
										StudentJobReflection currentStudentJobReflection = studentJobReflectionRepo.findOne(jobReflection.getId());
										currentStudentJobReflection.setApproved(jobReflection.getApproved());		
										profile.addStudentJobReflection(currentStudentJobReflection);
									}
								}								
							);
			
			
			/* Set Student Section Completions */
			profile.getStudentSectionCompletions().retainAll(reviewDto.getStudentSectionCompletions());
			reviewDto
				.getStudentSectionCompletions()
					.stream()
						.forEach( sectionCompletion -> {
									if(sectionCompletion.getId() == null){
										profile.addStudentSectionCompletion( new StudentSectionCompletion(sectionCompletion.getMyPlanWeightageId(), sectionCompletion.getCompleted()));
									} else {
										StudentSectionCompletion currentStudentSectionCompletion = studentSectionCompletionRepo.findOne(sectionCompletion.getId());
										profile.addStudentSectionCompletion(currentStudentSectionCompletion);
									}
								}
							);											
			
			profileRepo.save(profile);
		}
	}
	

	/**
	 * Saves student self marking for Habits and Skills
	 * @param userId
	 * @param reviewDto
	 * @throws Exception
	 */
	public void saveStudentReview(int userId, ReviewDTO reviewDto) throws Exception {
		
		int studentId = reviewDto.getStudentId() != null ? reviewDto.getStudentId() : userId;
		StudentProfile profile = this.getProfileByStudentId(studentId);
		
		if (profile != null) {
			Date currentDate = new Date();
						
			/* Set student Habit Grades */	
			profile.getStudentHabitGrades().retainAll(reviewDto.getHabitsGrades());
			reviewDto
				.getHabitsGrades()
					.stream()
						.forEach(habitGrade -> {
									if(habitGrade.getId() == null) {
										profile.addStudentHabitGrade( new StudentHabitGrade( habitGrade.getGrade(), habitGrade.getHabit(), currentDate, habitGrade.getSemesterId(), habitGrade.getIsTeacherGrade(), habitGrade.getIsStudentGrade() ) );	
									} else {
										StudentHabitGrade currentHabitGrade = studentHabitGradeRepo.findOne(habitGrade.getId());
										profile.addStudentHabitGrade(currentHabitGrade);
									}														
								}
							);
			
			/* Set student skill focus areas */
			profile.getStudentSkillFocusAreas().retainAll(reviewDto.getFocusAreas());
			reviewDto
				.getFocusAreas()
					.stream()
						.forEach(focusArea -> {
							if(focusArea.getId() == null ){
								profile.addStudentSkillFocusArea( new StudentSkillFocusArea( this.findSkillFocusAreaById(focusArea.getSkillFocusAreaId()), focusArea.getSemesterId(), currentDate, focusArea.getTeacherComments() ) );
							} else {
								StudentSkillFocusArea currentSkillFocusArea = studentSkillFocusAreaRepo.findOne(focusArea.getId());
								profile.addStudentSkillFocusArea(currentSkillFocusArea);
							}
						}
				);
			
			/* Set student skill focus levels */
			profile.getStudentSkillFocusLevels().retainAll(reviewDto.getFocusLevels());
			reviewDto
				.getFocusLevels()
					.stream()
						.forEach( focusLevel -> {
							if(focusLevel.getId() == null) {
								SkillFocusLevel skillFocusLevel = skillFocusLevelRepo.findOne(focusLevel.getSkillFocusLevelId());
								profile.addStudentSkillFocusLevel( new StudentSkillFocusLevel( skillFocusLevel, focusLevel.getSemesterId(), currentDate, focusLevel.getIsTeacherGrade(), focusLevel.getIsStudentGrade() ) );
							} else {
								StudentSkillFocusLevel currentSkillFocusLevel = studentSkillFocusLevelRepo.findOne(focusLevel.getId());
								profile.addStudentSkillFocusLevel(currentSkillFocusLevel);
							}
						}
							
					);

			profileRepo.save(profile);
		}
	}

	public void saveSupervisors(Integer userId, List<Supervisor> supervisors) {

		supervisors.stream().forEach(supervisor -> {
			User user = userRepo.findByUserId(supervisor.getId());

			if (user != null) {
				user.setRoleId(supervisor.getRoleId());
			} else {
				user = new User();
				user.setSynergyId(supervisor.getId());
				user.setRoleId(supervisor.getRoleId());
			}

			userRepo.save(user);
		});

	}
	
	public void saveStudentDocument(StudentDocument studentDoc){
		studentDocumentRepo.save(studentDoc);
	}

	public List<MyPlanWeightage> getMyPlanWeightage() {
		return (List<MyPlanWeightage>) MyPlanWeightageRepo.findAll();
	}

	public MyPlanWeightage getMyPlanWeightageBySectionName(String sectionName) {
		return MyPlanWeightageRepo.findBySection(sectionName);
	}

	public List<MyPlanWeightage> getAllMyPlanWeightages() {
		return (List<MyPlanWeightage>) MyPlanWeightageRepo.findAll();
	}

	public List<Semester> getSemesters() {
		return semesterRepo.findAllByOrderBySemesterIdAsc();
	}
	
	public byte[] getDocumentData(Integer studentDocumentId) {
		StudentDocument studentDoc = studentDocumentRepo.findOne(studentDocumentId);
		
		if (studentDoc.getStudentDocumentDataList().size() > 0) {
			StudentDocumentData docData = studentDoc.getStudentDocumentDataList().stream().findFirst().get();
			return docData.getDocumentData();
		}
		else {
			return null;
		}
	}
	
	private StudentContactDetails getStudentContactDetails(Integer studentId) {
		List<StudentContactDetails> studentContactDetails = synergeticDao.getStudentContactDetailsById(studentId);
		if(studentContactDetails !=null && studentContactDetails.size() > 0) {
			return studentContactDetails.get(0);
		}
		return null;
	}
	
	//--------------------------------------------------------------------------------------------------------
	public void generateMySedaPortfolio(HttpServletRequest request, HttpServletResponse response, ServletContext servletContext, Integer studentId) throws IOException {
		try {
			
			StudentProfile profile = getProfileByStudentId(studentId);
			StudentContactDetails studentContactInfo = getStudentContactDetails(studentId);			
			
			/*
			ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();		
			*/	
			
			ServletContextTemplateResolver templateResolver = new ServletContextTemplateResolver(servletContext);
			templateResolver.setPrefix("/WEB-INF/classes/");	
												
			templateResolver.setSuffix(".html");
			templateResolver.setTemplateMode(TemplateMode.HTML);
			templateResolver.setCharacterEncoding("UTF-8");
			 
			TemplateEngine templateEngine = new TemplateEngine();
			templateEngine.setTemplateResolver(templateResolver);			
			
			WebContext context = new WebContext(request, response, servletContext);			
			Date today = Calendar.getInstance().getTime();
			context.setVariable("documentDate", today);
			context.setVariable("firstname", profile.getStudentName());
			context.setVariable("phone", studentContactInfo.getPhone());
			context.setVariable("email", studentContactInfo.getEmail());
			context.setVariable("myBio", profile.getMybio());
			context.setVariable("studentJobs", profile.getStudentJobs());
			//context.setVariable("graph", graph);
			 
			
			String renderedHtmlContent = templateEngine.process("templates/myseda_portfolio/html/template", context);
			String xhtmlDocumentAsString = convertToXhtml(renderedHtmlContent);				
						
			ITextRenderer renderer = new ITextRenderer();			
			
			String extractHeaderImagePath = null;
			String applicationEnvironment = (String) properties.get("application.isTest"); 
			if(applicationEnvironment.equalsIgnoreCase("true")){
				extractHeaderImagePath = (String) properties.get("studentExtract.header.path.test");
			} else {
				extractHeaderImagePath = (String) properties.get("studentExtract.header.path.live");
			}			
			
			ImageReplacedElementFactory imageReplacementFactory = new ImageReplacedElementFactory(renderer.getSharedContext().getReplacedElementFactory());			
			imageReplacementFactory.setLogoAbsolutePath(extractHeaderImagePath);
			renderer.getSharedContext().setReplacedElementFactory(imageReplacementFactory);			
			
			renderer.setDocumentFromString(xhtmlDocumentAsString);
			renderer.layout();
			
			final ByteArrayOutputStream os = new ByteArrayOutputStream();
	        renderer.createPDF(os);
			
			mergePDFDocuments(profile, os.toByteArray(), response);
			
		} catch (Exception ex) {
			response.reset();
			response.getOutputStream().print("There was an error generating user document " + ex.getMessage());
			response.getOutputStream().close();
		} 
		
	}
	
	private String convertToXhtml(String html) throws UnsupportedEncodingException {
		Tidy tidy = new Tidy();
        tidy.setInputEncoding("UTF-8");
        tidy.setOutputEncoding("UTF-8");
        tidy.setXHTML(true);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(html.getBytes("UTF-8"));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        tidy.parseDOM(inputStream, outputStream);
        return outputStream.toString("UTF-8");
   }
	
   private void mergePDFDocuments(StudentProfile profile, byte[] templateContent, HttpServletResponse response) throws IOException, DocumentException {
	   Set<StudentDocument> studentDocuments = profile.getStudentDocuments();
			   
	   com.itextpdf.text.Document document = new com.itextpdf.text.Document();
	   OutputStream outputStream = response.getOutputStream();
	   
	   PdfWriter pdfWriter = PdfWriter.getInstance(document, outputStream);
	   document.open();
	   PdfContentByte pdfContentByte = pdfWriter.getDirectContent();
	   
	   PdfReader pdfReaderPortfolio = new PdfReader(templateContent);
	   for (int j = 1; j <= pdfReaderPortfolio.getNumberOfPages(); j++) {
		   document.newPage();
		   PdfImportedPage page = pdfWriter.getImportedPage(pdfReaderPortfolio, j);
		   pdfContentByte.addTemplate(page, 0, 0);
	   }
	   
	   // Adding charts
	  /*
	  List<String> chartsUrls = chartService.getLineGraphs(profile);
	   
	  for (String chartsUrl: chartsUrls) {
		   Image image = Image.getInstance(chartsUrl);		   
		   image.scaleToFit(500, 500);		   
		   image.setAlignment(Image.MIDDLE);
		   document.newPage();
		   document.add(image);
	   }
	   */
	  
	   for (StudentDocument studentDocument : studentDocuments) {
		   if (studentDocument.getContentType().equals("application/pdf")) {
			   byte[] documentData = getDocumentData(studentDocument.getId());
			   if (documentData != null) {
				   PdfReader pdfReader = new PdfReader(documentData);
				   for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
					   document.newPage();
					   PdfImportedPage page = pdfWriter.getImportedPage(pdfReader, i);
					   pdfContentByte.addTemplate(page, 0, 0);
				   }
			   }
		   }
	   }
	   
	   outputStream.flush();
	   document.close();
	   outputStream.close();
   }
   
   
	public StudentConversation saveConversation(Integer userId, StudentConversation conversation) {
		StudentConversation selectedConversation = null;
		Date currentDate = new Date();
		
		if(conversation != null) {			
			/* Existing conversation */
			if(conversation.getId() != null) {
				selectedConversation = studentConversationRepo.findOne(conversation.getId());
				selectedConversation.setTopic(conversation.getTopic());
				selectedConversation.setTeacherComment(conversation.getTeacherComment());
				selectedConversation.setStudentComment(conversation.getStudentComment());
				selectedConversation.setIsFinal(conversation.getIsFinal());
			} else {
				/* New conversation */
			//	conversation.setStudentProfile(studentProfile); //TODO comment student profile
				conversation.setConversationStartDate(currentDate);	
				conversation.setStudentId(userId);
				selectedConversation = conversation;
			}	
			
		}
				
		return studentConversationRepo.save(selectedConversation);
	}
   
   
}
