package au.com.sedagroup.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.charts4j.AxisLabelsFactory;
import com.googlecode.charts4j.Color;
import com.googlecode.charts4j.Data;
import com.googlecode.charts4j.GCharts;
import com.googlecode.charts4j.Line;
import com.googlecode.charts4j.LineChart;
import com.googlecode.charts4j.LineStyle;
import com.googlecode.charts4j.Plots;
import com.googlecode.charts4j.Shape;

import au.com.sedagroup.dao.SynergeticDAO;
import au.com.sedagroup.dto.TeacherSelectedSkillFocusArea;
import au.com.sedagroup.model.StudentProfile;

@Service
@Transactional
public class ChartsService {

	@Autowired
	private SynergeticDAO synDao;
	
	public List<String> getLineGraphs(StudentProfile profile) {
		
		List<TeacherSelectedSkillFocusArea> teacherSelectedSkillFocusAreas = synDao.getTeacherSelectedSkillFocusAreas(profile.getId());
		
		Set<Integer> skillAreasIds = new HashSet<Integer>();
		for (TeacherSelectedSkillFocusArea area : teacherSelectedSkillFocusAreas) {
			skillAreasIds.add(area.getSkillAreaId());
		}
		
		List<String> chartsUrls = new ArrayList<String>();
		
		
		// -----------------------------------------------------------------------------------------------------
		// At this point we have only semester 1 data, I have added dummy data until 2019.
		// When there is multiple semesters, synDao.getteacherSelectedSkillFocusAreas query may need to change.
		// If so the following logic will have to be changed.
		// -----------------------------------------------------------------------------------------------------
		
		for (Integer id : skillAreasIds) {
			List<TeacherSelectedSkillFocusArea> result = teacherSelectedSkillFocusAreas.stream()
					.filter(area -> area.getSkillAreaId() == id)
	                .collect(Collectors.toList()); 
			
			
			if (result.size() > 1) {
				Line line1 = Plots.newLine(Data.newData(Integer.parseInt(result.get(0).getSkillLevelValue())*100/6, 0, 4*100/6, 1*100/6), Color.CYAN, result.get(0).getFocusAreaSummary());
		        line1.setLineStyle(LineStyle.newLineStyle(2, 1, 0));
		        line1.addShapeMarkers(Shape.CIRCLE, Color.CYAN, 10);
		        
		        Line line2 = Plots.newLine(Data.newData(Integer.parseInt(result.get(0).getSkillLevelValue())*100/6, 5*100/6, 3*100/6, 6*100/6), Color.BLUE, result.get(1).getFocusAreaSummary());
		        line2.setLineStyle(LineStyle.newLineStyle(2, 1, 0));
		        line2.addShapeMarkers(Shape.CIRCLE, Color.BLUE, 10);
		        
		        LineChart chart = GCharts.newLineChart(line1, line2);
		        
		        chart.setSize(720, 360);
		        chart.setTitle( result.get(0).getSkillArea(), Color.WHITE, 14);
		        chart.setGrid(25, 25, 3, 2);
		        
		        chart.addXAxisLabels(AxisLabelsFactory.newAxisLabels("2018 Semester 1", "2018 Semester 2", "2019 Semester 1"));
		        chart.addYAxisLabels(AxisLabelsFactory.newNumericAxisLabels(0, 1, 2, 3, 4, 5, 6));
		        chart.setTitle(result.get(0).getSkillArea());
		        
		        String url = chart.toURLString();
		        chartsUrls.add(url);
			}
		}
		
		return chartsUrls;
	}
}
