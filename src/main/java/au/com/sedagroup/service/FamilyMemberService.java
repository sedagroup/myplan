package au.com.sedagroup.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.sedagroup.model.FamilyMember;
import au.com.sedagroup.repository.FamilyMemberRepository;

@Service
@Transactional
public class FamilyMemberService {
	@Autowired
	private FamilyMemberRepository repository;
	
	public void save(Set<FamilyMember> set){
		repository.save(set);
	}

}
