package au.com.sedagroup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.sedagroup.model.StudentConversation;
import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.repository.StudentConversationRepository;


@Service
@Transactional
public class StudentConversationService {

	@Autowired
	StudentConversationRepository studentConversationRepository;
	
	public List<StudentConversation> getStudentConversations(Integer id) {
			return studentConversationRepository.findByStudentIdOrderByConversationStartDateDesc(id);
		}
	
	//public List<StudentConversation> getStudentConversations(StudentProfile studentProfile) {
	//	return studentConversationRepository.findByStudentProfileOrderByConversationStartDateDesc(studentProfile);
	//}
}
