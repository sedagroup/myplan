package au.com.sedagroup.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import au.com.sedagroup.model.MyPlanWeightage;
import au.com.sedagroup.repository.MyPlanWeightageRepository;

@Service
public class MyPlanWeightageService {
	@Autowired
	private MyPlanWeightageRepository myPlanWeightageRepository;
	
	public MyPlanWeightage save(MyPlanWeightage myPlanWeightage){
		return myPlanWeightageRepository.save(myPlanWeightage);
	}
	
	public MyPlanWeightage getDocumentById(Integer id){
		return myPlanWeightageRepository.findOne(id);
	}
	
	public List<MyPlanWeightage> getAllDocuments(){
		return  (List<MyPlanWeightage>) myPlanWeightageRepository.findAll();
	}
}
