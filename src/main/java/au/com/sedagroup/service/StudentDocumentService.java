package au.com.sedagroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.sedagroup.model.StudentDocument;
import au.com.sedagroup.repository.StudentDocumentRepository;

@Service
@Transactional
public class StudentDocumentService {
	
	@Autowired
	private StudentDocumentRepository studentDocumentRepository;
	
	public StudentDocument save(StudentDocument studentDocument){
		return studentDocumentRepository.save(studentDocument);
		
	}
}
