package au.com.sedagroup.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.com.sedagroup.model.StudentJob;
import au.com.sedagroup.repository.StudentJobRepository;

@Service
@Transactional
public class StudentJobService {
	
	@Autowired
	private StudentJobRepository studentJobRepository;
	
	public void save(StudentJob job){
		studentJobRepository.save(job);
		
	}

}
