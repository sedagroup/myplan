package au.com.sedagroup.interceptor;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class MySedaLoginInterceptor extends HandlerInterceptorAdapter {
	
//	Teacher:
//	http://localhost:8080/MyPlan/?id=7579&key=b19dc9fbe9b2ec0c747eba63bb49ed6618b66f70&time=1323305767&user=john
//  Sarah Makin	
//	http://localhost:8080/MyPlan/?id=7542&key=6d1f8c8e2af0a4811c10fba0fe69579661586afe&time=1323305767&user=john
	
//
//	Parent:
//	http://localhost:8080/MyPlan/?id=7422&key=2ca47ea2405b00ad4e86f36119bbad6736327895&time=1323305767&user=john
//  	
	
//
//	Student:
//	http://localhost:8080/MyPlan/?id=41602&key=3b84a32578ee1a4d931229d05a51ecfe993e9685&time=1323305767&user=john
	
//  Koops-Corpus Koops - Student
//  http://localhost:8080/MyPlan/?id=7078&key=e7585d32d45b5e971605fe6d37af27701015764c&time=1323305767&user=john
//  Brock Dean - Student
//  http://localhost:8080/MyPlan/?id=43702&key=a96531f7961b90885edd8538986e666f80d6795f&time=1525149840&user=brock.dean		
//  Harmony Plues
//  http://localhost:8080/MyPlan/?id=43779&key=1a4d2536ad8eedc199b230a2c4dfd4846b1f5a27&time=1525152384&user=harmony.plues	

	
//	Admin/Supervisor:
//	http://localhost:8080/MyPlan/?id=7576&key=ee78c103a5604efa58824cfe1c122b8e5997b6da&time=1323305767&user=john
//  MatE
//	http://localhost:8080/MyPlan/?id=6672&key=540db428df3a0370dd9a0ed934b201d03e435842&time=1323305767&user=john

//	Supervisor:
//  http://localhost:8080/MyPlan/?id=7611&key=73ebcfcfa64262f3dd684c54e3127b028c5a9b00&time=1323305767&user=john

	
	
	private static final Logger logger = LoggerFactory.getLogger(MySedaLoginInterceptor.class);
	/**
	 * This session attribute is populated in mySedaLogin() with the student id after a successful validation 
	 * of a request from MySeda.
	 */
	public static final String MY_SEDA_SESSION_ATTRIBUTE = "MySedaStudentId";
	
	//--------------------------------
	// Injected from spring config xml
	//--------------------------------
	private String secret;
	private long timeDifferenceToleranceMillis = 0L;
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try{
        	if(!mySedaLogin(request)) {
				HttpServletResponse resp = (HttpServletResponse) response;
				resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
				resp.setContentLength(0);
			}
        } catch (Exception e) {
        	logger.error("Error in MySeda interceptor", e);
        }
        return super.preHandle(request, response, handler);
    }
    
    /**
	 * Checks authenticity of a remote service request from MySeda.
	 * MySeda (Schoolbox) user initiates the request by clicking on a tile in MySeda web application.
	 * A remote service that is configured to handle the request decodes base-64 encoded Scar URL and
	 * launches the url in a separate window passing studentId, timestamp and key parameters.
	 * This method calculated the SHA-1 hash of the timestamp, studentId and secret values against the
	 * key value. If the two match, the request is considered authentic.
	 * 
	 * @param request - request from MySeda
	 * @return true if the request validation passes.
	 */
	private boolean mySedaLogin( HttpServletRequest request ) throws NoSuchAlgorithmException {
		String studentId = request.getParameter("id");
		String timestamp = request.getParameter("time");
		String key = request.getParameter("key");
		
		//!!!!!!!!!!!!!!!!!!!!!!!
		boolean keyMatch = false;
		boolean timeMatch = false;
		//!!!!!!!!!!!!!!!!!!!!!!!
		if(studentId!=null && !studentId.isEmpty() && timestamp!=null && !timestamp.isEmpty() && key!=null && !key.isEmpty()){
			String inputStr = secret + timestamp + studentId;			
			String strHashCode = shaEncode( inputStr );
						
			keyMatch = StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty( inputStr) && strHashCode.equals( key );
			timeMatch = (Math.abs(new Date().getTime() - Long.valueOf( timestamp+"000" )) < timeDifferenceToleranceMillis || timeDifferenceToleranceMillis == -1);
			logger.info( "MySeda remote service access from studentId = " + studentId + ", timestamp = " + timestamp + ", key = " + key + ", shaHash = " + strHashCode+" currentTime: "+new Date().getTime());
			logger.info( "keyMatch: "+keyMatch+" timeMatch: "+timeMatch);
			
			if( keyMatch ) {
				request.getSession().setAttribute(MY_SEDA_SESSION_ATTRIBUTE, studentId);
			}
		}
		return keyMatch;
	} // end of mySedaLogin()
	

	/**
	 * @param inputStr
	 * @return
	 * @throws NoSuchAlgorithmException
	 */
	public String shaEncode(String inputStr ) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(inputStr.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) {
            sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
	}
	
	
	/**
	 * @param secret
	 */
	public void setSecret( String secret ) {
		this.secret = secret;
	}
	
	/**
	 * @param value
	 */
	public void setTimeDifferenceToleranceMillis( long value ) {
		timeDifferenceToleranceMillis = value;
	}
}
