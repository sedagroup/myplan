package au.com.sedagroup.dto;

import java.util.Set;

import au.com.sedagroup.model.StudentJob;

public class ExperiencesDTO {

	private Integer studentId;
	private Set<StudentJob> jobExperiences;
		
	public ExperiencesDTO(){		
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public Set<StudentJob> getJobExperiences() {
		return jobExperiences;
	}
	
	public void setJobExperiences(Set<StudentJob> jobExperiences) {
		this.jobExperiences = jobExperiences;
	}
	
}
