package au.com.sedagroup.dto;

import java.io.Serializable;
import java.util.Set;

import au.com.sedagroup.model.StudentCareerGoal;
import au.com.sedagroup.model.StudentIndustryGoal;
import au.com.sedagroup.model.StudentJob;
import au.com.sedagroup.model.StudentJobReflection;

public class PlacementsDTO implements Serializable {	

	private static final long serialVersionUID = 3795212202726087530L;
	private Integer studentId;
	private Set<StudentJob> studentWorkPlacements;
	private Set<StudentJobReflection> studentJobReflections;

	public PlacementsDTO() {
	}	
	
	public Integer getStudentId() {
		return studentId;
	}
	
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	public Set<StudentJob> getStudentWorkPlacements() {
		return studentWorkPlacements;
	}
	
	public void setStudentWorkPlacements(Set<StudentJob> studentWorkPlacements) {
		this.studentWorkPlacements = studentWorkPlacements;
	}
	
	public Set<StudentJobReflection> getStudentJobReflections() {
		return studentJobReflections;
	}
	
	public void setStudentJobReflections(Set<StudentJobReflection> studentJobReflections) {
		this.studentJobReflections = studentJobReflections;
	}
	
}
	