package au.com.sedagroup.dto;

public class StudentContactDetails{
	String phone;
	String email;
	
	public StudentContactDetails() {
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}