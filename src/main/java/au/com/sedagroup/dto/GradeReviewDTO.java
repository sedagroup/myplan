package au.com.sedagroup.dto;

import java.io.Serializable;
import java.util.List;

import au.com.sedagroup.model.StudentCareerGoal;
import au.com.sedagroup.model.StudentGrade;
import au.com.sedagroup.model.StudentGradeComment;
import au.com.sedagroup.model.StudentIndustryGoal;
import au.com.sedagroup.model.StudentJob;
import au.com.sedagroup.model.StudentJobReflection;
import au.com.sedagroup.model.StudentSectionCompletion;

public class GradeReviewDTO implements Serializable {
	
	private static final long serialVersionUID = 6770415489070129730L;
	
	private Integer studentId;
	private List<StudentCareerGoal> studentCareerGoals;
	private List<StudentIndustryGoal> studentIndustryGoals;
	private List<StudentJob> studentJobs;
	private List<StudentJobReflection> studentJobReflections;
	private List<StudentGrade> studentGrades;
	private List<StudentGradeComment> studentGradeComments;
	private List<StudentSectionCompletion> studentSectionCompletions;

	public GradeReviewDTO() {
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public List<StudentCareerGoal> getStudentCareerGoals() {
		return studentCareerGoals;
	}

	public void setStudentCareerGoals(List<StudentCareerGoal> studentCareerGoals) {
		this.studentCareerGoals = studentCareerGoals;
	}

	public List<StudentIndustryGoal> getStudentIndustryGoals() {
		return studentIndustryGoals;
	}

	public void setStudentIndustryGoals(List<StudentIndustryGoal> studentIndustryGoals) {
		this.studentIndustryGoals = studentIndustryGoals;
	}

	public List<StudentJob> getStudentJobs() {
		return studentJobs;
	}

	public void setStudentJobs(List<StudentJob> studentJobs) {
		this.studentJobs = studentJobs;
	}

	public List<StudentJobReflection> getStudentJobReflections() {
		return studentJobReflections;
	}

	public void setStudentJobReflections(List<StudentJobReflection> studentJobReflections) {
		this.studentJobReflections = studentJobReflections;
	}

	public List<StudentGrade> getStudentGrades() {
		return studentGrades;
	}
	
	public void setStudentGrades(List<StudentGrade> studentGrades) {
		this.studentGrades = studentGrades;
	}
	
	public List<StudentGradeComment> getStudentGradeComments() {
		return studentGradeComments;
	}
	
	public void setStudentGradeComments(List<StudentGradeComment> studentGradeComments) {
		this.studentGradeComments = studentGradeComments;
	}

	public List<StudentSectionCompletion> getStudentSectionCompletions() {
		return studentSectionCompletions;
	}

	public void setStudentSectionCompletions(List<StudentSectionCompletion> studentSectionCompletions) {
		this.studentSectionCompletions = studentSectionCompletions;
	}
	
	
}