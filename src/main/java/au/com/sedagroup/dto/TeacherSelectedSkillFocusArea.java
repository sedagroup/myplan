package au.com.sedagroup.dto;

public class TeacherSelectedSkillFocusArea {
	private Integer skillAreaId;
	private String skillArea;
	private String focusAreaSummary;
	private Integer semesterId;
	private String skillLevel;
	private String skillLevelValue;
	private Integer studentId;
	private boolean isTeacherGrade;

	public Integer getSkillAreaId() {
		return skillAreaId;
	}

	public void setSkillAreaId(Integer skillAreaId) {
		this.skillAreaId = skillAreaId;
	}

	public String getSkillArea() {
		return skillArea;
	}

	public void setSkillArea(String skillArea) {
		this.skillArea = skillArea;
	}

	public Integer getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(Integer semesterId) {
		this.semesterId = semesterId;
	}

	public String getFocusAreaSummary() {
		return focusAreaSummary;
	}

	public void setFocusAreaSummary(String focusAreaSummary) {
		this.focusAreaSummary = focusAreaSummary;
	}

	public String getSkillLevel() {
		return skillLevel;
	}

	public void setSkillLevel(String skillLevel) {
		this.skillLevel = skillLevel;
	}

	public String getSkillLevelValue() {
		return skillLevelValue;
	}

	public void setSkillLevelValue(String skillLevelValue) {
		this.skillLevelValue = skillLevelValue;
	}
	
	public Integer getStudentId() {
		return studentId;
	}
	
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	public boolean isTeacherGrade() {
		return isTeacherGrade;
	}

	public void setTeacherGrade(boolean isTeacherGrade) {
		this.isTeacherGrade = isTeacherGrade;
	}
	
	
}
