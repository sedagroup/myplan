package au.com.sedagroup.dto;

import java.io.Serializable;
import java.util.Set;

import au.com.sedagroup.model.FamilyMember;
import au.com.sedagroup.model.StudentCareerGoal;
import au.com.sedagroup.model.StudentIndustryGoal;
import au.com.sedagroup.model.StudentNetwork;

public class StudentProfileDTO implements Serializable {	

	private static final long serialVersionUID = -1073190044513182515L;
	private Integer studentId;
	private Integer teacherId;
	private String studentName;
	private String mybio;
	
	private Set<FamilyMember> familyMembers;
	private Set<StudentNetwork> studentNetworks;

	public StudentProfileDTO() {
	}	
		
	public Integer getStudentId() {
		return studentId;
	}
	
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	public Integer getTeacherId() {
		return teacherId;
	}
	
	public void setTeacherId(Integer teacherId) {
		this.teacherId = teacherId;
	}
	
	public String getStudentName() {
		return studentName;
	}
	
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	
	public String getMybio() {
		return mybio;
	}
	
	public void setMybio(String mybio) {
		this.mybio = mybio;
	}

	public Set<FamilyMember> getFamilyMembers() {
		return familyMembers;
	}

	public void setFamilyMembers(Set<FamilyMember> familyMembers) {
		this.familyMembers = familyMembers;
	}

	public Set<StudentNetwork> getStudentNetworks() {
		return studentNetworks;
	}

	public void setStudentNetworks(Set<StudentNetwork> studentNetworks) {
		this.studentNetworks = studentNetworks;
	}
	
}
	