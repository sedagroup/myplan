package au.com.sedagroup.dto;

import java.io.Serializable;
import java.util.Set;

import au.com.sedagroup.model.StudentCareerGoal;
import au.com.sedagroup.model.StudentIndustryGoal;

public class GoalsDTO implements Serializable {	

	private static final long serialVersionUID = 1481530318377438858L;
	private Integer studentId;
	private Set<StudentCareerGoal> studentCareerGoals;
	private Set<StudentIndustryGoal> studentIndustryGoals;

	public GoalsDTO() {
	}	
	
	public Integer getStudentId() {
		return studentId;
	}
	
	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	public Set<StudentCareerGoal> getStudentCareerGoals() {
		return studentCareerGoals;
	}
	
	public void setStudentCareerGoals(Set<StudentCareerGoal> studentCareerGoals) {
		this.studentCareerGoals = studentCareerGoals;
	}
	
	public Set<StudentIndustryGoal> getStudentIndustryGoals() {
		return studentIndustryGoals;
	}
	
	public void setStudentIndustryGoals(Set<StudentIndustryGoal> studentIndustryGoals) {
		this.studentIndustryGoals = studentIndustryGoals;
	}
	
}
	