package au.com.sedagroup.dto;

import java.io.Serializable;
import java.util.List;

import au.com.sedagroup.model.StudentCareerGoal;
import au.com.sedagroup.model.StudentHabitGrade;
import au.com.sedagroup.model.StudentHabitGradeComment;
import au.com.sedagroup.model.StudentIndustryGoal;
import au.com.sedagroup.model.StudentJob;
import au.com.sedagroup.model.StudentJobReflection;
import au.com.sedagroup.model.StudentSectionCompletion;
import au.com.sedagroup.model.StudentSkillFocusArea;
import au.com.sedagroup.model.StudentSkillFocusLevel;

public class ReviewDTO implements Serializable {
	
	private static final long serialVersionUID = 3469713734700941201L;
	private List<StudentCareerGoal> studentCareerGoals;
	private List<StudentIndustryGoal> studentIndustryGoals;
	private List<StudentJob> studentJobs;
	private List<StudentJobReflection> studentJobReflections;
	private List<StudentHabitGrade> habitsGrades;
	private List<StudentHabitGradeComment> habitGradeComments;
	private List<StudentSkillFocusArea> focusAreas;
	private List<StudentSkillFocusLevel> focusLevels;
	private List<StudentSectionCompletion> studentSectionCompletions;

	private Integer studentId;

	public ReviewDTO() {
	}
	
	public List<StudentHabitGrade> getHabitsGrades() {
		return habitsGrades;
	}

	public void setHabitsGrades(List<StudentHabitGrade> habitsGrades) {
		this.habitsGrades = habitsGrades;
	}
	
	public List<StudentHabitGradeComment> getHabitGradeComments() {
		return habitGradeComments;
	}
	
	public void setHabitGradeComments(List<StudentHabitGradeComment> habitGradeComments) {
		this.habitGradeComments = habitGradeComments;
	}

	public List<StudentSkillFocusArea> getFocusAreas() {
		return focusAreas;
	}
	
	public void setFocusAreas(List<StudentSkillFocusArea> focusAreas) {
		this.focusAreas = focusAreas;
	}

	public List<StudentSkillFocusLevel> getFocusLevels() {
		return focusLevels;
	}

	public void setFocusLevels(List<StudentSkillFocusLevel> focusLevels) {
		this.focusLevels = focusLevels;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}
	
	public List<StudentSectionCompletion> getStudentSectionCompletions() {
		return studentSectionCompletions;
	}

	public void setStudentSectionCompletions(List<StudentSectionCompletion> studentSectionCompletions) {
		this.studentSectionCompletions = studentSectionCompletions;
	}
	
	public List<StudentJobReflection> getStudentJobReflections() {
		return studentJobReflections;
	}

	public void setStudentJobReflections(List<StudentJobReflection> studentJobReflections) {
		this.studentJobReflections = studentJobReflections;
	}

	public List<StudentJob> getStudentJobs() {
		return studentJobs;
	}

	public void setStudentJobs(List<StudentJob> studentJobs) {
		this.studentJobs = studentJobs;
	}
	
	public List<StudentCareerGoal> getStudentCareerGoals() {
		return studentCareerGoals;
	}
	
	public void setStudentCareerGoals(List<StudentCareerGoal> studentCareerGoals) {
		this.studentCareerGoals = studentCareerGoals;
	}
	
	public List<StudentIndustryGoal> getStudentIndustryGoals() {
		return studentIndustryGoals;
	}
	
	public void setStudentIndustryGoals(List<StudentIndustryGoal> studentIndustryGoals) {
		this.studentIndustryGoals = studentIndustryGoals;
	}
	
}