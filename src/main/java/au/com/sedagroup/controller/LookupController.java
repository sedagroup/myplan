package au.com.sedagroup.controller;

import java.util.List;

import javax.servlet.ServletContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import au.com.sedagroup.dto.GenericResult;
import au.com.sedagroup.model.OrganizationType;
import au.com.sedagroup.service.LookupService;

@Controller
public class LookupController {

	@Autowired
	ServletContext servletContext;
	
	@Autowired
	private LookupService lookupService;

	private Logger logger = LoggerFactory.getLogger(LookupController.class);
	
	public LookupController(){		
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getOrganizationTypes")
	public @ResponseBody GenericResult<List<OrganizationType>> getOrganizationTypes() {
		GenericResult<List<OrganizationType>> result = new GenericResult<List<OrganizationType>>();
		try {						
			result.setData(lookupService.getOrganizationTypes());
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage("There was an error getting organization types. Please contact ICT");
    		logger.error("There was an error getting organization types", e);
    	}
		return result;
	}
	
}
