package au.com.sedagroup.controller;

import java.util.List;
import javax.servlet.ServletContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.ServletContextAware;
import au.com.sedagroup.dto.GenericResult;
import au.com.sedagroup.dto.GradeReviewDTO;
import au.com.sedagroup.model.Area;
import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.service.ReviewService;

@Controller
@SessionAttributes("MySedaStudentId")
public class ReviewController implements ServletContextAware {

	@Autowired
	private ReviewService reviewService;
	@Autowired
	ServletContext servletContext;
	
	private Logger logger = LoggerFactory.getLogger(ReviewController.class);

	public ReviewController() { 
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;		
	}
	
	public ServletContext getServletContext(){
		return this.servletContext;
	}
				
	@RequestMapping(method = RequestMethod.GET, value="/api/latest/getProfileByStudentId")
	public @ResponseBody GenericResult<StudentProfile> getProfileByStudentId(@ModelAttribute("MySedaStudentId") Integer userId) {
		GenericResult<StudentProfile> result = new GenericResult<StudentProfile>();
		try {						
			result.setData(reviewService.getProfileByStudentId(userId));
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage("There was an error getting the profile. Please contact ICT");
    		logger.error("There was an error getting the profile", e);
    	}

		return result;
	}
							
	/**
	 * get all areas
	 * @return List<Area>
	 */
	@RequestMapping(method = RequestMethod.GET, value="/api/latest/getAreas")
	public @ResponseBody GenericResult<List<Area>> getAreas() {
		GenericResult<List<Area>> result = new GenericResult<List<Area>>();
		try {						
			result.setData(reviewService.getAreas());
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage("There was an error getting habits/skills. Please contact ICT");
    		logger.error("There was an error getting habits/skills", e);
    	}
		return result;
	}
	
	/**
	 * Save student markings, comments and approvals
	 * @param userId
	 * @param gradeReviewDto
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value="/api/latest/saveTeacherReview")
	public @ResponseBody GenericResult<String> saveTeacherReview(@ModelAttribute("MySedaStudentId") Integer userId, @RequestBody GradeReviewDTO gradeReviewDto) {
		GenericResult<String> result = new GenericResult<String>();
		try {
			reviewService.saveTeacherReview(userId, gradeReviewDto);
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving the teacher review", e);
    	}

		return result;
	}
	
	
	/**
	 * Save student marking and comments for Habits/Skills
	 * @param userId
	 * @param gradeReviewDto
	 * @return
	 */
	@RequestMapping(method = RequestMethod.POST, value="/api/latest/saveStudentReview")
	public @ResponseBody GenericResult<String> saveStudentReview(@ModelAttribute("MySedaStudentId") Integer userId, @RequestBody GradeReviewDTO gradeReviewDto) {
		GenericResult<String> result = new GenericResult<String>();
		try {
			reviewService.saveStudentReview(userId, gradeReviewDto);
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving the student review", e);
    	}

		return result;
	}
	
	
}
