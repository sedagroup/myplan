package au.com.sedagroup.controller;

import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Calendar;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.MultipartFile;

import au.com.sedagroup.dao.SynergeticDAO;
import au.com.sedagroup.dto.ExperiencesDTO;
import au.com.sedagroup.dto.GenericResult;
import au.com.sedagroup.dto.GoalsDTO;
import au.com.sedagroup.dto.PlacementsDTO;
import au.com.sedagroup.dto.ReviewDTO;
import au.com.sedagroup.dto.Student;
import au.com.sedagroup.dto.StudentProfileDTO;
import au.com.sedagroup.exception.StudentProfileNotFoundException;
import au.com.sedagroup.model.Document;
import au.com.sedagroup.model.Grade;
import au.com.sedagroup.model.Habit;
import au.com.sedagroup.model.MyPlanWeightage;
import au.com.sedagroup.model.Semester;
import au.com.sedagroup.model.SkillArea;
import au.com.sedagroup.model.StudentConversation;
import au.com.sedagroup.model.StudentDocument;
import au.com.sedagroup.model.StudentDocumentData;
import au.com.sedagroup.model.StudentProfile;
import au.com.sedagroup.model.Supervisor;
import au.com.sedagroup.model.Teacher;
import au.com.sedagroup.service.ApplicationService;
import au.com.sedagroup.service.StudentConversationService;

@Controller
@SessionAttributes("MySedaStudentId")
public class ApplicationController implements ServletContextAware {

	@Autowired
	private ApplicationService applicationService;
	@Autowired
	private SynergeticDAO synDao;
	@Autowired
	private StudentConversationService studentConversationService;
	@Autowired
	ServletContext servletContext;
	
	private Logger logger = LoggerFactory.getLogger(ApplicationController.class);

	public ApplicationController() { 
	}
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;		
	}
	
	public ServletContext getServletContext(){
		return this.servletContext;
	}
			
	@RequestMapping(method = RequestMethod.POST, value="/api/saveStudentProfile")
	public @ResponseBody GenericResult<StudentProfile> saveStudentProfile(@ModelAttribute("MySedaStudentId") Integer userId, @RequestBody StudentProfileDTO studentProfileDTO) {
		GenericResult<StudentProfile> result = new GenericResult<StudentProfile>();
		try {
			StudentProfile studentProfile = applicationService.saveStudentProfile(userId, studentProfileDTO);	
			result.setData(studentProfile);
		} catch(StudentProfileNotFoundException profileNotFoundException) {
			result.setError(true);
			result.setErrorMessage("Student profile not found. Please contact ICT");
			logger.error("Student profile not found", profileNotFoundException.getMessage());
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage("There was an error saving the student profile. Please contact ICT");
    		logger.error("There was an error saving the student profile", e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getProfileByStudentId")
	public @ResponseBody GenericResult<StudentProfile> getProfileByStudentId(@ModelAttribute("MySedaStudentId") Integer userId) {
		GenericResult<StudentProfile> result = new GenericResult<StudentProfile>();
		try{						
			result.setData(applicationService.getProfileByStudentId(userId));
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting the profile. Please contact ICT");
    		logger.error("There was an error getting the profile", e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/api/saveMyGoals")
	public @ResponseBody GenericResult<String> saveMyGoals(@RequestBody GoalsDTO goalsDTO) {
		GenericResult<String> result = new GenericResult<String>();
		try {
			applicationService.saveMyGoals(goalsDTO);
		} catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving goals", e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/api/saveMyPlacements")
	public @ResponseBody GenericResult<String> saveMyPlacements(@RequestBody PlacementsDTO placementsDTO) {
		GenericResult<String> result = new GenericResult<String>();
		try {
			applicationService.saveMyPlacements(placementsDTO);
		} catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving work placements", e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/api/saveMyExperiences")
	public @ResponseBody GenericResult<String> saveMyExperiences(@RequestBody ExperiencesDTO experiencesDTO) {
		GenericResult<String> result = new GenericResult<String>();
		try {
			applicationService.saveMyExperiences(experiencesDTO);
		} catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving industry experiences", e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getDocuments")
	public @ResponseBody GenericResult<List<Document>> getDocuments() {
		GenericResult<List<Document>> result = new GenericResult<List<Document>>();
		try{						
			result.setData(applicationService.getDocuments());
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting documents. Please contact ICT");
    		logger.error("There was an error getting documents",e);
    	}
		return result;
	}	
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getUserType")
	public @ResponseBody GenericResult<String> isStudent(@ModelAttribute("MySedaStudentId") Integer userId) {
		GenericResult<String> result = new GenericResult<String>();
		try {						
			result.setData(synDao.getUsertype(userId));
		} catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting the user type. Please contact ICT");
    		logger.error("There was an error getting the user type", e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/teacherStudentList")
	public @ResponseBody GenericResult<List<Student>> getTeacherStudentList(@ModelAttribute("MySedaStudentId") Integer userId,
																				@RequestParam(value = "teacherId", required= false) String teacherId) { // This is used when supervisor search -> select teacher
		if(!(teacherId.equalsIgnoreCase("null")) ) {
			userId = Integer.parseInt(teacherId);
		}
		
		GenericResult<List<Student>> result = getAllStudentsOfTeacherById(userId);
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/selectedteacherStudentList/", params= {"teacherId"} )
	public @ResponseBody GenericResult<List<Student>> getSelectedTeacherStudentList(@RequestParam(value = "teacherId") Integer teacherId) {
		GenericResult<List<Student>> result = getAllStudentsOfTeacherById(teacherId);
		return result;
	}
	
	private GenericResult<List<Student>> getAllStudentsOfTeacherById(Integer staffId) {
		GenericResult<List<Student>> result = new GenericResult<List<Student>>();
		result.setData(new ArrayList<Student>());
		
		try {	
			List<String> teacherClass = synDao.getTeacherClass(staffId);
						
			if(teacherClass != null && !teacherClass.isEmpty()) {
				for(String classCode : teacherClass){					
					result.getData().addAll(applicationService.getStudentsProfile(synDao.getStudentsByClass(classCode)));
				}
			} else {
				result.setError(true);
	    		result.setErrorMessage("There are no classes assigned to you. Please contact ICT");
			}
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage("There was an error getting the students. Please contact ICT");
    		logger.error("There was an error getting the students for teacher "+ staffId, e);
    	}
		
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/parentStudentList")
	public @ResponseBody GenericResult<List<Student>> getParentStudentList(@ModelAttribute("MySedaStudentId") Integer userId) {
		GenericResult<List<Student>> result = new GenericResult<List<Student>>();
		try {						
			List<Student> students = synDao.getStudentsByParentId(userId);
			if(students.isEmpty()) {
				result.setError(true);
	    		result.setErrorMessage("There is no students assigned to you. Please contact ICT");
			}
			result.setData(applicationService.getStudentsProfile(students));
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage("There was an error getting the students. Please contact ICT");
    		logger.error("There was an error getting the students for teacher "+ userId, e);
    	}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getAllGrades")
	public @ResponseBody GenericResult<List<Grade>> getGrades() {
		GenericResult<List<Grade>> result = new GenericResult<List<Grade>>();
		try{						
			result.setData(applicationService.getAllGrades());
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting grades. Please contact ICT");
    		logger.error("There was an error getting grades",e);
    	}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getAllHabits")
	public @ResponseBody GenericResult<List<Habit>> getHabits() {
		GenericResult<List<Habit>> result = new GenericResult<List<Habit>>();
		try{						
			result.setData(applicationService.getAllHabits());
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting habits. Please contact ICT");
    		logger.error("There was an error getting habits",e);
    	}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getSkillAreas")
	public @ResponseBody GenericResult<List<SkillArea>> getSkillAreas() {
		GenericResult<List<SkillArea>> result = new GenericResult<List<SkillArea>>();
		try{						
			result.setData(applicationService.getSkillAreas());
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting skill areas. Please contact ICT");
    		logger.error("There was an error getting skill areas",e);
    	}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/api/saveTeacherReview")
	public @ResponseBody GenericResult<String> saveTeacherReview(@ModelAttribute("MySedaStudentId") Integer userId, @RequestBody ReviewDTO reviewDto) {
		GenericResult<String> result = new GenericResult<String>();
		try {
			applicationService.saveTeacherReview(userId, reviewDto);
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving the teacher review", e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/api/saveStudentReview")
	public @ResponseBody GenericResult<String> saveStudentReview(@ModelAttribute("MySedaStudentId") Integer userId, @RequestBody ReviewDTO reviewDto) {
		GenericResult<String> result = new GenericResult<String>();
		try {
			applicationService.saveStudentReview(userId, reviewDto);
		} catch(Exception e) {
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving the student review", e);
    	}

		return result;
	}
	
	
	@RequestMapping(method= RequestMethod.GET, value="api/getTeacherList")
	public @ResponseBody GenericResult<List<Teacher>> getTeacherList(@ModelAttribute("MySedaStudentId") Integer userId){
		GenericResult<List<Teacher>> result = new GenericResult<List<Teacher>>();
		try {
			List<Teacher> teachers = synDao.getAllTeachers();
			result.setData(teachers);	
		} catch (Exception e) {
			result.setError(true);
    		result.setErrorMessage("There was an error getting teachers. Please contact ICT");
    		logger.error("There was an error getting the teachers for user "+ userId, e);
		}
		
		return result;
	}
	
	public @ResponseBody GenericResult<Boolean> isValidTermDate(String studentId){
		GenericResult<Boolean> result = new GenericResult<Boolean>();
		Boolean validTermDate = false;
		
		try {
			validTermDate = synDao.isValidTermDate(Integer.parseInt(studentId));
			result.setData(validTermDate);
		} catch (Exception e) {
			result.setError(true);
			result.setErrorMessage("There was an error while checking for valid term date, Please contact ICT");
			logger.error("There was an error while checking for valid term date for student " + studentId, e);
		}
		
		return result;
	}
	
	@RequestMapping(method= RequestMethod.GET, value="api/getSupervisorList")
	public @ResponseBody GenericResult<List<Supervisor>> getSupervisorList(@ModelAttribute("MySedaStudentId") Integer userId){
		GenericResult<List<Supervisor>> result = new GenericResult<List<Supervisor>>();
		try {
			List<Supervisor> supervisors = synDao.getSupervisors();
			result.setData(supervisors);	
		} catch (Exception e) {
			result.setError(true);
    		result.setErrorMessage("There was an error getting supervisors. Please contact ICT");
    		logger.error("There was an error getting the supervisors for user "+ userId, e);
		}
		
		return result;
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/api/saveSupervisors")
	public @ResponseBody GenericResult<List<Supervisor>> saveSupervisors(@ModelAttribute("MySedaStudentId") Integer userId, @RequestBody List<Supervisor> supervisors) {
		GenericResult<List<Supervisor>> result = new GenericResult<List<Supervisor>>();
		try {
			applicationService.saveSupervisors(userId, supervisors);
			result = this.getSupervisorList(userId);
		} catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage(e.getMessage());
    		logger.error("There was an error saving supervisor.",e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getMyPlanWeightageBySectionName")
	public @ResponseBody GenericResult<MyPlanWeightage> getMyPlanWeightageBySectionName(@ModelAttribute("MySedaStudentId") Integer userId, @RequestParam(value = "sectionName") String sectionName) {
		GenericResult<MyPlanWeightage> result = new GenericResult<MyPlanWeightage>();
		try{						
			result.setData(applicationService.getMyPlanWeightageBySectionName(sectionName));
			
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting the WeightageMaster. Please contact ICT");
    		logger.error("There was an error getting the WeightageMaster",e);
    	}

		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getAllMyPlanWeightages")
	public @ResponseBody GenericResult<List<MyPlanWeightage>> getAllMyPlanWeightages() {
		GenericResult<List<MyPlanWeightage>> result = new GenericResult<List<MyPlanWeightage>>();
		try{						
			result.setData(applicationService.getAllMyPlanWeightages());
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting my plan weightages. Please contact ICT");
    		logger.error("There was an error getting weightages",e);
    	}
		return result;
	}
		
	// PDF request goes here - TODO
	@RequestMapping(method = RequestMethod.GET, value="/api/getPDFData")
	public @ResponseBody GenericResult<List<MyPlanWeightage>> getPDFData() {
		GenericResult<List<MyPlanWeightage>> result = new GenericResult<List<MyPlanWeightage>>();
		try{						
			result.getData();
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting my plan weightages. Please contact ICT");
    		logger.error("There was an error getting weightages",e);
    	}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getDocumentData")
	public @ResponseBody GenericResult<byte[]> getDocumentData(@RequestParam Integer studentDocumentId) {
		GenericResult<byte[]> result = new GenericResult<byte[]>();
		try{						
			result.setData(applicationService.getDocumentData(studentDocumentId));
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error loading the document. Please contact ICT");
    		logger.error("There was an error loading the document",e);
    	}
		return result;
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/api/deleteDocument")
	public @ResponseBody GenericResult<String> deleteDocument(@RequestParam Integer documentId) {
		GenericResult<String> result = new GenericResult<String>();
		try{			
			result.setData(applicationService.deleteDocument(documentId));
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error deleting document. Please contact ICT");
    		logger.error("There was an error deleting document id: "+documentId,e);
    	}
		return result;
	}

	
	// Separated document upload part from the profile, Profile only load documents when loading.
	@RequestMapping(method = RequestMethod.POST, value = "/api/upload")
	public @ResponseBody GenericResult<StudentProfile> uploadMultipleFile(@ModelAttribute("MySedaStudentId") Integer userId, @RequestParam("uploadedFiles") MultipartFile[] files, @RequestParam("documentType") String documentType) {
		GenericResult<StudentProfile> result = new GenericResult<StudentProfile>();
		try{
			StudentProfile profile = applicationService.getProfileByStudentId(userId);
			Document master = applicationService.getDocumentByType(documentType);
			for (int i = 0; i < files.length; i++) {
				MultipartFile file = files[i];
				StudentDocument doc = new StudentDocument();
				
				StudentDocumentData docData = new StudentDocumentData();
				docData.setDocumentData(file.getBytes());
				docData.setStudentDocument(doc);
				
				doc.setDocument(master);
				doc.setContentType(file.getContentType());
				doc.setName(file.getOriginalFilename());
				
				doc.getStudentDocumentDataList().add(docData);
				
				doc.setStudentProfile(profile); // Set parents
				/*
				doc.setUpdatedByUser("" + userId + "");
				doc.setUpdatedDate(Calendar.getInstance().getTime());
				*/
				applicationService.saveStudentDocument(doc);
			}
			
			// Here we should only send documents without data to the front end(With Lazy Loading) 
			result.setData(applicationService.getProfileByStudentId(userId));
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error uploading document. Please contact ICT");
    		logger.error("There was an error uploading document userId: "+userId,e);
    	}
		return result;
	
	}
	
	// update portfolio with myPortfolio documents
	@RequestMapping(method = RequestMethod.GET, value = "/api/generateMySedaPortfolio",  produces = "application/pdf")
	public void generateMySedaPortfolio(HttpServletRequest request, HttpServletResponse response, @RequestParam(value = "studentId") Integer studentId) {
		GenericResult<byte[]> result = new GenericResult<byte[]>();
		
		try{					
			// System.out.println("servletContext : "+ servletContext);
			applicationService.generateMySedaPortfolio(request, response, servletContext, studentId);
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error loading the document. Please contact ICT");
    		logger.error("There was an error loading the document",e);
    	}
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getSemesters")
	public @ResponseBody GenericResult<List<Semester>> getSemesters() {
		GenericResult<List<Semester>> result = new GenericResult<List<Semester>>();
		try{						
			result.setData(applicationService.getSemesters());
		}catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting semesters. Please contact ICT");
    		logger.error("There was an error getting semesters",e);
    	}
		return result;
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value="/api/getStudentConversations")
	public @ResponseBody GenericResult<List<StudentConversation>> getStudentConversations(@ModelAttribute("MySedaStudentId") Integer studentId, @RequestParam(value = "selectedStudentId", required= false) Integer selectedStudentId) {
		GenericResult<List<StudentConversation>> result = new GenericResult<List<StudentConversation>>();
		try {			
			
			if(selectedStudentId!=null) {
				result.setData(studentConversationService.getStudentConversations(selectedStudentId));
			}else {
				result.setData(studentConversationService.getStudentConversations(studentId));
			}
		} catch(Exception e){
    		result.setError(true);
    		result.setErrorMessage("There was an error getting student conversations. Please contact ICT");
    		logger.error("There was an error getting student conversations", e);
    		e.printStackTrace();
    	}
		return result;
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value="/api/saveConversation")
	public @ResponseBody GenericResult<StudentConversation> saveConversation(@ModelAttribute("MySedaStudentId") Integer userId, @RequestBody StudentConversation conversation) {
		GenericResult<StudentConversation> result = new GenericResult<StudentConversation>();
		try {
			if(conversation.getTeacherId() == null) {
			//	StudentProfile studentP=applicationService.getProfileByStudentId(userId);
				conversation.setTeacherId(1);
			}
			
			if(conversation.getTopic() != null && conversation.getStudentComment() != null) {
				result.setData(applicationService.saveConversation(userId, conversation));
			} else {
				result.setError(true);
				result.setErrorMessage("Conversation should be initiated by the student.");
			}
		} catch(Exception e) {
    		result.setError(true);
    		e.printStackTrace();
    		result.setErrorMessage("There was an error saving the conversation. Please contact ICT");
    		logger.error("There was an error saving the conversation", e);
    	}

		return result;
	}
	
}
