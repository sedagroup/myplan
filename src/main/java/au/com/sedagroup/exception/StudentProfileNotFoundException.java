package au.com.sedagroup.exception;

public class StudentProfileNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;

	public StudentProfileNotFoundException(String message) {
		super(message);
	}

}
